
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- incidencia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `incidencia`;

CREATE TABLE `incidencia`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `origen` VARCHAR(1024) NOT NULL,
    `nombre_persona` VARCHAR(1024) NOT NULL,
    `breve` VARCHAR(1024),
    `expediente` VARCHAR(256),
    `descripcion` TEXT,
    `atencion` VARCHAR(256),
    `prioridad` INTEGER,
    `resuelta` TINYINT(1),
    `marcada` TINYINT(1),
    `eliminado` TINYINT(1),
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `fecha_incidencia` DATE,
    `fecha_resolucion` DATE,
    `comunidad_id` INTEGER,
    `persona_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `incidencia_FI_1` (`comunidad_id`),
    INDEX `incidencia_FI_2` (`persona_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- aviso
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `aviso`;

CREATE TABLE `aviso`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(1024) NOT NULL,
    `descripcion` TEXT,
    `prioridad` INTEGER NOT NULL,
    `marcada` TINYINT(1),
    `eliminado` TINYINT(1),
    `fecha_aviso` DATE,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `incidencia_id` INTEGER,
    `comunidad_id` INTEGER,
    `persona_id` INTEGER,
    `entrega_id` INTEGER,
    `contabilidad_id` INTEGER,
    `tarea_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `aviso_FI_1` (`incidencia_id`),
    INDEX `aviso_FI_2` (`tarea_id`),
    INDEX `aviso_FI_3` (`comunidad_id`),
    INDEX `aviso_FI_4` (`persona_id`),
    INDEX `aviso_FI_5` (`entrega_id`),
    INDEX `aviso_FI_6` (`contabilidad_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tarea
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tarea`;

CREATE TABLE `tarea`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(1024) NOT NULL,
    `descripcion` TEXT,
    `prioridad` INTEGER NOT NULL,
    `orden` INTEGER,
    `marcada` TINYINT(1),
    `eliminado` TINYINT(1),
    `realizada` TINYINT(1),
    `fecha_tarea` DATE,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `incidencia_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `tarea_FI_1` (`incidencia_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- entrega
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `entrega`;

CREATE TABLE `entrega`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(1024) NOT NULL,
    `descripcion` TEXT,
    `priodidad` INTEGER,
    `marcada` TINYINT(1),
    `eliminado` TINYINT(1),
    `fecha_entrega` DATE,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `comunidad_id` INTEGER,
    `tipo_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `entrega_FI_1` (`comunidad_id`),
    INDEX `entrega_FI_2` (`tipo_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- contabilidad
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contabilidad`;

CREATE TABLE `contabilidad`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `resumen` VARCHAR(1024) NOT NULL,
    `notas` TEXT,
    `saldo` FLOAT,
    `deuda` FLOAT,
    `eliminado` TINYINT(1),
    `fecha_entrega` DATE NOT NULL,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `marcada` TINYINT(1),
    `comunidad_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `contabilidad_FI_1` (`comunidad_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tipoentrega
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipoentrega`;

CREATE TABLE `tipoentrega`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tipo` VARCHAR(1024) NOT NULL,
    `notas` TEXT,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- seguimiento
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `seguimiento`;

CREATE TABLE `seguimiento`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `descripcion` TEXT,
    `fecha_seguimiento` DATE,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `incidencia_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `seguimiento_FI_1` (`incidencia_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- comunidad
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `comunidad`;

CREATE TABLE `comunidad`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(128) NOT NULL,
    `direccion` VARCHAR(1028),
    `presidente` VARCHAR(128),
    `cif` VARCHAR(128),
    `cuenta` VARCHAR(512),
    `marcada` TINYINT(1),
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- persona
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(128) NOT NULL,
    `direccion` VARCHAR(128),
    `marcada` TINYINT(1),
    `nota` TEXT,
    `comunidad_id` INTEGER,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    PRIMARY KEY (`id`),
    INDEX `persona_FI_1` (`comunidad_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- telefono
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `telefono`;

CREATE TABLE `telefono`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `telefono` VARCHAR(128) NOT NULL,
    `tipo` VARCHAR(128) DEFAULT 'CASA',
    `nota` TEXT,
    `marcada` TINYINT(1) DEFAULT 0,
    `persona_id` INTEGER,
    `incidencia_id` INTEGER,
    `empresa_id` INTEGER,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    PRIMARY KEY (`id`),
    INDEX `telefono_FI_1` (`persona_id`),
    INDEX `telefono_FI_2` (`incidencia_id`),
    INDEX `telefono_FI_3` (`empresa_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- empresa
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(128) NOT NULL,
    `direccion` VARCHAR(1024),
    `cif` VARCHAR(128),
    `contacto` VARCHAR(128),
    `observaciones` TEXT,
    `marcada` TINYINT(1),
    `comunidad_id` INTEGER,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    PRIMARY KEY (`id`),
    INDEX `empresa_FI_1` (`comunidad_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- caja
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `notas` TEXT,
    `texto` VARCHAR(128) NOT NULL,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `marcada` TINYINT(1),
    `comunidad_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `caja_FI_1` (`comunidad_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- movimiento_caja
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `movimiento_caja`;

CREATE TABLE `movimiento_caja`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `fecha_movimiento` DATE NOT NULL,
    `concepto` VARCHAR(1024) NOT NULL,
    `devolver` TINYINT(1),
    `cantidad` REAL NOT NULL,
    `fecha_creacion` DATE,
    `fecha_modificacion` DATE,
    `caja_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `movimiento_caja_FI_1` (`caja_id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
