<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 13:13
 */



$app->get('/movimiento/:id', function ($id  ) use ($app) {
    try {
        $movimiento = MovimientoQuery::create()->join('Caja')->withColumn('Caja.Texto','Texto')->filterByCajaId($id)->find();

        if ($movimiento->count() > 0) {
            //$movimiento->populateRelation('Comunidad');
            $array = $movimiento->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('movimiento');

$app->get('/movimiento/get/:id', function ($id) use ($app) {
    try {
        $movimiento = MovimientoQuery::create()->join('Caja')->withColumn('Caja.Texto','Texto')->filterByPrimaryKey($id)->findOne();

        if ($movimiento) {
            $array = $movimiento->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('movimientoGet');

$app->post('/movimiento/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $movimiento = new Movimiento();
        $fecha = new DateTime($post['FechaMovimiento']);
        $movimiento->setFechaMovimiento($fecha);
        $movimiento->setConcepto($post['Concepto']);
        $movimiento->setCajaId($post['CajaId']);
        $movimiento->setCantidad($post['Cantidad']);//TODO posible error con el float
        $movimiento->setFechaCreacion(new DateTime());
        $movimiento->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $movimiento->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('movimientoCreate');

$app->put('/movimiento/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $movimiento = MovimientoQuery::create()->filterById($id)->findOne();
        if (isset($movimiento)) {
            $fecha = new DateTime($post['FechaMovimiento']);
            $movimiento->setFechaMovimiento($fecha);
            $movimiento->setCajaId($post['CajaId']);
            $movimiento->setConcepto($post['Concepto']);
            $movimiento->setCantidad($post['Cantidad']);//TODO posible error con el float
            $movimiento->setFechaMovimiento(new DateTime());
            $movimiento->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('movimientoUpdate');

$app->delete('/movimiento/delete/:id', function ($id) use ($app) {
    try {
        $movimiento = MovimientoQuery::create()->filterById($id)->findOne();
        if ($movimiento) {
            $movimiento->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('movimientoDelete');