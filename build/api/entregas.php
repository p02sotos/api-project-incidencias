<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 13/06/14
 * Time: 17:47
 */
$app->get('/entrega', function () use ($app) {
    try {
        $entrega = EntregaQuery::create()->join('Comunidad')->join('Tipo')->select(array('Id','Nombre','Descripcion','Tipo.Tipo','FechaEntrega','Comunidad.Id','Comunidad.Nombre'))->find();


        if ($entrega->count() > 0) {
            //$entrega->populateRelation('Comunidad');
            $array = $entrega->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('entrega');

$app->get('/entrega/:id', function ($id) use ($app) {
    try {
        $entrega = EntregaQuery::create()->join('Comunidad')->join('Tipo')->select(array('Id','Nombre','Descripcion','Tipo.Id','FechaEntrega','Comunidad.Nombre','Comunidad.Id'))->filterById($id)->findOne();

       if ($entrega) {
            $array = $entrega;//$entrega->toJSON(false, false);
            echo (json_encode($array));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('entregaGet');

$app->get('/entrega/between/:inicio/:final', function ($inicio, $final) use($app) {

    $entregas = EntregaQuery::create()->join('Comunidad')->join('Tipo')->withColumn('Tipo.Tipo','Tipo.Tipo')->withColumn('Comunidad.Id','Comunidad.Id')->withColumn('Comunidad.Nombre','Comunidad.Nombre')->filterByFechaEntrega(array('min'=>strtotime($inicio),'max'=>strtotime($final)))->find();
    if ($entregas->count() > 0) {
        $array = $entregas->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('entregaBetween');

$app->get('/entrega/search/:date', function ($date) use($app) {
    var_dump($date);
    $parsedDate = new DateTime($date);
    $entregas = EntregaQuery::create()->filterByFechaEntrega($parsedDate)->find();
    if ($entregas->count() > 0) {
        $array = $entregas->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('entregaSearch');
$app->post('/entrega/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $entrega = new Entrega();
        $entrega->setNombre($post['Nombre']);
        $entrega->setDescripcion($post['Descripcion']);
        $entrega->setFechaCreacion(new DateTime());
        $fecha = new DateTime($post['FechaEntrega']);
        $entrega->setFechaEntrega($fecha);
        $entrega->setTipoId($post['TipoId']);
        $entrega->setComunidadId($post['ComunidadId']);
        $entrega->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $entrega->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('entregaCreate');

$app->put('/entrega/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $entrega = EntregaQuery::create()->filterById($id)->findOne();
        if (isset($entrega)) {
            $entrega->setFechaModificacion(new DateTime());
            $entrega->setNombre($post['Nombre']);
            $entrega->setDescripcion($post['Descripcion']);
            $fecha = new DateTime($post['FechaEntrega']);
            $entrega->setFechaEntrega($fecha);
            $entrega->setFechaModificacion(new DateTime());
            $entrega->setTipoId($post['Tipo.Id']);
            $entrega->setComunidadId($post['Comunidad.Id']);
            $entrega->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('entregaUpdate');

$app->delete('/entrega/delete/:id', function ($id) use ($app) {
    try {
        $entrega = EntregaQuery::create()->filterById($id)->findOne();
        if ($entrega) {
            $entrega->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('entregaDelete');