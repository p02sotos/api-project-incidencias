<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 0:02
 */

$app->get('/tipo', function () use ($app) {
    try {
        $tipo = TipoQuery::create()->find();

        if ($tipo->count() > 0) {
            //$tipo->populateRelation('Comunidad');
            $array = $tipo->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('tipo');

$app->get('/tipo/:id', function ($id) use ($app) {
    try {
        $tipo = TipoQuery::create()->filterByPrimaryKey($id)->findOne();

        if ($tipo) {
            $array = $tipo->toJSON(false, false);
        echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('tipoGet');

$app->post('/tipo/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $tipo = new Tipo();
        $tipo->setNotas($post['Notas']);
        $tipo->setTipo($post['Tipo']);
        $tipo->setFechaCreacion(new DateTime());

        $tipo->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $tipo->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('tipoCreate');

$app->put('/tipo/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $tipo = TipoQuery::create()->filterById($id)->findOne();
        if (isset($tipo)) {
            $tipo->setFechaModificacion(new DateTime());
            $tipo->setNotas($post['Notas']);
            $tipo->setTipo($post['Tipo']);
            $fecha = new DateTime($post['FechaTipo']);
            $tipo->setFechaTipo($fecha);
            $tipo->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('tipoUpdate');

$app->delete('/tipo/delete/:id', function ($id) use ($app) {
    try {
        $tipo = TipoQuery::create()->filterById($id)->findOne();
        if ($tipo) {
            $tipo->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('tipoDelete');