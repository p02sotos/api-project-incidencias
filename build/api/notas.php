<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 13:50
 */


$app->get('/nota', function () use ($app) {
    try {
        $nota = NotaQuery::create()->join('Comunidad')->join('Incidencia')->withColumn('Comunidad.Nombre')->withColumn('Incidencia.Breve')->find();

        if ($nota->count() > 0) {
            //$nota->populateRelation('Comunidad');
            $array = $nota->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('nota');

$app->get('/nota/:id', function ($id) use ($app) {
    try {
        $nota = NotaQuery::create()->join('Comunidad')->join('Incidencia')->withColumn('Comunidad.Nombre')->withColumn('Incidencia.Breve')->filterByPrimaryKey($id)->findOne();

        if ($nota) {
            $array = $nota->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('notaGet');

$app->post('/nota/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $nota = new Nota();

        $nota->setComunidadId($post['ComunidadId']);//TODO poner isset, posible error
        $nota->setIncidenciaId($post['IncidenciaId']);
        $nota->setTexto($post['Texto']);
        $nota->setFechaCreacion(new DateTime());
        $nota->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $nota->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('notaCreate');

$app->put('/nota/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $nota = NotaQuery::create()->filterById($id)->findOne();
        if (isset($nota)) {
            $nota->setComunidadId($post['ComunidadId']);//TODO poner isset, posible error
            $nota->setIncidenciaId($post['IncidenciaId']);
            $nota->setTexto($post['Texto']);
            $nota->setFechaModificacion(new DateTime());

            $nota->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('notaUpdate');

$app->delete('/nota/delete/:id', function ($id) use ($app) {
    try {
        $nota = NotaQuery::create()->filterById($id)->findOne();
        if ($nota) {
            $nota->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('notaDelete');