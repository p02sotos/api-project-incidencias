<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 19:13
 */

$app->get('/urgencia', function () use ($app) {
    try {
        $urgencia = UrgenciaQuery::create()->find();

        if ($urgencia->count() > 0) {
            //$urgencia->populateRelation('Comunidad');
            $array = $urgencia->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('urgencia');

$app->get('/urgencia/:id', function ($id) use ($app) {
    try {
        $urgencia = UrgenciaQuery::create()->filterByPrimaryKey($id)->findOne();

        if ($urgencia) {
            $array = $urgencia->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('urgenciaGet');

$app->post('/urgencia/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $urgencia = new Urgencia();
        $urgencia->setNombre($post['Nombre']);
        $urgencia->setDescripcion($post['Descriptcion']);
        $urgencia->setPrioridad($post['Prioridad']);
        $urgencia->setFechaCreacion(new DateTime());
        $urgencia->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $urgencia->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('urgenciaCreate');

$app->put('/urgencia/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $urgencia = UrgenciaQuery::create()->filterById($id)->findOne();
        if (isset($urgencia)) {
            $urgencia = new Urgencia();
            $urgencia->setNombre($post['Nombre']);
            $urgencia->setDescripcion($post['Descriptcion']);
            $urgencia->setPrioridad($post['Prioridad']);
            $urgencia->setFechaModificacion(new DateTime());
            $urgencia->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('urgenciaUpdate');

$app->delete('/urgencia/delete/:id', function ($id) use ($app) {
    try {
        $urgencia = UrgenciaQuery::create()->filterById($id)->findOne();
        if ($urgencia) {
            $urgencia->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('urgenciaDelete');