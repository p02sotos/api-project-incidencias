<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 12/06/14
 * Time: 19:44
 */

/*
 *
 * Tabla Incidencia
 *
 */

$app->get('/incidencia', function () use($app) {
    $today = new DateTime();

    $incidencias = IncidenciaQuery::create()->filterByEliminado(false)->filterByFechaIncidencia(array('min'=>strtotime("-6 month")))->find();
    if ($incidencias->count() > 0) {
        $incArray = $incidencias->toJSON(false,false);
        echo $incArray;
    } else {
        $incArray = array();
        echo json_encode($incArray);
    }
})->name('incidencia');

$app->get('/incidencia/phone/', function () use ($app) {
    try {
        //$incidencias = PersonaQuery::create()->withColumn('TelefonoPersona.Telefono','Telefono')->find();
        $incidencias = IncidenciaQuery::create()->filterByEliminado(false)->joinPersona()->joinComunidad()->withColumn('Comunidad.Nombre')->withColumn('Persona.Nombre')->find();
        //$incidencia = IncidenciaQuery::create()->findOne();

        if ($incidencias->count() > 0) {
            $incidenciasArray = array();
            foreach ($incidencias as $incidencia){
                $incidenciaArray = $incidencia->toArray();
                $telefonos = array();
                foreach ($incidencia->getTelefonoIncidencias() as $phone){
                    array_push($telefonos,$phone->getTelefono());
                }
                if(empty($telefonos)){
                    array_push($telefonos,"");
                }
                $incidenciaArray['Telefonos']=$telefonos;
                array_push($incidenciasArray,$incidenciaArray);

            }
            echo (json_encode($incidenciasArray));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('persona');
$app->get('/incidencia/phone/trash', function () use ($app) {
    try {
        //$incidencias = PersonaQuery::create()->withColumn('TelefonoPersona.Telefono','Telefono')->find();
        $incidencias = IncidenciaQuery::create()->filterByEliminado(true)->joinPersona()->joinComunidad()->withColumn('Comunidad.Nombre')->withColumn('Persona.Nombre')->find();
        //$incidencia = IncidenciaQuery::create()->findOne();

        if ($incidencias->count() > 0) {
            $incidenciasArray = array();
            foreach ($incidencias as $incidencia){
                $incidenciaArray = $incidencia->toArray();
                $telefonos = array();
                foreach ($incidencia->getTelefonoIncidencias() as $phone){
                    array_push($telefonos,$phone->getTelefono());
                }
                if(empty($telefonos)){
                    array_push($telefonos,"");
                }
                $incidenciaArray['Telefonos']=$telefonos;
                array_push($incidenciasArray,$incidenciaArray);

            }
            echo (json_encode($incidenciasArray));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('incidenciaTrash');

$app->get('/incidencia/all', function () use($app) {
    $incidencias = IncidenciaQuery::create()->find();
    if ($incidencias->count() > 0) {
        //echo json_encode($incidenciasToSend);
        $incArray = $incidencias->toJSON(false,false);
        echo $incArray;
    } else {
        $incArray = array();
        echo json_encode($incArray);
    }
})->name('incidenciaAll');
$app->get('/incidencia/:id', function ($id) use($app) {
    try {
        $incidencia = IncidenciaQuery::create()->join('Comunidad')->joinPersona()->withColumn('Comunidad.Nombre')->withColumn('Persona.Nombre')->filterById($id)->findOne();
        //$incidencia = PersonaQuery::create()->findOne();

        //$incidencias = PersonaQuery::create()->filterById($id)->findOne();

        if ($incidencia) {
            $phones = $incidencia->getTelefonoIncidencias();
            $phonesArray = array();
            foreach ($phones as $phone){
                array_push($phonesArray, $phone->getTelefono());
            }
            //$array = $incidencia;//$incidencia->toJSON(false, false);
            //echo (json_encode($incidencia));
            $incidenciaArray = $incidencia->toArray();
            $incidenciaArray['Telefonos'] = $phonesArray;

            echo json_encode($incidenciaArray);
        } else {
            $array = array();
            echo "no incidencia";
            //echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaBetween');

$app->get('/incidencia/between/:inicio/:final', function ($inicio, $final) use($app) {
    $incidencias = IncidenciaQuery::create()->filterByEliminado(false)->join('Comunidad')->join('Persona')->withColumn('Comunidad.Nombre','ComunidadNombre')->withColumn('Persona.Nombre')->filterByFechaIncidencia(array('min'=>strtotime($inicio),'max'=>strtotime($final)))->find();
    if ($incidencias->count() > 0) {
        $incidenciasArray = array();
        foreach ($incidencias as $incidencia){
            $incidenciaArray = $incidencia->toArray();
            $telefonos = array();
            foreach ($incidencia->getTelefonoIncidencias() as $phone){
                array_push($telefonos,$phone->getTelefono());
            }
            if(empty($telefonos)){
                array_push($telefonos,"");
            }
            $incidenciaArray['Telefonos']=$telefonos;
            array_push($incidenciasArray,$incidenciaArray);

        }
        echo (json_encode($incidenciasArray));
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('incidenciaBetween');

$app->get('/incidencia/search/:field/:value', function ($field, $value) use($app) {
    switch ($field) {
        case "descripcion":
            $incidencias = IncidenciaQuery::create()->filterByDescripcion($value)->find();
            break;
        case "origen":
            $incidencias = IncidenciaQuery::create()->filterByOrigen($value)->find();
            break;
        case "nota":
            $incidencias = IncidenciaQuery::create()->filterByNota($value)->find();
            break;
        case "breve":
             $incidencias = IncidenciaQuery::create()->filterByBreve($value)->find();
            break;
        case "telefono":
            $incidencias = IncidenciaQuery::create()
                ->useTelefonoQuery()
                    ->filterByTelefono($value)
                ->endUse()
                ->find();

            break;
        case "seguimiento":
            $incidencias = IncidenciaQuery::create()
                ->useSeguimientoQuery($value)
                    ->filterByDescripcion()->find();


            break;
           }
    if ($incidencias->count() > 0) {
        //echo json_encode($incidenciasToSend);
        $incArray = $incidencias->toJSON(false,false);
        echo $incArray;
    } else {
        $incArray = array();
        echo json_encode($incArray);
    }
})->name('incidenciaField');


$app->post('/incidencia/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $incidencia = new Incidencia();
        $incidencia->setBreve($post['Breve']);
        $incidencia->setOrigen($post['Origen']);
        $incidencia->setAtencion($post['Atencion']);
        $incidencia->setComunidadId($post['ComunidadId']);
        $incidencia->setResuelta(false);
        $incidencia->setExpediente($post['Expediente']);
        $incidencia->setPrioridad($post['Prioridad']);
        $incidencia->setPersonaId($post['PersonaId']);
        $incidencia->setEliminado(false);
        $incidencia->setDescripcion($post['Descripcion']);
        $fecha = new DateTime($post['FechaIncidencia']);
        $incidencia->setFechaIncidencia($fecha);
        $incidencia->setFechaCreacion(new DateTime());
        $phones = $post['Telefonos'];
        if(!empty($phones)){
            foreach ($phones as $phone){
                $newPhone = new Telefono();
                $newPhone->setTelefono($phone);
                $newPhone->setIncidencia($incidencia);
                $newPhone->setFechaCreacion(new DateTime());
                $newPhone->save();
            }
        }
        $incidencia->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $incidencia->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaCreate');

$app->put('/incidencia/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if (isset($incidencia)) {
            $incidencia->setBreve($post['Breve']);
            $incidencia->setOrigen($post['Origen']);
            $incidencia->setAtencion($post['Atencion']);
            $incidencia->setComunidadId($post['ComunidadId']);
            $incidencia->setResuelta(false);
            $incidencia->setPrioridad($post['Prioridad']);
            $incidencia->setPersonaId($post['PersonaId']);
             $incidencia->setExpediente($post['Expediente']);
            $incidencia->setEliminado(false);
            $incidencia->setDescripcion($post['Descripcion']);
            $fecha = new DateTime($post['FechaIncidencia']);
            $incidencia->setFechaIncidencia($fecha);
            $incidencia->setFechaCreacion(new DateTime());
            $actualphones = TelefonoQuery::create()->filterByIncidencia($incidencia)->find();
            foreach ($actualphones as $phone){
                $phone->delete();
            }
            $phones = $post['Telefonos'];
            if(!empty($phones)){
                foreach ($phones as $phone){
                    $newPhone = new Telefono();
                    $newPhone->setTelefono($phone);
                    $newPhone->setIncidencia($incidencia);
                    $newPhone->setFechaCreacion(new DateTime());
                    $newPhone->save();
                }
            }
            $incidencia->save();
            $app->response()->header('Content-Type', 'application/json');
            echo $incidencia->toJSON(false, false);
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaUpdate');

$app->get('/incidencia/delete/:id', function ($id) use ($app) {
    try {
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if ($incidencia) {
            $incidencia->setEliminado(true);
            $incidencia->save();
            echo "OK";
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaDelete');
$app->get('/incidencia/restore/:id', function ($id) use ($app) {
    try {
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if ($incidencia) {
            $incidencia->setEliminado(false);
            $incidencia->save();
            echo "OK";
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaRestore');
$app->delete('/incidencia/purgue/:id', function ($id) use ($app) {
    try {
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if ($incidencia) {
            $incidencia->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaDelete');

$app->get('/incidencia/marcar/:id', function ($id) use ($app) {
    try {
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if ($incidencia) {
            if ($incidencia->getMarcada()){
                $incidencia->setMarcada(false);
            }else {
                $incidencia->setMarcada(true);
            }
            $incidencia->save();
            echo "OK";


        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaMarcar');

$app->get('/incidencia/resolve/:id', function ($id) use ($app) {
    try {
        $incidencia = IncidenciaQuery::create()->filterById($id)->findOne();
        if ($incidencia) {
            if ($incidencia->getResuelta()){
                $incidencia->setResuelta(false);
            }else {
                $incidencia->setResuelta(true);
            }
            $incidencia->save();
            echo "OK";


        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('incidenciaMarcar');