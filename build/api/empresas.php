<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 18:49
 */


$app->get('/empresa', function () use ($app) {
    try {
        $empresa = EmpresaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre','Comunidad.Nombre')->find();

        if ($empresa->count() > 0) {
            //$empresa->populateRelation('Comunidad');
            $array = $empresa->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('empresa');

$app->get('/empresa/phones/', function () use ($app) {
    try {
        //$empresas = EmpresaQuery::create()->withColumn('TelefonoEmpresa.Telefono','Telefono')->find();
        //$empresa = EmpresaQuery::create()->findOne();
        //$empresa->getTe
        $empresas = EmpresaQuery::create()->joinComunidad()->withColumn('Comunidad.Nombre')->find();

        if ($empresas->count() > 0) {
            $empresasArray = array();
            foreach ($empresas as $empresa){
                $empresaArray = $empresa->toArray();
                $telefonos = array();
                foreach ($empresa->getTelefonoEmpresas() as $phone){
                    array_push($telefonos,$phone->getTelefono());
                }
                if(empty($telefonos)){
                    array_push($telefonos,"");
                }
                $empresaArray['Telefonos']=$telefonos;
                array_push($empresasArray,$empresaArray);

            }
            echo (json_encode($empresasArray));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('empresaPhones');

$app->get('/empresa/:id', function ($id) use ($app) {
    try {
        $empresa = EmpresaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre')->filterByPrimaryKey($id)->findOne();
        $phones = $empresa->getTelefonoEmpresas();
        $phonesArray = array();
        foreach ($phones as $phone){
            array_push($phonesArray, $phone->getTelefono());
        }
        if ($empresa) {
            //$array = $persona;//$persona->toJSON(false, false);
            //echo (json_encode($persona));
            $empresaArray = $empresa->toArray();
            $empresaArray['Telefonos'] = $phonesArray;

            echo json_encode($empresaArray);
        } else {
            $array = array();
            echo "no persona";
            //echo json_encode($array);
        }
        /*if ($empresa) {
            $array = $empresa->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }*/
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('empresaGet');

$app->post('/empresa/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $empresa = new Empresa();
        $empresa->setNombre($post['Nombre']);
        $empresa->setDireccion($post['Direccion']);
        $empresa->setCif($post['Cif']);
        $empresa->setMarcada(0);
        $empresa->setComunidadId($post['ComunidadId']);
        $empresa->setContacto($post['Contacto']);
        $empresa->setObservaciones($post['Observaciones']);
        $empresa->setFechaCreacion(new DateTime());
        $phones = $post['Telefonos'];

        if(!empty($phones)){
            foreach ($phones as $phone){
                $newPhone = new Telefono();
                $newPhone->setTelefono($phone);
                $newPhone->setEmpresa($empresa);
                $newPhone->setFechaCreacion(new DateTime());
                $newPhone->save();
            }
        }
        $empresa->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $empresa->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('empresaCreate');

$app->put('/empresa/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $empresa = EmpresaQuery::create()->filterById($id)->findOne();
        //Borramos los telefonos
        $actualphones = TelefonoQuery::create()->filterByEmpresa($empresa)->find();
        foreach ($actualphones as $phone){
            $phone->delete();
        }
        //Actualizamos
        $phones = $post['Telefonos'];

        if(!empty($phones)){
            foreach ($phones as $phone){
                $newPhone = new Telefono();
                $newPhone->setTelefono($phone);
                $newPhone->setFechaCreacion(new DateTime());
                $newPhone->setEmpresa($empresa);
                $newPhone->save();
            }
        }
        if (isset($empresa)) {
            $empresa->setNombre($post['Nombre']);
            $empresa->setDireccion($post['Direccion']);
            $empresa->setCif($post['Cif']);
            $empresa->setContacto($post['Contacto']);
            $empresa->setComunidadId($post['ComunidadId']);
            $empresa->setObservaciones($post['Observaciones']);
            $empresa->setFechaModificacion(new DateTime());
            $empresa->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('empresaUpdate');

$app->get('/empresa/marcar/:id', function ($id) use ($app) {
    try {
        $empresa = EmpresaQuery::create()->filterById($id)->findOne();
        if ($empresa) {
            if ($empresa->getMarcada()){
                $empresa->setMarcada(false);
            }else {
                $empresa->setMarcada(true);
            }
            $empresa->save();
            echo "OK";


        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('empresaMarcar');

$app->delete('/empresa/delete/:id', function ($id) use ($app) {
    try {
        $empresa = EmpresaQuery::create()->filterById($id)->findOne();
        if ($empresa) {
            $empresa->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('empresaDelete');