<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 13:40
 */

$app->get('/telefono', function () use ($app) {
    try {
        //$telefono = TelefonoQuery::create()->join('Persona')->withColumn('Persona.Nombre')->join('Incidencia')->withColumn('Incidencia.Id')->join('Empresa')->withColumn('Empresa.Nombre')->find();
        $telefono = TelefonoQuery::create()->joinPersona()->joinEmpresa()->joinIncidencia()->withColumn('Persona.Nombre')->withColumn('Incidencia.Id')->withColumn('Empresa.Nombre')->find();


        if ($telefono->count() > 0) {
            //$telefono->populateRelation('Comunidad');
            $array = $telefono->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('telefono');

$app->get('/telefono/:id', function ($id) use ($app) {
    try {
        //$telefono = TelefonoQuery::create()->joinPersona()->joinEmpresa()->joinIncidencia()->withColumn('Persona.Nombre')->withColumn('Incidencia.Resumen')->withColumn('Empresa.Nombre')->filterById($id)->findOne();
        $telefono = TelefonoQuery::create()->filterById($id)->findOne();

        if ($telefono) {
            $array = $telefono->toJSON(false,false);//$telefono->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoGet');

$app->get('/telefono/between/:inicio/:final', function ($inicio, $final) use($app) {
    $today = new DateTime();

    $telefonos = TelefonoQuery::create()->filterByFechaTelefono(array('min'=>strtotime($inicio),'max'=>strtotime($final)))->find();
    if ($telefonos->count() > 0) {
        $array = $telefonos->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('telefonoBetween');

$app->get('/telefono/search/:date', function ($date) use($app) {
    var_dump($date);
    $parsedDate = new DateTime($date);
    $telefonos = TelefonoQuery::create()->filterByFechaTelefono($parsedDate)->find();
    if ($telefonos->count() > 0) {
        $array = $telefonos->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('telefonoSearch');
$app->post('/telefono/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $telefono = new Telefono();
        $telefono->setTelefono($post['Telefono']);
        $telefono->setTipo($post['Tipo']);
        $telefono->setNota($post['Nota']);
        if(isset($post['PersonaId'])){
            $telefono->setPersonaId($post['PersonaId']);
        }
        if(isset($post['IncidenciaId'])){
            $telefono->setIncidenciaId($post['IncidenciaId']);
        }
        if(isset($post['EmpresaId'])){
            $telefono->setEmpresaId($post['EmpresaId']);
        }
        $telefono->setFechaCreacion(new DateTime());
        $telefono->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $telefono->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoCreate');

$app->put('/telefono/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $telefono = TelefonoQuery::create()->filterById($id)->findOne();
        if (isset($telefono)) {
            $telefono->setTelefono($post['Telefono']);
            $telefono->setTipo($post['Tipo']);
            $telefono->setNota($post['Nota']);
            if(isset($post['PersonaId'])){
                $telefono->setPersonaId($post['PersonaId']);
            }else {
                $telefono->setPersonaId(null);
            }
            if(isset($post['IncidenciaId'])){
                $telefono->setIncidenciaId($post['IncidenciaId']);
            }else{
                $telefono->setIncidenciaId(null);
            }
            if(isset($post['EmpresaId'])){
                $telefono->setEmpresaId($post['EmpresaId']);
            }else{
                $telefono->setEmpresaId(null);
            }
            $telefono->setFechaModificacion(new DateTime());
            $telefono->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoUpdate');

$app->delete('/telefono/delete/:id', function ($id) use ($app) {
    try {
        $telefono = TelefonoQuery::create()->filterById($id)->findOne();
        if ($telefono) {
            $telefono->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoDelete');

$app->get('/telefono/checkPersona/:phone/:id', function ($phone,$id) use ($app) {
    try {
        //$telefono = TelefonoQuery::create()->joinPersona()->joinEmpresa()->joinIncidencia()->withColumn('Persona.Nombre')->withColumn('Incidencia.Resumen')->withColumn('Empresa.Nombre')->filterById($id)->findOne();
        $telefonos = TelefonoQuery::create()->filterByTelefono($phone)->filterByPersonaId($id)->find();

        if ($telefonos->count()>=1) {
            $res = array();
            $res['Error']= "ERROR";
            echo json_encode($res);
        } else if ($telefonos->count()==0){
            echo json_encode("NO");
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoCheck');
$app->get('/telefono/checkEmpresa/:phone/:id', function ($phone,$id) use ($app) {
    try {
        //$telefono = TelefonoQuery::create()->joinPersona()->joinEmpresa()->joinIncidencia()->withColumn('Persona.Nombre')->withColumn('Incidencia.Resumen')->withColumn('Empresa.Nombre')->filterById($id)->findOne();
        $telefonos = TelefonoQuery::create()->filterByTelefono($phone)->filterByEmpresaId($id)->find();

        if ($telefonos->count()>=1) {
            $res = array();
            $res['Error']= "ERROR";
            echo json_encode($res);
        } else if ($telefonos->count()==0){
            echo json_encode("NO");
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoCheck');
$app->get('/telefono/checkIncidencia/:phone/:id', function ($phone,$id) use ($app) {
    try {
        //$telefono = TelefonoQuery::create()->joinPersona()->joinEmpresa()->joinIncidencia()->withColumn('Persona.Nombre')->withColumn('Incidencia.Resumen')->withColumn('Empresa.Nombre')->filterById($id)->findOne();
        $telefonos = TelefonoQuery::create()->filterByTelefono($phone)->filterByIncidenciaId($id)->find();

        if ($telefonos->count()>=1) {
            $res = array();
            $res['Error']= "ERROR";
            echo json_encode($res);
        } else if ($telefonos->count()==0){
            echo json_encode("NO");
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('telefonoCheck');