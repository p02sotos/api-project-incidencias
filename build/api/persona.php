<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 13/06/14
 * Time: 17:47
 */
$app->get('/persona', function () use ($app) {
    try {

        $persona = PersonaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre')->find();


        if ($persona->count() > 0) {
            //$persona->populateRelation('Comunidad');
            $array = $persona->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('persona');

$app->get('/persona/phones/', function () use ($app) {
    try {
        //$personas = PersonaQuery::create()->withColumn('TelefonoPersona.Telefono','Telefono')->find();
        $personas = PersonaQuery::create()->joinComunidad()->withColumn('Comunidad.Nombre')->find();

        if ($personas->count() > 0) {
            $personasArray = array();
            foreach ($personas as $persona){
                $personaArray = $persona->toArray();
                $telefonos = array();
                foreach ($persona->getTelefonoPersonas() as $phone){
                    array_push($telefonos,$phone->getTelefono());
                }
                if(empty($telefonos)){
                   array_push($telefonos,"");
                }
                    $personaArray['Telefonos']=$telefonos;
                    array_push($personasArray,$personaArray);

            }
            echo (json_encode($personasArray));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('persona');


$app->get('/persona/:id', function ($id) use ($app) {
    try {
        $persona = PersonaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre')->filterById($id)->findOne();
        //$persona = PersonaQuery::create()->findOne();
        $phones = $persona->getTelefonoPersonas();
        $phonesArray = array();
        foreach ($phones as $phone){
            array_push($phonesArray, $phone->getTelefono());
        }


        //$personas = PersonaQuery::create()->filterById($id)->findOne();

        if ($persona) {
            //$array = $persona;//$persona->toJSON(false, false);
            //echo (json_encode($persona));
            $personaArray = $persona->toArray();
            $personaArray['Telefonos'] = $phonesArray;

            echo json_encode($personaArray);
        } else {
            $array = array();
            echo "no persona";
            //echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('personaGet');

$app->get('/persona/between/:inicio/:final', function ($inicio, $final) use($app) {
    $today = new DateTime();

    $personas = PersonaQuery::create()->filterByFechaPersona(array('min'=>strtotime($inicio),'max'=>strtotime($final)))->find();
    if ($personas->count() > 0) {
        $array = $personas->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('personaBetween');

$app->get('/persona/search/:date', function ($date) use($app) {
    var_dump($date);
    $parsedDate = new DateTime($date);
    $personas = PersonaQuery::create()->filterByFechaPersona($parsedDate)->find();
    if ($personas->count() > 0) {
        $array = $personas->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('personaSearch');
$app->post('/persona/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $persona = new Persona();
        $persona->setNombre($post['Nombre']);
        $persona->setDireccion($post['Direccion']);
        $persona->setNota($post['Nota']);
        $persona->setFechaCreacion(new DateTime());
        $persona->setMarcada(0);
        //Actualizamos
        $phones = $post['Telefonos'];

        if(!empty($phones)){
            foreach ($phones as $phone){
                $newPhone = new Telefono();
                $newPhone->setTelefono($phone);
                $newPhone->setPersona($persona);
                $newPhone->setFechaCreacion(new DateTime());
                $newPhone->save();
            }
        }

        $persona->setComunidadId($post['ComunidadId']);
        $persona->save();
        $app->response()->header('Content-Type', 'application/json');
        echo "OK";

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('personaCreate');

$app->put('/persona/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $persona = PersonaQuery::create()->filterById($id)->findOne();
        $persona->setNombre($post['Nombre']);
        $persona->setDireccion($post['Direccion']);
        $persona->setNota($post['Nota']);
        $persona->setFechaModificacion(new DateTime());
        //Borramos los telefonos
        $actualphones = TelefonoQuery::create()->filterByPersona($persona)->find();
        foreach ($actualphones as $phone){
            $phone->delete();
        }
        //Actualizamos
        $phones = $post['Telefonos'];

        if(!empty($phones)){
            foreach ($phones as $phone){
                $newPhone = new Telefono();
                $newPhone->setTelefono($phone);
                $newPhone->setPersona($persona);
                $newPhone->setFechaCreacion(new DateTime());
                $newPhone->save();
            }
        }

        $persona->setComunidadId($post['ComunidadId']);
        $persona->save();
        $app->response()->header('Content-Type', 'application/json');
        echo "OK";
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('personaUpdate');

$app->delete('/persona/delete/:id', function ($id) use ($app) {
    try {
        $persona = PersonaQuery::create()->filterById($id)->findOne();
        if ($persona) {
            $persona->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('personaDelete');