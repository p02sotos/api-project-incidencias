<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 13/06/14
 * Time: 18:31
 */
$app->get('/comunidad', function () use ($app) {
    $app->response()->header('Access-Control-Allow-Origin', 'http://localhost:9000');
    try {
        $comunidad = ComunidadQuery::create()->find();
        if ($comunidad->count() > 0) {
            $array = $comunidad->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('comunidad');


$app->get('/comunidad/:id', function ($id) use ($app) {
    $app->response()->header('Access-Control-Allow-Origin', 'http://localhost:9000');
    try {
        $comunidad = ComunidadQuery::create()->filterById($id)->findOne();
        if ($comunidad) {
            $array = $comunidad->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('comunidadGet');

$app->post('/comunidad/create', function () use ($app) {
    $app->response()->header('Access-Control-Allow-Origin', 'http://localhost:9000');
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $comunidad = new Comunidad();
        $comunidad->setFechaModificacion(new DateTime());
        $comunidad->setDireccion($post['Direccion']);
        $comunidad->setPresidente($post['Presidente']);
        $comunidad->setCif($post['Cif']);
        $comunidad->setNombre($post['Nombre']);
        $comunidad->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $comunidad->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('comunidadCreate');

$app->put('/comunidad/:id', function ($id) use ($app) {
    $app->response()->header('Access-Control-Allow-Origin', 'http://localhost:9000');
    $app->response()->header('Access-Control-Allow-Methods', 'GET, POST, PUT');
    $app->response()->header('Access-Control-Allow-Headers', 'accept, origin, content-type');
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        $post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $comunidad = ComunidadQuery::create()->filterById($id)->findOne();
        if (isset($comunidad)) {
            $comunidad->setFechaModificacion(new DateTime());
            $comunidad->setDireccion($post['Direccion']);
            $comunidad->setPresidente($post['Presidente']);
            $comunidad->setCif($post['Cif']);
            $comunidad->setNombre($post['Nombre']);
            $comunidad->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('comunidadUpdate');

$app->delete('/comunidad/delete/:id', function ($id) use ($app) {
    $app->response()->header('Access-Control-Allow-Origin', 'http://localhost:9000');
    try {
        $comunidad = ComunidadQuery::create()->filterById($id)->findOne();
        if ($comunidad) {
            $comunidad->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('comunidadDelete');