<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 12:58
 */


$app->get('/caja', function () use ($app) {
    try {
        $caja = CajaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre','Comunidad.Nombre')->find();

        if ($caja->count() > 0) {
            //$caja->populateRelation('Comunidad');
            $array = $caja->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('caja');

$app->get('/caja/:id', function ($id) use ($app) {
    try {
        $caja = CajaQuery::create()->join('Comunidad')->withColumn('Comunidad.Nombre','Comunidad.Nombre')->filterByPrimaryKey($id)->findOne();

        if ($caja) {
            $array = $caja->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('cajaGet');

$app->post('/caja/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $caja = new Caja();
        $caja->setNotas($post['Notas']);
        $caja->setTexto($post['Texto']);
        $caja->setMarcada(false);
        $caja->setComunidadId($post['ComunidadId']);
        $caja->setFechaCreacion(new DateTime());
        $caja->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $caja->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('cajaCreate');

$app->put('/caja/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $caja = CajaQuery::create()->filterById($id)->findOne();
        if (isset($caja)) {
            $caja->setFechaModificacion(new DateTime());
            $caja->setNotas($post['Notas']);
            $caja->setTexto($post['Texto']);
            $caja->setComunidadId($post['ComunidadId']);

            $caja->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('cajaUpdate');

$app->delete('/caja/delete/:id', function ($id) use ($app) {
    try {
        $caja = CajaQuery::create()->filterById($id)->findOne();
        if ($caja) {
            $caja->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('cajaDelete');

$app->get('/caja/marcar/:id', function ($id) use ($app) {
    try {
        $caja = CajaQuery::create()->filterById($id)->findOne();
        if ($caja) {
            if ($caja->getMarcada()){
                $caja->setMarcada(0);
            }else {
                $caja->setMarcada(1);
            }
            $caja->save();
            echo "OK";


        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('cajaMarcar');