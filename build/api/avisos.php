<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 19:09
 */

$app->get('/aviso', function () use ($app) {
    try {
        $aviso = AvisoQuery::create()->joinIncidencia()->joinTarea()->joinPersona()->joinComunidad()
            ->withColumn('Tarea.Nombre')->withColumn('Persona.Nombre')
            ->withColumn('Comunidad.Nombre')->withColumn('Incidencia.Id')
            ->find();

        if ($aviso->count() > 0) {
            //$aviso->populateRelation('Comunidad');
            $array = $aviso->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('aviso');

$app->get('/aviso/:id', function ($id) use ($app) {
    try {
        $aviso = AvisoQuery::create()->joinIncidencia()->joinTarea()->joinPersona()->joinComunidad()
            ->withColumn('Tarea.Nombre')->withColumn('Persona.Nombre')
            ->withColumn('Comunidad.Nombre')->withColumn('Incidencia.Id')->filterByPrimaryKey($id)->findOne();

        if ($aviso) {
            $array = $aviso->toJSON(false, false);
            echo ($array);
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('avisoGet');

$app->post('/aviso/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $aviso = new Aviso();
        if (isset($post['IncidenciaId'])){
            $aviso->setIncidenciaId($post['IncidenciaId']);
        }
        if (isset($post['PersonaId'])){
            $aviso->setPersonaId($post['PersonaId']);
        }
        if (isset($post['TareaId'])){
            $aviso->setTareaId($post['TareaId']);
        }
        if (isset($post['ComunidadId'])){
            $aviso->setComunidadId($post['ComunidadId']);
        }
        $aviso->setDescripcion($post['Descripcion']);
        $aviso->setNombre($post['Nombre']);
        $aviso->setPrioridad($post['Prioridad']);
        $fecha = new DateTime($post['FechaAviso']);
        $aviso->setFechaAviso($fecha);
        $aviso->setFechaCreacion(new DateTime());
        $aviso->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $aviso->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('avisoCreate');

$app->put('/aviso/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $aviso = AvisoQuery::create()->filterById($id)->findOne();
        if (isset($aviso)) {
            if (isset($post['IncidenciaId'])){
                $aviso->setIncidenciaId($post['IncidenciaId']);
            }
            if (isset($post['PersonaId'])){
                $aviso->setPersonaId($post['PersonaId']);
            }
            if (isset($post['TareaId'])){
                $aviso->setTareaId($post['TareaId']);
            }
            if (isset($post['ComunidadId'])){
                $aviso->setComunidadId($post['ComunidadId']);
            }
            $aviso->setDescripcion($post['Descripcion']);
            $aviso->setNombre($post['Nombre']);
            $aviso->setPrioridad($post['Prioridad']);
            $fecha = new DateTime($post['FechaAviso']);
            $aviso->setFechaAviso($fecha);
            $aviso->setFechaModificacion(new DateTime());
            $aviso->save();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('avisoUpdate');

$app->delete('/aviso/delete/:id', function ($id) use ($app) {
    try {
        $aviso = AvisoQuery::create()->filterById($id)->findOne();
        if ($aviso) {
            $aviso->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('avisoDelete');