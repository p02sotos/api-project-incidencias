<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 21/06/14
 * Time: 0:26
 */


$app->get('/contabilidad', function () use ($app) {
    try {
        $contabilidad = ContabilidadQuery::create()->join('Comunidad')->select(array('Id','Resumen','Notas','FechaEntrega','Marcada','FechaCreacion','Comunidad.Id','Comunidad.Nombre'))->find();
        if ($contabilidad->count() > 0) {
            //$contabilidad->populateRelation('Comunidad');
            $array = $contabilidad->toJSON(false, false);
            echo $array;
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }

})->name('contabilidad');

$app->get('/contabilidad/:id', function ($id) use ($app) {
    try {
        $contabilidad = ContabilidadQuery::create()->join('Comunidad')->select(array('Id','Resumen','Notas','FechaEntrega','Marcada','FechaCreacion','Comunidad.Id','Comunidad.Nombre'))->filterById($id)->findOne();

        if ($contabilidad) {
            $array = $contabilidad;//$contabilidad->toJSON(false, false);
            echo (json_encode($array));
        } else {
            $array = array();
            echo json_encode($array);
        }
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('contabilidadGet');

$app->get('/contabilidad/between/:inicio/:final', function ($inicio, $final) use($app) {
    $today = new DateTime();

    $contabilidads = ContabilidadQuery::create()->join('Comunidad')->withColumn('Comunidad.Id','Comunidad.Id')->withColumn('Comunidad.Nombre','Comunidad.Nombre')->filterByFechaEntrega(array('min'=>strtotime($inicio),'max'=>strtotime($final)))->find();
    if ($contabilidads->count() > 0) {
        $array = $contabilidads->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('contabilidadBetween');

$app->get('/contabilidad/search/:date', function ($date) use($app) {
    var_dump($date);
    $parsedDate = new DateTime($date);
    $contabilidads = ContabilidadQuery::create()->filterByFechaContabilidad($parsedDate)->find();
    if ($contabilidads->count() > 0) {
        $array = $contabilidads->toJSON(false,false);
        echo $array;
    } else {
        $array = array();
        echo json_encode($array);
    }
})->name('contabilidadSearch');
$app->post('/contabilidad/create/', function () use ($app) {
    try {
        $request = $app->request();
        $post = json_decode($request->getBody(), true);
        $contabilidad = new Contabilidad();
        $contabilidad->setResumen($post['Resumen']);
        $contabilidad->setNotas($post['Notas']);
        $contabilidad->setFechaCreacion(new DateTime());
        $fecha = new DateTime($post['FechaEntrega']);
        $contabilidad->setFechaEntrega($fecha);
        $contabilidad->setMarcada(0);
        $contabilidad->setComunidadId($post['ComunidadId']);
        $contabilidad->save();
        $app->response()->header('Content-Type', 'application/json');
        echo $contabilidad->toJSON(false, false);

    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('contabilidadCreate');

$app->put('/contabilidad/:id', function ($id) use ($app) {
    try {
        $request = $app->request();
        $postDirty = $request->getBody();
        //$post = html_entity_decode($postDirty);
        $post = json_decode($request->getBody(), true);
        $contabilidad = ContabilidadQuery::create()->filterById($id)->findOne();
        if (isset($contabilidad)) {
            $contabilidad->setResumen($post['Resumen']);
            $contabilidad->setNotas($post['Notas']);
            $contabilidad->setFechaModificacion(new DateTime());
            $fecha = new DateTime($post['FechaEntrega']);
            $contabilidad->setFechaEntrega($fecha);
            $contabilidad->setComunidadId($post['Comunidad.Id']);
            $contabilidad->save();
        } else {
            throw new Exception();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('contabilidadUpdate');

$app->get('/contabilidad/marcar/:id', function ($id) use ($app) {
    try {
        $contabilidad = ContabilidadQuery::create()->filterById($id)->findOne();
        if ($contabilidad) {
            if ($contabilidad->getMarcada()){
                $contabilidad->setMarcada(false);
            }else {
                $contabilidad->setMarcada(true);
            }
            $contabilidad->save();
            echo "OK";


        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('contabilidadMarcar');

$app->delete('/contabilidad/delete/:id', function ($id) use ($app) {
    try {
        $contabilidad = ContabilidadQuery::create()->filterById($id)->findOne();
        if ($contabilidad) {
            $contabilidad->delete();
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
        $app->response()->header('X-Status-Reason', "No encontrado el recurso");
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
})->name('contabilidadDelete');