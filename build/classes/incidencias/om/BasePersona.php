<?php


/**
 * Base class that represents a row from the 'persona' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BasePersona extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'PersonaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PersonaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the direccion field.
     * @var        string
     */
    protected $direccion;

    /**
     * The value for the marcada field.
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the nota field.
     * @var        string
     */
    protected $nota;

    /**
     * The value for the comunidad_id field.
     * @var        int
     */
    protected $comunidad_id;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * @var        Comunidad
     */
    protected $aComunidad;

    /**
     * @var        PropelObjectCollection|Incidencia[] Collection to store aggregation of Incidencia objects.
     */
    protected $collIncidenciaPers;
    protected $collIncidenciaPersPartial;

    /**
     * @var        PropelObjectCollection|Aviso[] Collection to store aggregation of Aviso objects.
     */
    protected $collAvisoPers;
    protected $collAvisoPersPartial;

    /**
     * @var        PropelObjectCollection|Telefono[] Collection to store aggregation of Telefono objects.
     */
    protected $collTelefonoPersonas;
    protected $collTelefonoPersonasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $incidenciaPersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $avisoPersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $telefonoPersonasScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     * 
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [direccion] column value.
     * 
     * @return string
     */
    public function getDireccion()
    {

        return $this->direccion;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [nota] column value.
     * 
     * @return string
     */
    public function getNota()
    {

        return $this->nota;
    }

    /**
     * Get the [comunidad_id] column value.
     * 
     * @return int
     */
    public function getComunidadId()
    {

        return $this->comunidad_id;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Persona The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = PersonaPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     * 
     * @param  string $v new value
     * @return Persona The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = PersonaPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [direccion] column.
     * 
     * @param  string $v new value
     * @return Persona The current object (for fluent API support)
     */
    public function setDireccion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->direccion !== $v) {
            $this->direccion = $v;
            $this->modifiedColumns[] = PersonaPeer::DIRECCION;
        }


        return $this;
    } // setDireccion()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Persona The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = PersonaPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Set the value of [nota] column.
     * 
     * @param  string $v new value
     * @return Persona The current object (for fluent API support)
     */
    public function setNota($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nota !== $v) {
            $this->nota = $v;
            $this->modifiedColumns[] = PersonaPeer::NOTA;
        }


        return $this;
    } // setNota()

    /**
     * Set the value of [comunidad_id] column.
     * 
     * @param  int $v new value
     * @return Persona The current object (for fluent API support)
     */
    public function setComunidadId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comunidad_id !== $v) {
            $this->comunidad_id = $v;
            $this->modifiedColumns[] = PersonaPeer::COMUNIDAD_ID;
        }

        if ($this->aComunidad !== null && $this->aComunidad->getId() !== $v) {
            $this->aComunidad = null;
        }


        return $this;
    } // setComunidadId()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Persona The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = PersonaPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Persona The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = PersonaPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->direccion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->marcada = ($row[$startcol + 3] !== null) ? (boolean) $row[$startcol + 3] : null;
            $this->nota = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->comunidad_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->fecha_creacion = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->fecha_modificacion = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 8; // 8 = PersonaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Persona object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aComunidad !== null && $this->comunidad_id !== $this->aComunidad->getId()) {
            $this->aComunidad = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PersonaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PersonaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aComunidad = null;
            $this->collIncidenciaPers = null;

            $this->collAvisoPers = null;

            $this->collTelefonoPersonas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PersonaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PersonaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PersonaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PersonaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if ($this->aComunidad->isModified() || $this->aComunidad->isNew()) {
                    $affectedRows += $this->aComunidad->save($con);
                }
                $this->setComunidad($this->aComunidad);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->incidenciaPersScheduledForDeletion !== null) {
                if (!$this->incidenciaPersScheduledForDeletion->isEmpty()) {
                    foreach ($this->incidenciaPersScheduledForDeletion as $incidenciaPer) {
                        // need to save related object because we set the relation to null
                        $incidenciaPer->save($con);
                    }
                    $this->incidenciaPersScheduledForDeletion = null;
                }
            }

            if ($this->collIncidenciaPers !== null) {
                foreach ($this->collIncidenciaPers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->avisoPersScheduledForDeletion !== null) {
                if (!$this->avisoPersScheduledForDeletion->isEmpty()) {
                    foreach ($this->avisoPersScheduledForDeletion as $avisoPer) {
                        // need to save related object because we set the relation to null
                        $avisoPer->save($con);
                    }
                    $this->avisoPersScheduledForDeletion = null;
                }
            }

            if ($this->collAvisoPers !== null) {
                foreach ($this->collAvisoPers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->telefonoPersonasScheduledForDeletion !== null) {
                if (!$this->telefonoPersonasScheduledForDeletion->isEmpty()) {
                    foreach ($this->telefonoPersonasScheduledForDeletion as $telefonoPersona) {
                        // need to save related object because we set the relation to null
                        $telefonoPersona->save($con);
                    }
                    $this->telefonoPersonasScheduledForDeletion = null;
                }
            }

            if ($this->collTelefonoPersonas !== null) {
                foreach ($this->collTelefonoPersonas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = PersonaPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PersonaPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PersonaPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(PersonaPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(PersonaPeer::DIRECCION)) {
            $modifiedColumns[':p' . $index++]  = '`direccion`';
        }
        if ($this->isColumnModified(PersonaPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(PersonaPeer::NOTA)) {
            $modifiedColumns[':p' . $index++]  = '`nota`';
        }
        if ($this->isColumnModified(PersonaPeer::COMUNIDAD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`comunidad_id`';
        }
        if ($this->isColumnModified(PersonaPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(PersonaPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }

        $sql = sprintf(
            'INSERT INTO `persona` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':						
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`direccion`':						
                        $stmt->bindValue($identifier, $this->direccion, PDO::PARAM_STR);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`nota`':						
                        $stmt->bindValue($identifier, $this->nota, PDO::PARAM_STR);
                        break;
                    case '`comunidad_id`':						
                        $stmt->bindValue($identifier, $this->comunidad_id, PDO::PARAM_INT);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if (!$this->aComunidad->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aComunidad->getValidationFailures());
                }
            }


            if (($retval = PersonaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collIncidenciaPers !== null) {
                    foreach ($this->collIncidenciaPers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAvisoPers !== null) {
                    foreach ($this->collAvisoPers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTelefonoPersonas !== null) {
                    foreach ($this->collTelefonoPersonas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PersonaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDireccion();
                break;
            case 3:
                return $this->getMarcada();
                break;
            case 4:
                return $this->getNota();
                break;
            case 5:
                return $this->getComunidadId();
                break;
            case 6:
                return $this->getFechaCreacion();
                break;
            case 7:
                return $this->getFechaModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Persona'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Persona'][$this->getPrimaryKey()] = true;
        $keys = PersonaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDireccion(),
            $keys[3] => $this->getMarcada(),
            $keys[4] => $this->getNota(),
            $keys[5] => $this->getComunidadId(),
            $keys[6] => $this->getFechaCreacion(),
            $keys[7] => $this->getFechaModificacion(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aComunidad) {
                $result['Comunidad'] = $this->aComunidad->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collIncidenciaPers) {
                $result['IncidenciaPers'] = $this->collIncidenciaPers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAvisoPers) {
                $result['AvisoPers'] = $this->collAvisoPers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTelefonoPersonas) {
                $result['TelefonoPersonas'] = $this->collTelefonoPersonas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PersonaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDireccion($value);
                break;
            case 3:
                $this->setMarcada($value);
                break;
            case 4:
                $this->setNota($value);
                break;
            case 5:
                $this->setComunidadId($value);
                break;
            case 6:
                $this->setFechaCreacion($value);
                break;
            case 7:
                $this->setFechaModificacion($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PersonaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDireccion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setMarcada($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNota($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setComunidadId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setFechaCreacion($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFechaModificacion($arr[$keys[7]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PersonaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PersonaPeer::ID)) $criteria->add(PersonaPeer::ID, $this->id);
        if ($this->isColumnModified(PersonaPeer::NOMBRE)) $criteria->add(PersonaPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(PersonaPeer::DIRECCION)) $criteria->add(PersonaPeer::DIRECCION, $this->direccion);
        if ($this->isColumnModified(PersonaPeer::MARCADA)) $criteria->add(PersonaPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(PersonaPeer::NOTA)) $criteria->add(PersonaPeer::NOTA, $this->nota);
        if ($this->isColumnModified(PersonaPeer::COMUNIDAD_ID)) $criteria->add(PersonaPeer::COMUNIDAD_ID, $this->comunidad_id);
        if ($this->isColumnModified(PersonaPeer::FECHA_CREACION)) $criteria->add(PersonaPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(PersonaPeer::FECHA_MODIFICACION)) $criteria->add(PersonaPeer::FECHA_MODIFICACION, $this->fecha_modificacion);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PersonaPeer::DATABASE_NAME);
        $criteria->add(PersonaPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Persona (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDireccion($this->getDireccion());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setNota($this->getNota());
        $copyObj->setComunidadId($this->getComunidadId());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getIncidenciaPers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIncidenciaPer($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAvisoPers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAvisoPer($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTelefonoPersonas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTelefonoPersona($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Persona Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PersonaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PersonaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Comunidad object.
     *
     * @param                  Comunidad $v
     * @return Persona The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComunidad(Comunidad $v = null)
    {
        if ($v === null) {
            $this->setComunidadId(NULL);
        } else {
            $this->setComunidadId($v->getId());
        }

        $this->aComunidad = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Comunidad object, it will not be re-added.
        if ($v !== null) {
            $v->addPersona($this);
        }


        return $this;
    }


    /**
     * Get the associated Comunidad object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Comunidad The associated Comunidad object.
     * @throws PropelException
     */
    public function getComunidad(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aComunidad === null && ($this->comunidad_id !== null) && $doQuery) {
            $this->aComunidad = ComunidadQuery::create()->findPk($this->comunidad_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComunidad->addPersonas($this);
             */
        }

        return $this->aComunidad;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('IncidenciaPer' == $relationName) {
            $this->initIncidenciaPers();
        }
        if ('AvisoPer' == $relationName) {
            $this->initAvisoPers();
        }
        if ('TelefonoPersona' == $relationName) {
            $this->initTelefonoPersonas();
        }
    }

    /**
     * Clears out the collIncidenciaPers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Persona The current object (for fluent API support)
     * @see        addIncidenciaPers()
     */
    public function clearIncidenciaPers()
    {
        $this->collIncidenciaPers = null; // important to set this to null since that means it is uninitialized
        $this->collIncidenciaPersPartial = null;

        return $this;
    }

    /**
     * reset is the collIncidenciaPers collection loaded partially
     *
     * @return void
     */
    public function resetPartialIncidenciaPers($v = true)
    {
        $this->collIncidenciaPersPartial = $v;
    }

    /**
     * Initializes the collIncidenciaPers collection.
     *
     * By default this just sets the collIncidenciaPers collection to an empty array (like clearcollIncidenciaPers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIncidenciaPers($overrideExisting = true)
    {
        if (null !== $this->collIncidenciaPers && !$overrideExisting) {
            return;
        }
        $this->collIncidenciaPers = new PropelObjectCollection();
        $this->collIncidenciaPers->setModel('Incidencia');
    }

    /**
     * Gets an array of Incidencia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Persona is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Incidencia[] List of Incidencia objects
     * @throws PropelException
     */
    public function getIncidenciaPers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collIncidenciaPersPartial && !$this->isNew();
        if (null === $this->collIncidenciaPers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIncidenciaPers) {
                // return empty collection
                $this->initIncidenciaPers();
            } else {
                $collIncidenciaPers = IncidenciaQuery::create(null, $criteria)
                    ->filterByPersona($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collIncidenciaPersPartial && count($collIncidenciaPers)) {
                      $this->initIncidenciaPers(false);

                      foreach ($collIncidenciaPers as $obj) {
                        if (false == $this->collIncidenciaPers->contains($obj)) {
                          $this->collIncidenciaPers->append($obj);
                        }
                      }

                      $this->collIncidenciaPersPartial = true;
                    }

                    $collIncidenciaPers->getInternalIterator()->rewind();

                    return $collIncidenciaPers;
                }

                if ($partial && $this->collIncidenciaPers) {
                    foreach ($this->collIncidenciaPers as $obj) {
                        if ($obj->isNew()) {
                            $collIncidenciaPers[] = $obj;
                        }
                    }
                }

                $this->collIncidenciaPers = $collIncidenciaPers;
                $this->collIncidenciaPersPartial = false;
            }
        }

        return $this->collIncidenciaPers;
    }

    /**
     * Sets a collection of IncidenciaPer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $incidenciaPers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Persona The current object (for fluent API support)
     */
    public function setIncidenciaPers(PropelCollection $incidenciaPers, PropelPDO $con = null)
    {
        $incidenciaPersToDelete = $this->getIncidenciaPers(new Criteria(), $con)->diff($incidenciaPers);


        $this->incidenciaPersScheduledForDeletion = $incidenciaPersToDelete;

        foreach ($incidenciaPersToDelete as $incidenciaPerRemoved) {
            $incidenciaPerRemoved->setPersona(null);
        }

        $this->collIncidenciaPers = null;
        foreach ($incidenciaPers as $incidenciaPer) {
            $this->addIncidenciaPer($incidenciaPer);
        }

        $this->collIncidenciaPers = $incidenciaPers;
        $this->collIncidenciaPersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Incidencia objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Incidencia objects.
     * @throws PropelException
     */
    public function countIncidenciaPers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collIncidenciaPersPartial && !$this->isNew();
        if (null === $this->collIncidenciaPers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIncidenciaPers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIncidenciaPers());
            }
            $query = IncidenciaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersona($this)
                ->count($con);
        }

        return count($this->collIncidenciaPers);
    }

    /**
     * Method called to associate a Incidencia object to this object
     * through the Incidencia foreign key attribute.
     *
     * @param    Incidencia $l Incidencia
     * @return Persona The current object (for fluent API support)
     */
    public function addIncidenciaPer(Incidencia $l)
    {
        if ($this->collIncidenciaPers === null) {
            $this->initIncidenciaPers();
            $this->collIncidenciaPersPartial = true;
        }

        if (!in_array($l, $this->collIncidenciaPers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddIncidenciaPer($l);

            if ($this->incidenciaPersScheduledForDeletion and $this->incidenciaPersScheduledForDeletion->contains($l)) {
                $this->incidenciaPersScheduledForDeletion->remove($this->incidenciaPersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	IncidenciaPer $incidenciaPer The incidenciaPer object to add.
     */
    protected function doAddIncidenciaPer($incidenciaPer)
    {
        $this->collIncidenciaPers[]= $incidenciaPer;
        $incidenciaPer->setPersona($this);
    }

    /**
     * @param	IncidenciaPer $incidenciaPer The incidenciaPer object to remove.
     * @return Persona The current object (for fluent API support)
     */
    public function removeIncidenciaPer($incidenciaPer)
    {
        if ($this->getIncidenciaPers()->contains($incidenciaPer)) {
            $this->collIncidenciaPers->remove($this->collIncidenciaPers->search($incidenciaPer));
            if (null === $this->incidenciaPersScheduledForDeletion) {
                $this->incidenciaPersScheduledForDeletion = clone $this->collIncidenciaPers;
                $this->incidenciaPersScheduledForDeletion->clear();
            }
            $this->incidenciaPersScheduledForDeletion[]= $incidenciaPer;
            $incidenciaPer->setPersona(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related IncidenciaPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Incidencia[] List of Incidencia objects
     */
    public function getIncidenciaPersJoinComunidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = IncidenciaQuery::create(null, $criteria);
        $query->joinWith('Comunidad', $join_behavior);

        return $this->getIncidenciaPers($query, $con);
    }

    /**
     * Clears out the collAvisoPers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Persona The current object (for fluent API support)
     * @see        addAvisoPers()
     */
    public function clearAvisoPers()
    {
        $this->collAvisoPers = null; // important to set this to null since that means it is uninitialized
        $this->collAvisoPersPartial = null;

        return $this;
    }

    /**
     * reset is the collAvisoPers collection loaded partially
     *
     * @return void
     */
    public function resetPartialAvisoPers($v = true)
    {
        $this->collAvisoPersPartial = $v;
    }

    /**
     * Initializes the collAvisoPers collection.
     *
     * By default this just sets the collAvisoPers collection to an empty array (like clearcollAvisoPers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAvisoPers($overrideExisting = true)
    {
        if (null !== $this->collAvisoPers && !$overrideExisting) {
            return;
        }
        $this->collAvisoPers = new PropelObjectCollection();
        $this->collAvisoPers->setModel('Aviso');
    }

    /**
     * Gets an array of Aviso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Persona is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     * @throws PropelException
     */
    public function getAvisoPers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAvisoPersPartial && !$this->isNew();
        if (null === $this->collAvisoPers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAvisoPers) {
                // return empty collection
                $this->initAvisoPers();
            } else {
                $collAvisoPers = AvisoQuery::create(null, $criteria)
                    ->filterByPersona($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAvisoPersPartial && count($collAvisoPers)) {
                      $this->initAvisoPers(false);

                      foreach ($collAvisoPers as $obj) {
                        if (false == $this->collAvisoPers->contains($obj)) {
                          $this->collAvisoPers->append($obj);
                        }
                      }

                      $this->collAvisoPersPartial = true;
                    }

                    $collAvisoPers->getInternalIterator()->rewind();

                    return $collAvisoPers;
                }

                if ($partial && $this->collAvisoPers) {
                    foreach ($this->collAvisoPers as $obj) {
                        if ($obj->isNew()) {
                            $collAvisoPers[] = $obj;
                        }
                    }
                }

                $this->collAvisoPers = $collAvisoPers;
                $this->collAvisoPersPartial = false;
            }
        }

        return $this->collAvisoPers;
    }

    /**
     * Sets a collection of AvisoPer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $avisoPers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Persona The current object (for fluent API support)
     */
    public function setAvisoPers(PropelCollection $avisoPers, PropelPDO $con = null)
    {
        $avisoPersToDelete = $this->getAvisoPers(new Criteria(), $con)->diff($avisoPers);


        $this->avisoPersScheduledForDeletion = $avisoPersToDelete;

        foreach ($avisoPersToDelete as $avisoPerRemoved) {
            $avisoPerRemoved->setPersona(null);
        }

        $this->collAvisoPers = null;
        foreach ($avisoPers as $avisoPer) {
            $this->addAvisoPer($avisoPer);
        }

        $this->collAvisoPers = $avisoPers;
        $this->collAvisoPersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Aviso objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Aviso objects.
     * @throws PropelException
     */
    public function countAvisoPers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAvisoPersPartial && !$this->isNew();
        if (null === $this->collAvisoPers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAvisoPers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAvisoPers());
            }
            $query = AvisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersona($this)
                ->count($con);
        }

        return count($this->collAvisoPers);
    }

    /**
     * Method called to associate a Aviso object to this object
     * through the Aviso foreign key attribute.
     *
     * @param    Aviso $l Aviso
     * @return Persona The current object (for fluent API support)
     */
    public function addAvisoPer(Aviso $l)
    {
        if ($this->collAvisoPers === null) {
            $this->initAvisoPers();
            $this->collAvisoPersPartial = true;
        }

        if (!in_array($l, $this->collAvisoPers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAvisoPer($l);

            if ($this->avisoPersScheduledForDeletion and $this->avisoPersScheduledForDeletion->contains($l)) {
                $this->avisoPersScheduledForDeletion->remove($this->avisoPersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	AvisoPer $avisoPer The avisoPer object to add.
     */
    protected function doAddAvisoPer($avisoPer)
    {
        $this->collAvisoPers[]= $avisoPer;
        $avisoPer->setPersona($this);
    }

    /**
     * @param	AvisoPer $avisoPer The avisoPer object to remove.
     * @return Persona The current object (for fluent API support)
     */
    public function removeAvisoPer($avisoPer)
    {
        if ($this->getAvisoPers()->contains($avisoPer)) {
            $this->collAvisoPers->remove($this->collAvisoPers->search($avisoPer));
            if (null === $this->avisoPersScheduledForDeletion) {
                $this->avisoPersScheduledForDeletion = clone $this->collAvisoPers;
                $this->avisoPersScheduledForDeletion->clear();
            }
            $this->avisoPersScheduledForDeletion[]= $avisoPer;
            $avisoPer->setPersona(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related AvisoPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoPersJoinIncidencia($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Incidencia', $join_behavior);

        return $this->getAvisoPers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related AvisoPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoPersJoinTarea($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Tarea', $join_behavior);

        return $this->getAvisoPers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related AvisoPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoPersJoinComunidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Comunidad', $join_behavior);

        return $this->getAvisoPers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related AvisoPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoPersJoinEntrega($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Entrega', $join_behavior);

        return $this->getAvisoPers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related AvisoPers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoPersJoinContabilidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Contabilidad', $join_behavior);

        return $this->getAvisoPers($query, $con);
    }

    /**
     * Clears out the collTelefonoPersonas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Persona The current object (for fluent API support)
     * @see        addTelefonoPersonas()
     */
    public function clearTelefonoPersonas()
    {
        $this->collTelefonoPersonas = null; // important to set this to null since that means it is uninitialized
        $this->collTelefonoPersonasPartial = null;

        return $this;
    }

    /**
     * reset is the collTelefonoPersonas collection loaded partially
     *
     * @return void
     */
    public function resetPartialTelefonoPersonas($v = true)
    {
        $this->collTelefonoPersonasPartial = $v;
    }

    /**
     * Initializes the collTelefonoPersonas collection.
     *
     * By default this just sets the collTelefonoPersonas collection to an empty array (like clearcollTelefonoPersonas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTelefonoPersonas($overrideExisting = true)
    {
        if (null !== $this->collTelefonoPersonas && !$overrideExisting) {
            return;
        }
        $this->collTelefonoPersonas = new PropelObjectCollection();
        $this->collTelefonoPersonas->setModel('Telefono');
    }

    /**
     * Gets an array of Telefono objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Persona is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     * @throws PropelException
     */
    public function getTelefonoPersonas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoPersonasPartial && !$this->isNew();
        if (null === $this->collTelefonoPersonas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTelefonoPersonas) {
                // return empty collection
                $this->initTelefonoPersonas();
            } else {
                $collTelefonoPersonas = TelefonoQuery::create(null, $criteria)
                    ->filterByPersona($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTelefonoPersonasPartial && count($collTelefonoPersonas)) {
                      $this->initTelefonoPersonas(false);

                      foreach ($collTelefonoPersonas as $obj) {
                        if (false == $this->collTelefonoPersonas->contains($obj)) {
                          $this->collTelefonoPersonas->append($obj);
                        }
                      }

                      $this->collTelefonoPersonasPartial = true;
                    }

                    $collTelefonoPersonas->getInternalIterator()->rewind();

                    return $collTelefonoPersonas;
                }

                if ($partial && $this->collTelefonoPersonas) {
                    foreach ($this->collTelefonoPersonas as $obj) {
                        if ($obj->isNew()) {
                            $collTelefonoPersonas[] = $obj;
                        }
                    }
                }

                $this->collTelefonoPersonas = $collTelefonoPersonas;
                $this->collTelefonoPersonasPartial = false;
            }
        }

        return $this->collTelefonoPersonas;
    }

    /**
     * Sets a collection of TelefonoPersona objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $telefonoPersonas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Persona The current object (for fluent API support)
     */
    public function setTelefonoPersonas(PropelCollection $telefonoPersonas, PropelPDO $con = null)
    {
        $telefonoPersonasToDelete = $this->getTelefonoPersonas(new Criteria(), $con)->diff($telefonoPersonas);


        $this->telefonoPersonasScheduledForDeletion = $telefonoPersonasToDelete;

        foreach ($telefonoPersonasToDelete as $telefonoPersonaRemoved) {
            $telefonoPersonaRemoved->setPersona(null);
        }

        $this->collTelefonoPersonas = null;
        foreach ($telefonoPersonas as $telefonoPersona) {
            $this->addTelefonoPersona($telefonoPersona);
        }

        $this->collTelefonoPersonas = $telefonoPersonas;
        $this->collTelefonoPersonasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Telefono objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Telefono objects.
     * @throws PropelException
     */
    public function countTelefonoPersonas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoPersonasPartial && !$this->isNew();
        if (null === $this->collTelefonoPersonas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTelefonoPersonas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTelefonoPersonas());
            }
            $query = TelefonoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersona($this)
                ->count($con);
        }

        return count($this->collTelefonoPersonas);
    }

    /**
     * Method called to associate a Telefono object to this object
     * through the Telefono foreign key attribute.
     *
     * @param    Telefono $l Telefono
     * @return Persona The current object (for fluent API support)
     */
    public function addTelefonoPersona(Telefono $l)
    {
        if ($this->collTelefonoPersonas === null) {
            $this->initTelefonoPersonas();
            $this->collTelefonoPersonasPartial = true;
        }

        if (!in_array($l, $this->collTelefonoPersonas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTelefonoPersona($l);

            if ($this->telefonoPersonasScheduledForDeletion and $this->telefonoPersonasScheduledForDeletion->contains($l)) {
                $this->telefonoPersonasScheduledForDeletion->remove($this->telefonoPersonasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	TelefonoPersona $telefonoPersona The telefonoPersona object to add.
     */
    protected function doAddTelefonoPersona($telefonoPersona)
    {
        $this->collTelefonoPersonas[]= $telefonoPersona;
        $telefonoPersona->setPersona($this);
    }

    /**
     * @param	TelefonoPersona $telefonoPersona The telefonoPersona object to remove.
     * @return Persona The current object (for fluent API support)
     */
    public function removeTelefonoPersona($telefonoPersona)
    {
        if ($this->getTelefonoPersonas()->contains($telefonoPersona)) {
            $this->collTelefonoPersonas->remove($this->collTelefonoPersonas->search($telefonoPersona));
            if (null === $this->telefonoPersonasScheduledForDeletion) {
                $this->telefonoPersonasScheduledForDeletion = clone $this->collTelefonoPersonas;
                $this->telefonoPersonasScheduledForDeletion->clear();
            }
            $this->telefonoPersonasScheduledForDeletion[]= $telefonoPersona;
            $telefonoPersona->setPersona(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related TelefonoPersonas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoPersonasJoinIncidencia($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Incidencia', $join_behavior);

        return $this->getTelefonoPersonas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Persona is new, it will return
     * an empty collection; or if this Persona has previously
     * been saved, it will retrieve related TelefonoPersonas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Persona.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoPersonasJoinEmpresa($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Empresa', $join_behavior);

        return $this->getTelefonoPersonas($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->direccion = null;
        $this->marcada = null;
        $this->nota = null;
        $this->comunidad_id = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collIncidenciaPers) {
                foreach ($this->collIncidenciaPers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAvisoPers) {
                foreach ($this->collAvisoPers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTelefonoPersonas) {
                foreach ($this->collTelefonoPersonas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aComunidad instanceof Persistent) {
              $this->aComunidad->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collIncidenciaPers instanceof PropelCollection) {
            $this->collIncidenciaPers->clearIterator();
        }
        $this->collIncidenciaPers = null;
        if ($this->collAvisoPers instanceof PropelCollection) {
            $this->collAvisoPers->clearIterator();
        }
        $this->collAvisoPers = null;
        if ($this->collTelefonoPersonas instanceof PropelCollection) {
            $this->collTelefonoPersonas->clearIterator();
        }
        $this->collTelefonoPersonas = null;
        $this->aComunidad = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PersonaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
