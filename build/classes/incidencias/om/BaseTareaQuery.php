<?php


/**
 * Base class that represents a query for the 'tarea' table.
 *
 * 
 *
 * @method TareaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method TareaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method TareaQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method TareaQuery orderByPrioridad($order = Criteria::ASC) Order by the prioridad column
 * @method TareaQuery orderByOrden($order = Criteria::ASC) Order by the orden column
 * @method TareaQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method TareaQuery orderByEliminado($order = Criteria::ASC) Order by the eliminado column
 * @method TareaQuery orderByRealizada($order = Criteria::ASC) Order by the realizada column
 * @method TareaQuery orderByFechaTarea($order = Criteria::ASC) Order by the fecha_tarea column
 * @method TareaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method TareaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method TareaQuery orderByIncidenciaId($order = Criteria::ASC) Order by the incidencia_id column
 *
 * @method TareaQuery groupById() Group by the id column
 * @method TareaQuery groupByNombre() Group by the nombre column
 * @method TareaQuery groupByDescripcion() Group by the descripcion column
 * @method TareaQuery groupByPrioridad() Group by the prioridad column
 * @method TareaQuery groupByOrden() Group by the orden column
 * @method TareaQuery groupByMarcada() Group by the marcada column
 * @method TareaQuery groupByEliminado() Group by the eliminado column
 * @method TareaQuery groupByRealizada() Group by the realizada column
 * @method TareaQuery groupByFechaTarea() Group by the fecha_tarea column
 * @method TareaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method TareaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method TareaQuery groupByIncidenciaId() Group by the incidencia_id column
 *
 * @method TareaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TareaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TareaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method TareaQuery leftJoinIncidencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Incidencia relation
 * @method TareaQuery rightJoinIncidencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Incidencia relation
 * @method TareaQuery innerJoinIncidencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Incidencia relation
 *
 * @method TareaQuery leftJoinAvisoTar($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvisoTar relation
 * @method TareaQuery rightJoinAvisoTar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvisoTar relation
 * @method TareaQuery innerJoinAvisoTar($relationAlias = null) Adds a INNER JOIN clause to the query using the AvisoTar relation
 *
 * @method Tarea findOne(PropelPDO $con = null) Return the first Tarea matching the query
 * @method Tarea findOneOrCreate(PropelPDO $con = null) Return the first Tarea matching the query, or a new Tarea object populated from the query conditions when no match is found
 *
 * @method Tarea findOneByNombre(string $nombre) Return the first Tarea filtered by the nombre column
 * @method Tarea findOneByDescripcion(string $descripcion) Return the first Tarea filtered by the descripcion column
 * @method Tarea findOneByPrioridad(int $prioridad) Return the first Tarea filtered by the prioridad column
 * @method Tarea findOneByOrden(int $orden) Return the first Tarea filtered by the orden column
 * @method Tarea findOneByMarcada(boolean $marcada) Return the first Tarea filtered by the marcada column
 * @method Tarea findOneByEliminado(boolean $eliminado) Return the first Tarea filtered by the eliminado column
 * @method Tarea findOneByRealizada(boolean $realizada) Return the first Tarea filtered by the realizada column
 * @method Tarea findOneByFechaTarea(string $fecha_tarea) Return the first Tarea filtered by the fecha_tarea column
 * @method Tarea findOneByFechaCreacion(string $fecha_creacion) Return the first Tarea filtered by the fecha_creacion column
 * @method Tarea findOneByFechaModificacion(string $fecha_modificacion) Return the first Tarea filtered by the fecha_modificacion column
 * @method Tarea findOneByIncidenciaId(int $incidencia_id) Return the first Tarea filtered by the incidencia_id column
 *
 * @method array findById(int $id) Return Tarea objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Tarea objects filtered by the nombre column
 * @method array findByDescripcion(string $descripcion) Return Tarea objects filtered by the descripcion column
 * @method array findByPrioridad(int $prioridad) Return Tarea objects filtered by the prioridad column
 * @method array findByOrden(int $orden) Return Tarea objects filtered by the orden column
 * @method array findByMarcada(boolean $marcada) Return Tarea objects filtered by the marcada column
 * @method array findByEliminado(boolean $eliminado) Return Tarea objects filtered by the eliminado column
 * @method array findByRealizada(boolean $realizada) Return Tarea objects filtered by the realizada column
 * @method array findByFechaTarea(string $fecha_tarea) Return Tarea objects filtered by the fecha_tarea column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Tarea objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Tarea objects filtered by the fecha_modificacion column
 * @method array findByIncidenciaId(int $incidencia_id) Return Tarea objects filtered by the incidencia_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseTareaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTareaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Tarea';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TareaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TareaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TareaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TareaQuery) {
            return $criteria;
        }
        $query = new TareaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Tarea|Tarea[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TareaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TareaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tarea A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Tarea A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `descripcion`, `prioridad`, `orden`, `marcada`, `eliminado`, `realizada`, `fecha_tarea`, `fecha_creacion`, `fecha_modificacion`, `incidencia_id` FROM `tarea` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Tarea();
            $obj->hydrate($row);
            TareaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Tarea|Tarea[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Tarea[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TareaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TareaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TareaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TareaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TareaPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TareaPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the prioridad column
     *
     * Example usage:
     * <code>
     * $query->filterByPrioridad(1234); // WHERE prioridad = 1234
     * $query->filterByPrioridad(array(12, 34)); // WHERE prioridad IN (12, 34)
     * $query->filterByPrioridad(array('min' => 12)); // WHERE prioridad >= 12
     * $query->filterByPrioridad(array('max' => 12)); // WHERE prioridad <= 12
     * </code>
     *
     * @param     mixed $prioridad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByPrioridad($prioridad = null, $comparison = null)
    {
        if (is_array($prioridad)) {
            $useMinMax = false;
            if (isset($prioridad['min'])) {
                $this->addUsingAlias(TareaPeer::PRIORIDAD, $prioridad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prioridad['max'])) {
                $this->addUsingAlias(TareaPeer::PRIORIDAD, $prioridad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::PRIORIDAD, $prioridad, $comparison);
    }

    /**
     * Filter the query on the orden column
     *
     * Example usage:
     * <code>
     * $query->filterByOrden(1234); // WHERE orden = 1234
     * $query->filterByOrden(array(12, 34)); // WHERE orden IN (12, 34)
     * $query->filterByOrden(array('min' => 12)); // WHERE orden >= 12
     * $query->filterByOrden(array('max' => 12)); // WHERE orden <= 12
     * </code>
     *
     * @param     mixed $orden The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByOrden($orden = null, $comparison = null)
    {
        if (is_array($orden)) {
            $useMinMax = false;
            if (isset($orden['min'])) {
                $this->addUsingAlias(TareaPeer::ORDEN, $orden['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($orden['max'])) {
                $this->addUsingAlias(TareaPeer::ORDEN, $orden['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::ORDEN, $orden, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(TareaPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the eliminado column
     *
     * Example usage:
     * <code>
     * $query->filterByEliminado(true); // WHERE eliminado = true
     * $query->filterByEliminado('yes'); // WHERE eliminado = true
     * </code>
     *
     * @param     boolean|string $eliminado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByEliminado($eliminado = null, $comparison = null)
    {
        if (is_string($eliminado)) {
            $eliminado = in_array(strtolower($eliminado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(TareaPeer::ELIMINADO, $eliminado, $comparison);
    }

    /**
     * Filter the query on the realizada column
     *
     * Example usage:
     * <code>
     * $query->filterByRealizada(true); // WHERE realizada = true
     * $query->filterByRealizada('yes'); // WHERE realizada = true
     * </code>
     *
     * @param     boolean|string $realizada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByRealizada($realizada = null, $comparison = null)
    {
        if (is_string($realizada)) {
            $realizada = in_array(strtolower($realizada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(TareaPeer::REALIZADA, $realizada, $comparison);
    }

    /**
     * Filter the query on the fecha_tarea column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaTarea('2011-03-14'); // WHERE fecha_tarea = '2011-03-14'
     * $query->filterByFechaTarea('now'); // WHERE fecha_tarea = '2011-03-14'
     * $query->filterByFechaTarea(array('max' => 'yesterday')); // WHERE fecha_tarea < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaTarea The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByFechaTarea($fechaTarea = null, $comparison = null)
    {
        if (is_array($fechaTarea)) {
            $useMinMax = false;
            if (isset($fechaTarea['min'])) {
                $this->addUsingAlias(TareaPeer::FECHA_TAREA, $fechaTarea['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaTarea['max'])) {
                $this->addUsingAlias(TareaPeer::FECHA_TAREA, $fechaTarea['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::FECHA_TAREA, $fechaTarea, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(TareaPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(TareaPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(TareaPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(TareaPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the incidencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIncidenciaId(1234); // WHERE incidencia_id = 1234
     * $query->filterByIncidenciaId(array(12, 34)); // WHERE incidencia_id IN (12, 34)
     * $query->filterByIncidenciaId(array('min' => 12)); // WHERE incidencia_id >= 12
     * $query->filterByIncidenciaId(array('max' => 12)); // WHERE incidencia_id <= 12
     * </code>
     *
     * @see       filterByIncidencia()
     *
     * @param     mixed $incidenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function filterByIncidenciaId($incidenciaId = null, $comparison = null)
    {
        if (is_array($incidenciaId)) {
            $useMinMax = false;
            if (isset($incidenciaId['min'])) {
                $this->addUsingAlias(TareaPeer::INCIDENCIA_ID, $incidenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($incidenciaId['max'])) {
                $this->addUsingAlias(TareaPeer::INCIDENCIA_ID, $incidenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TareaPeer::INCIDENCIA_ID, $incidenciaId, $comparison);
    }

    /**
     * Filter the query by a related Incidencia object
     *
     * @param   Incidencia|PropelObjectCollection $incidencia The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TareaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIncidencia($incidencia, $comparison = null)
    {
        if ($incidencia instanceof Incidencia) {
            return $this
                ->addUsingAlias(TareaPeer::INCIDENCIA_ID, $incidencia->getId(), $comparison);
        } elseif ($incidencia instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TareaPeer::INCIDENCIA_ID, $incidencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByIncidencia() only accepts arguments of type Incidencia or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Incidencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function joinIncidencia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Incidencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Incidencia');
        }

        return $this;
    }

    /**
     * Use the Incidencia relation Incidencia object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   IncidenciaQuery A secondary query class using the current class as primary query
     */
    public function useIncidenciaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIncidencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Incidencia', 'IncidenciaQuery');
    }

    /**
     * Filter the query by a related Aviso object
     *
     * @param   Aviso|PropelObjectCollection $aviso  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TareaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAvisoTar($aviso, $comparison = null)
    {
        if ($aviso instanceof Aviso) {
            return $this
                ->addUsingAlias(TareaPeer::ID, $aviso->getTareaId(), $comparison);
        } elseif ($aviso instanceof PropelObjectCollection) {
            return $this
                ->useAvisoTarQuery()
                ->filterByPrimaryKeys($aviso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvisoTar() only accepts arguments of type Aviso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvisoTar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function joinAvisoTar($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvisoTar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvisoTar');
        }

        return $this;
    }

    /**
     * Use the AvisoTar relation Aviso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AvisoQuery A secondary query class using the current class as primary query
     */
    public function useAvisoTarQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvisoTar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvisoTar', 'AvisoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Tarea $tarea Object to remove from the list of results
     *
     * @return TareaQuery The current query, for fluid interface
     */
    public function prune($tarea = null)
    {
        if ($tarea) {
            $this->addUsingAlias(TareaPeer::ID, $tarea->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
