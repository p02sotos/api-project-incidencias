<?php


/**
 * Base class that represents a query for the 'empresa' table.
 *
 * 
 *
 * @method EmpresaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method EmpresaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method EmpresaQuery orderByDireccion($order = Criteria::ASC) Order by the direccion column
 * @method EmpresaQuery orderByCif($order = Criteria::ASC) Order by the cif column
 * @method EmpresaQuery orderByContacto($order = Criteria::ASC) Order by the contacto column
 * @method EmpresaQuery orderByObservaciones($order = Criteria::ASC) Order by the observaciones column
 * @method EmpresaQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method EmpresaQuery orderByComunidadId($order = Criteria::ASC) Order by the comunidad_id column
 * @method EmpresaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method EmpresaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 *
 * @method EmpresaQuery groupById() Group by the id column
 * @method EmpresaQuery groupByNombre() Group by the nombre column
 * @method EmpresaQuery groupByDireccion() Group by the direccion column
 * @method EmpresaQuery groupByCif() Group by the cif column
 * @method EmpresaQuery groupByContacto() Group by the contacto column
 * @method EmpresaQuery groupByObservaciones() Group by the observaciones column
 * @method EmpresaQuery groupByMarcada() Group by the marcada column
 * @method EmpresaQuery groupByComunidadId() Group by the comunidad_id column
 * @method EmpresaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method EmpresaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 *
 * @method EmpresaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method EmpresaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method EmpresaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method EmpresaQuery leftJoinComunidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comunidad relation
 * @method EmpresaQuery rightJoinComunidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comunidad relation
 * @method EmpresaQuery innerJoinComunidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Comunidad relation
 *
 * @method EmpresaQuery leftJoinTelefonoEmpresa($relationAlias = null) Adds a LEFT JOIN clause to the query using the TelefonoEmpresa relation
 * @method EmpresaQuery rightJoinTelefonoEmpresa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TelefonoEmpresa relation
 * @method EmpresaQuery innerJoinTelefonoEmpresa($relationAlias = null) Adds a INNER JOIN clause to the query using the TelefonoEmpresa relation
 *
 * @method Empresa findOne(PropelPDO $con = null) Return the first Empresa matching the query
 * @method Empresa findOneOrCreate(PropelPDO $con = null) Return the first Empresa matching the query, or a new Empresa object populated from the query conditions when no match is found
 *
 * @method Empresa findOneByNombre(string $nombre) Return the first Empresa filtered by the nombre column
 * @method Empresa findOneByDireccion(string $direccion) Return the first Empresa filtered by the direccion column
 * @method Empresa findOneByCif(string $cif) Return the first Empresa filtered by the cif column
 * @method Empresa findOneByContacto(string $contacto) Return the first Empresa filtered by the contacto column
 * @method Empresa findOneByObservaciones(string $observaciones) Return the first Empresa filtered by the observaciones column
 * @method Empresa findOneByMarcada(boolean $marcada) Return the first Empresa filtered by the marcada column
 * @method Empresa findOneByComunidadId(int $comunidad_id) Return the first Empresa filtered by the comunidad_id column
 * @method Empresa findOneByFechaCreacion(string $fecha_creacion) Return the first Empresa filtered by the fecha_creacion column
 * @method Empresa findOneByFechaModificacion(string $fecha_modificacion) Return the first Empresa filtered by the fecha_modificacion column
 *
 * @method array findById(int $id) Return Empresa objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Empresa objects filtered by the nombre column
 * @method array findByDireccion(string $direccion) Return Empresa objects filtered by the direccion column
 * @method array findByCif(string $cif) Return Empresa objects filtered by the cif column
 * @method array findByContacto(string $contacto) Return Empresa objects filtered by the contacto column
 * @method array findByObservaciones(string $observaciones) Return Empresa objects filtered by the observaciones column
 * @method array findByMarcada(boolean $marcada) Return Empresa objects filtered by the marcada column
 * @method array findByComunidadId(int $comunidad_id) Return Empresa objects filtered by the comunidad_id column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Empresa objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Empresa objects filtered by the fecha_modificacion column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseEmpresaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseEmpresaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Empresa';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new EmpresaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   EmpresaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return EmpresaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof EmpresaQuery) {
            return $criteria;
        }
        $query = new EmpresaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Empresa|Empresa[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = EmpresaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(EmpresaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Empresa A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Empresa A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `direccion`, `cif`, `contacto`, `observaciones`, `marcada`, `comunidad_id`, `fecha_creacion`, `fecha_modificacion` FROM `empresa` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Empresa();
            $obj->hydrate($row);
            EmpresaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Empresa|Empresa[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Empresa[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EmpresaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EmpresaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EmpresaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EmpresaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the direccion column
     *
     * Example usage:
     * <code>
     * $query->filterByDireccion('fooValue');   // WHERE direccion = 'fooValue'
     * $query->filterByDireccion('%fooValue%'); // WHERE direccion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $direccion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByDireccion($direccion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($direccion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $direccion)) {
                $direccion = str_replace('*', '%', $direccion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::DIRECCION, $direccion, $comparison);
    }

    /**
     * Filter the query on the cif column
     *
     * Example usage:
     * <code>
     * $query->filterByCif('fooValue');   // WHERE cif = 'fooValue'
     * $query->filterByCif('%fooValue%'); // WHERE cif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByCif($cif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cif)) {
                $cif = str_replace('*', '%', $cif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::CIF, $cif, $comparison);
    }

    /**
     * Filter the query on the contacto column
     *
     * Example usage:
     * <code>
     * $query->filterByContacto('fooValue');   // WHERE contacto = 'fooValue'
     * $query->filterByContacto('%fooValue%'); // WHERE contacto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contacto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByContacto($contacto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contacto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contacto)) {
                $contacto = str_replace('*', '%', $contacto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::CONTACTO, $contacto, $comparison);
    }

    /**
     * Filter the query on the observaciones column
     *
     * Example usage:
     * <code>
     * $query->filterByObservaciones('fooValue');   // WHERE observaciones = 'fooValue'
     * $query->filterByObservaciones('%fooValue%'); // WHERE observaciones LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observaciones The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByObservaciones($observaciones = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observaciones)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $observaciones)) {
                $observaciones = str_replace('*', '%', $observaciones);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::OBSERVACIONES, $observaciones, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EmpresaPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the comunidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidadId(1234); // WHERE comunidad_id = 1234
     * $query->filterByComunidadId(array(12, 34)); // WHERE comunidad_id IN (12, 34)
     * $query->filterByComunidadId(array('min' => 12)); // WHERE comunidad_id >= 12
     * $query->filterByComunidadId(array('max' => 12)); // WHERE comunidad_id <= 12
     * </code>
     *
     * @see       filterByComunidad()
     *
     * @param     mixed $comunidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByComunidadId($comunidadId = null, $comparison = null)
    {
        if (is_array($comunidadId)) {
            $useMinMax = false;
            if (isset($comunidadId['min'])) {
                $this->addUsingAlias(EmpresaPeer::COMUNIDAD_ID, $comunidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comunidadId['max'])) {
                $this->addUsingAlias(EmpresaPeer::COMUNIDAD_ID, $comunidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::COMUNIDAD_ID, $comunidadId, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(EmpresaPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(EmpresaPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(EmpresaPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(EmpresaPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query by a related Comunidad object
     *
     * @param   Comunidad|PropelObjectCollection $comunidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 EmpresaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComunidad($comunidad, $comparison = null)
    {
        if ($comunidad instanceof Comunidad) {
            return $this
                ->addUsingAlias(EmpresaPeer::COMUNIDAD_ID, $comunidad->getId(), $comparison);
        } elseif ($comunidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EmpresaPeer::COMUNIDAD_ID, $comunidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComunidad() only accepts arguments of type Comunidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comunidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function joinComunidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comunidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comunidad');
        }

        return $this;
    }

    /**
     * Use the Comunidad relation Comunidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComunidadQuery A secondary query class using the current class as primary query
     */
    public function useComunidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComunidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comunidad', 'ComunidadQuery');
    }

    /**
     * Filter the query by a related Telefono object
     *
     * @param   Telefono|PropelObjectCollection $telefono  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 EmpresaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTelefonoEmpresa($telefono, $comparison = null)
    {
        if ($telefono instanceof Telefono) {
            return $this
                ->addUsingAlias(EmpresaPeer::ID, $telefono->getEmpresaId(), $comparison);
        } elseif ($telefono instanceof PropelObjectCollection) {
            return $this
                ->useTelefonoEmpresaQuery()
                ->filterByPrimaryKeys($telefono->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTelefonoEmpresa() only accepts arguments of type Telefono or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TelefonoEmpresa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function joinTelefonoEmpresa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TelefonoEmpresa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TelefonoEmpresa');
        }

        return $this;
    }

    /**
     * Use the TelefonoEmpresa relation Telefono object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TelefonoQuery A secondary query class using the current class as primary query
     */
    public function useTelefonoEmpresaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTelefonoEmpresa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TelefonoEmpresa', 'TelefonoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Empresa $empresa Object to remove from the list of results
     *
     * @return EmpresaQuery The current query, for fluid interface
     */
    public function prune($empresa = null)
    {
        if ($empresa) {
            $this->addUsingAlias(EmpresaPeer::ID, $empresa->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
