<?php


/**
 * Base class that represents a query for the 'nota' table.
 *
 * 
 *
 * @method NotaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method NotaQuery orderByTexto($order = Criteria::ASC) Order by the texto column
 * @method NotaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method NotaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method NotaQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method NotaQuery orderByComunidadId($order = Criteria::ASC) Order by the comunidad_id column
 * @method NotaQuery orderByPersonaId($order = Criteria::ASC) Order by the persona_id column
 * @method NotaQuery orderByIncidenciaId($order = Criteria::ASC) Order by the incidencia_id column
 *
 * @method NotaQuery groupById() Group by the id column
 * @method NotaQuery groupByTexto() Group by the texto column
 * @method NotaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method NotaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method NotaQuery groupByMarcada() Group by the marcada column
 * @method NotaQuery groupByComunidadId() Group by the comunidad_id column
 * @method NotaQuery groupByPersonaId() Group by the persona_id column
 * @method NotaQuery groupByIncidenciaId() Group by the incidencia_id column
 *
 * @method NotaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method NotaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method NotaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method NotaQuery leftJoinComunidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comunidad relation
 * @method NotaQuery rightJoinComunidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comunidad relation
 * @method NotaQuery innerJoinComunidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Comunidad relation
 *
 * @method NotaQuery leftJoinIncidencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Incidencia relation
 * @method NotaQuery rightJoinIncidencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Incidencia relation
 * @method NotaQuery innerJoinIncidencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Incidencia relation
 *
 * @method NotaQuery leftJoinPersona($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persona relation
 * @method NotaQuery rightJoinPersona($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persona relation
 * @method NotaQuery innerJoinPersona($relationAlias = null) Adds a INNER JOIN clause to the query using the Persona relation
 *
 * @method Nota findOne(PropelPDO $con = null) Return the first Nota matching the query
 * @method Nota findOneOrCreate(PropelPDO $con = null) Return the first Nota matching the query, or a new Nota object populated from the query conditions when no match is found
 *
 * @method Nota findOneByTexto(string $texto) Return the first Nota filtered by the texto column
 * @method Nota findOneByFechaCreacion(string $fecha_creacion) Return the first Nota filtered by the fecha_creacion column
 * @method Nota findOneByFechaModificacion(string $fecha_modificacion) Return the first Nota filtered by the fecha_modificacion column
 * @method Nota findOneByMarcada(boolean $marcada) Return the first Nota filtered by the marcada column
 * @method Nota findOneByComunidadId(int $comunidad_id) Return the first Nota filtered by the comunidad_id column
 * @method Nota findOneByPersonaId(int $persona_id) Return the first Nota filtered by the persona_id column
 * @method Nota findOneByIncidenciaId(int $incidencia_id) Return the first Nota filtered by the incidencia_id column
 *
 * @method array findById(int $id) Return Nota objects filtered by the id column
 * @method array findByTexto(string $texto) Return Nota objects filtered by the texto column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Nota objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Nota objects filtered by the fecha_modificacion column
 * @method array findByMarcada(boolean $marcada) Return Nota objects filtered by the marcada column
 * @method array findByComunidadId(int $comunidad_id) Return Nota objects filtered by the comunidad_id column
 * @method array findByPersonaId(int $persona_id) Return Nota objects filtered by the persona_id column
 * @method array findByIncidenciaId(int $incidencia_id) Return Nota objects filtered by the incidencia_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseNotaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseNotaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Nota';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new NotaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   NotaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return NotaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof NotaQuery) {
            return $criteria;
        }
        $query = new NotaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Nota|Nota[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = NotaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(NotaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Nota A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Nota A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `texto`, `fecha_creacion`, `fecha_modificacion`, `marcada`, `comunidad_id`, `persona_id`, `incidencia_id` FROM `nota` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Nota();
            $obj->hydrate($row);
            NotaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Nota|Nota[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Nota[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(NotaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(NotaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(NotaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(NotaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the texto column
     *
     * Example usage:
     * <code>
     * $query->filterByTexto('fooValue');   // WHERE texto = 'fooValue'
     * $query->filterByTexto('%fooValue%'); // WHERE texto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByTexto($texto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $texto)) {
                $texto = str_replace('*', '%', $texto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(NotaPeer::TEXTO, $texto, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(NotaPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(NotaPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(NotaPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(NotaPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(NotaPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the comunidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidadId(1234); // WHERE comunidad_id = 1234
     * $query->filterByComunidadId(array(12, 34)); // WHERE comunidad_id IN (12, 34)
     * $query->filterByComunidadId(array('min' => 12)); // WHERE comunidad_id >= 12
     * $query->filterByComunidadId(array('max' => 12)); // WHERE comunidad_id <= 12
     * </code>
     *
     * @see       filterByComunidad()
     *
     * @param     mixed $comunidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByComunidadId($comunidadId = null, $comparison = null)
    {
        if (is_array($comunidadId)) {
            $useMinMax = false;
            if (isset($comunidadId['min'])) {
                $this->addUsingAlias(NotaPeer::COMUNIDAD_ID, $comunidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comunidadId['max'])) {
                $this->addUsingAlias(NotaPeer::COMUNIDAD_ID, $comunidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::COMUNIDAD_ID, $comunidadId, $comparison);
    }

    /**
     * Filter the query on the persona_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonaId(1234); // WHERE persona_id = 1234
     * $query->filterByPersonaId(array(12, 34)); // WHERE persona_id IN (12, 34)
     * $query->filterByPersonaId(array('min' => 12)); // WHERE persona_id >= 12
     * $query->filterByPersonaId(array('max' => 12)); // WHERE persona_id <= 12
     * </code>
     *
     * @see       filterByPersona()
     *
     * @param     mixed $personaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByPersonaId($personaId = null, $comparison = null)
    {
        if (is_array($personaId)) {
            $useMinMax = false;
            if (isset($personaId['min'])) {
                $this->addUsingAlias(NotaPeer::PERSONA_ID, $personaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personaId['max'])) {
                $this->addUsingAlias(NotaPeer::PERSONA_ID, $personaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::PERSONA_ID, $personaId, $comparison);
    }

    /**
     * Filter the query on the incidencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIncidenciaId(1234); // WHERE incidencia_id = 1234
     * $query->filterByIncidenciaId(array(12, 34)); // WHERE incidencia_id IN (12, 34)
     * $query->filterByIncidenciaId(array('min' => 12)); // WHERE incidencia_id >= 12
     * $query->filterByIncidenciaId(array('max' => 12)); // WHERE incidencia_id <= 12
     * </code>
     *
     * @see       filterByIncidencia()
     *
     * @param     mixed $incidenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function filterByIncidenciaId($incidenciaId = null, $comparison = null)
    {
        if (is_array($incidenciaId)) {
            $useMinMax = false;
            if (isset($incidenciaId['min'])) {
                $this->addUsingAlias(NotaPeer::INCIDENCIA_ID, $incidenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($incidenciaId['max'])) {
                $this->addUsingAlias(NotaPeer::INCIDENCIA_ID, $incidenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NotaPeer::INCIDENCIA_ID, $incidenciaId, $comparison);
    }

    /**
     * Filter the query by a related Comunidad object
     *
     * @param   Comunidad|PropelObjectCollection $comunidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NotaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComunidad($comunidad, $comparison = null)
    {
        if ($comunidad instanceof Comunidad) {
            return $this
                ->addUsingAlias(NotaPeer::COMUNIDAD_ID, $comunidad->getId(), $comparison);
        } elseif ($comunidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NotaPeer::COMUNIDAD_ID, $comunidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComunidad() only accepts arguments of type Comunidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comunidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function joinComunidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comunidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comunidad');
        }

        return $this;
    }

    /**
     * Use the Comunidad relation Comunidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComunidadQuery A secondary query class using the current class as primary query
     */
    public function useComunidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComunidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comunidad', 'ComunidadQuery');
    }

    /**
     * Filter the query by a related Incidencia object
     *
     * @param   Incidencia|PropelObjectCollection $incidencia The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NotaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIncidencia($incidencia, $comparison = null)
    {
        if ($incidencia instanceof Incidencia) {
            return $this
                ->addUsingAlias(NotaPeer::INCIDENCIA_ID, $incidencia->getId(), $comparison);
        } elseif ($incidencia instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NotaPeer::INCIDENCIA_ID, $incidencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByIncidencia() only accepts arguments of type Incidencia or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Incidencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function joinIncidencia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Incidencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Incidencia');
        }

        return $this;
    }

    /**
     * Use the Incidencia relation Incidencia object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   IncidenciaQuery A secondary query class using the current class as primary query
     */
    public function useIncidenciaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIncidencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Incidencia', 'IncidenciaQuery');
    }

    /**
     * Filter the query by a related Persona object
     *
     * @param   Persona|PropelObjectCollection $persona The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 NotaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPersona($persona, $comparison = null)
    {
        if ($persona instanceof Persona) {
            return $this
                ->addUsingAlias(NotaPeer::PERSONA_ID, $persona->getId(), $comparison);
        } elseif ($persona instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(NotaPeer::PERSONA_ID, $persona->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPersona() only accepts arguments of type Persona or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persona relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function joinPersona($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persona');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persona');
        }

        return $this;
    }

    /**
     * Use the Persona relation Persona object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PersonaQuery A secondary query class using the current class as primary query
     */
    public function usePersonaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPersona($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persona', 'PersonaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Nota $nota Object to remove from the list of results
     *
     * @return NotaQuery The current query, for fluid interface
     */
    public function prune($nota = null)
    {
        if ($nota) {
            $this->addUsingAlias(NotaPeer::ID, $nota->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
