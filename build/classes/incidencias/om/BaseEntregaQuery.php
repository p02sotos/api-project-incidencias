<?php


/**
 * Base class that represents a query for the 'entrega' table.
 *
 * 
 *
 * @method EntregaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method EntregaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method EntregaQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method EntregaQuery orderByPriodidad($order = Criteria::ASC) Order by the priodidad column
 * @method EntregaQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method EntregaQuery orderByEliminado($order = Criteria::ASC) Order by the eliminado column
 * @method EntregaQuery orderByFechaEntrega($order = Criteria::ASC) Order by the fecha_entrega column
 * @method EntregaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method EntregaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method EntregaQuery orderByComunidadId($order = Criteria::ASC) Order by the comunidad_id column
 * @method EntregaQuery orderByTipoId($order = Criteria::ASC) Order by the tipo_id column
 *
 * @method EntregaQuery groupById() Group by the id column
 * @method EntregaQuery groupByNombre() Group by the nombre column
 * @method EntregaQuery groupByDescripcion() Group by the descripcion column
 * @method EntregaQuery groupByPriodidad() Group by the priodidad column
 * @method EntregaQuery groupByMarcada() Group by the marcada column
 * @method EntregaQuery groupByEliminado() Group by the eliminado column
 * @method EntregaQuery groupByFechaEntrega() Group by the fecha_entrega column
 * @method EntregaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method EntregaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method EntregaQuery groupByComunidadId() Group by the comunidad_id column
 * @method EntregaQuery groupByTipoId() Group by the tipo_id column
 *
 * @method EntregaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method EntregaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method EntregaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method EntregaQuery leftJoinComunidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comunidad relation
 * @method EntregaQuery rightJoinComunidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comunidad relation
 * @method EntregaQuery innerJoinComunidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Comunidad relation
 *
 * @method EntregaQuery leftJoinTipo($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tipo relation
 * @method EntregaQuery rightJoinTipo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tipo relation
 * @method EntregaQuery innerJoinTipo($relationAlias = null) Adds a INNER JOIN clause to the query using the Tipo relation
 *
 * @method EntregaQuery leftJoinAvisoEntrega($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvisoEntrega relation
 * @method EntregaQuery rightJoinAvisoEntrega($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvisoEntrega relation
 * @method EntregaQuery innerJoinAvisoEntrega($relationAlias = null) Adds a INNER JOIN clause to the query using the AvisoEntrega relation
 *
 * @method Entrega findOne(PropelPDO $con = null) Return the first Entrega matching the query
 * @method Entrega findOneOrCreate(PropelPDO $con = null) Return the first Entrega matching the query, or a new Entrega object populated from the query conditions when no match is found
 *
 * @method Entrega findOneByNombre(string $nombre) Return the first Entrega filtered by the nombre column
 * @method Entrega findOneByDescripcion(string $descripcion) Return the first Entrega filtered by the descripcion column
 * @method Entrega findOneByPriodidad(int $priodidad) Return the first Entrega filtered by the priodidad column
 * @method Entrega findOneByMarcada(boolean $marcada) Return the first Entrega filtered by the marcada column
 * @method Entrega findOneByEliminado(boolean $eliminado) Return the first Entrega filtered by the eliminado column
 * @method Entrega findOneByFechaEntrega(string $fecha_entrega) Return the first Entrega filtered by the fecha_entrega column
 * @method Entrega findOneByFechaCreacion(string $fecha_creacion) Return the first Entrega filtered by the fecha_creacion column
 * @method Entrega findOneByFechaModificacion(string $fecha_modificacion) Return the first Entrega filtered by the fecha_modificacion column
 * @method Entrega findOneByComunidadId(int $comunidad_id) Return the first Entrega filtered by the comunidad_id column
 * @method Entrega findOneByTipoId(int $tipo_id) Return the first Entrega filtered by the tipo_id column
 *
 * @method array findById(int $id) Return Entrega objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Entrega objects filtered by the nombre column
 * @method array findByDescripcion(string $descripcion) Return Entrega objects filtered by the descripcion column
 * @method array findByPriodidad(int $priodidad) Return Entrega objects filtered by the priodidad column
 * @method array findByMarcada(boolean $marcada) Return Entrega objects filtered by the marcada column
 * @method array findByEliminado(boolean $eliminado) Return Entrega objects filtered by the eliminado column
 * @method array findByFechaEntrega(string $fecha_entrega) Return Entrega objects filtered by the fecha_entrega column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Entrega objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Entrega objects filtered by the fecha_modificacion column
 * @method array findByComunidadId(int $comunidad_id) Return Entrega objects filtered by the comunidad_id column
 * @method array findByTipoId(int $tipo_id) Return Entrega objects filtered by the tipo_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseEntregaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseEntregaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Entrega';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new EntregaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   EntregaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return EntregaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof EntregaQuery) {
            return $criteria;
        }
        $query = new EntregaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Entrega|Entrega[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = EntregaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(EntregaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Entrega A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Entrega A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `descripcion`, `priodidad`, `marcada`, `eliminado`, `fecha_entrega`, `fecha_creacion`, `fecha_modificacion`, `comunidad_id`, `tipo_id` FROM `entrega` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Entrega();
            $obj->hydrate($row);
            EntregaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Entrega|Entrega[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Entrega[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EntregaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EntregaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EntregaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EntregaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EntregaPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EntregaPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the priodidad column
     *
     * Example usage:
     * <code>
     * $query->filterByPriodidad(1234); // WHERE priodidad = 1234
     * $query->filterByPriodidad(array(12, 34)); // WHERE priodidad IN (12, 34)
     * $query->filterByPriodidad(array('min' => 12)); // WHERE priodidad >= 12
     * $query->filterByPriodidad(array('max' => 12)); // WHERE priodidad <= 12
     * </code>
     *
     * @param     mixed $priodidad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByPriodidad($priodidad = null, $comparison = null)
    {
        if (is_array($priodidad)) {
            $useMinMax = false;
            if (isset($priodidad['min'])) {
                $this->addUsingAlias(EntregaPeer::PRIODIDAD, $priodidad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priodidad['max'])) {
                $this->addUsingAlias(EntregaPeer::PRIODIDAD, $priodidad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::PRIODIDAD, $priodidad, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EntregaPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the eliminado column
     *
     * Example usage:
     * <code>
     * $query->filterByEliminado(true); // WHERE eliminado = true
     * $query->filterByEliminado('yes'); // WHERE eliminado = true
     * </code>
     *
     * @param     boolean|string $eliminado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByEliminado($eliminado = null, $comparison = null)
    {
        if (is_string($eliminado)) {
            $eliminado = in_array(strtolower($eliminado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EntregaPeer::ELIMINADO, $eliminado, $comparison);
    }

    /**
     * Filter the query on the fecha_entrega column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaEntrega('2011-03-14'); // WHERE fecha_entrega = '2011-03-14'
     * $query->filterByFechaEntrega('now'); // WHERE fecha_entrega = '2011-03-14'
     * $query->filterByFechaEntrega(array('max' => 'yesterday')); // WHERE fecha_entrega < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaEntrega The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByFechaEntrega($fechaEntrega = null, $comparison = null)
    {
        if (is_array($fechaEntrega)) {
            $useMinMax = false;
            if (isset($fechaEntrega['min'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_ENTREGA, $fechaEntrega['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaEntrega['max'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_ENTREGA, $fechaEntrega['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::FECHA_ENTREGA, $fechaEntrega, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(EntregaPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the comunidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidadId(1234); // WHERE comunidad_id = 1234
     * $query->filterByComunidadId(array(12, 34)); // WHERE comunidad_id IN (12, 34)
     * $query->filterByComunidadId(array('min' => 12)); // WHERE comunidad_id >= 12
     * $query->filterByComunidadId(array('max' => 12)); // WHERE comunidad_id <= 12
     * </code>
     *
     * @see       filterByComunidad()
     *
     * @param     mixed $comunidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByComunidadId($comunidadId = null, $comparison = null)
    {
        if (is_array($comunidadId)) {
            $useMinMax = false;
            if (isset($comunidadId['min'])) {
                $this->addUsingAlias(EntregaPeer::COMUNIDAD_ID, $comunidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comunidadId['max'])) {
                $this->addUsingAlias(EntregaPeer::COMUNIDAD_ID, $comunidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::COMUNIDAD_ID, $comunidadId, $comparison);
    }

    /**
     * Filter the query on the tipo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoId(1234); // WHERE tipo_id = 1234
     * $query->filterByTipoId(array(12, 34)); // WHERE tipo_id IN (12, 34)
     * $query->filterByTipoId(array('min' => 12)); // WHERE tipo_id >= 12
     * $query->filterByTipoId(array('max' => 12)); // WHERE tipo_id <= 12
     * </code>
     *
     * @see       filterByTipo()
     *
     * @param     mixed $tipoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function filterByTipoId($tipoId = null, $comparison = null)
    {
        if (is_array($tipoId)) {
            $useMinMax = false;
            if (isset($tipoId['min'])) {
                $this->addUsingAlias(EntregaPeer::TIPO_ID, $tipoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tipoId['max'])) {
                $this->addUsingAlias(EntregaPeer::TIPO_ID, $tipoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EntregaPeer::TIPO_ID, $tipoId, $comparison);
    }

    /**
     * Filter the query by a related Comunidad object
     *
     * @param   Comunidad|PropelObjectCollection $comunidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 EntregaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComunidad($comunidad, $comparison = null)
    {
        if ($comunidad instanceof Comunidad) {
            return $this
                ->addUsingAlias(EntregaPeer::COMUNIDAD_ID, $comunidad->getId(), $comparison);
        } elseif ($comunidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EntregaPeer::COMUNIDAD_ID, $comunidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComunidad() only accepts arguments of type Comunidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comunidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function joinComunidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comunidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comunidad');
        }

        return $this;
    }

    /**
     * Use the Comunidad relation Comunidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComunidadQuery A secondary query class using the current class as primary query
     */
    public function useComunidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComunidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comunidad', 'ComunidadQuery');
    }

    /**
     * Filter the query by a related Tipo object
     *
     * @param   Tipo|PropelObjectCollection $tipo The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 EntregaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTipo($tipo, $comparison = null)
    {
        if ($tipo instanceof Tipo) {
            return $this
                ->addUsingAlias(EntregaPeer::TIPO_ID, $tipo->getId(), $comparison);
        } elseif ($tipo instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EntregaPeer::TIPO_ID, $tipo->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTipo() only accepts arguments of type Tipo or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tipo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function joinTipo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tipo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tipo');
        }

        return $this;
    }

    /**
     * Use the Tipo relation Tipo object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TipoQuery A secondary query class using the current class as primary query
     */
    public function useTipoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTipo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tipo', 'TipoQuery');
    }

    /**
     * Filter the query by a related Aviso object
     *
     * @param   Aviso|PropelObjectCollection $aviso  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 EntregaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAvisoEntrega($aviso, $comparison = null)
    {
        if ($aviso instanceof Aviso) {
            return $this
                ->addUsingAlias(EntregaPeer::ID, $aviso->getEntregaId(), $comparison);
        } elseif ($aviso instanceof PropelObjectCollection) {
            return $this
                ->useAvisoEntregaQuery()
                ->filterByPrimaryKeys($aviso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvisoEntrega() only accepts arguments of type Aviso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvisoEntrega relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function joinAvisoEntrega($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvisoEntrega');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvisoEntrega');
        }

        return $this;
    }

    /**
     * Use the AvisoEntrega relation Aviso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AvisoQuery A secondary query class using the current class as primary query
     */
    public function useAvisoEntregaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvisoEntrega($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvisoEntrega', 'AvisoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Entrega $entrega Object to remove from the list of results
     *
     * @return EntregaQuery The current query, for fluid interface
     */
    public function prune($entrega = null)
    {
        if ($entrega) {
            $this->addUsingAlias(EntregaPeer::ID, $entrega->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
