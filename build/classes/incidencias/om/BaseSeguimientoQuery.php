<?php


/**
 * Base class that represents a query for the 'seguimiento' table.
 *
 * 
 *
 * @method SeguimientoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method SeguimientoQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method SeguimientoQuery orderByFechaSeguimiento($order = Criteria::ASC) Order by the fecha_seguimiento column
 * @method SeguimientoQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method SeguimientoQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method SeguimientoQuery orderByIncidenciaId($order = Criteria::ASC) Order by the incidencia_id column
 *
 * @method SeguimientoQuery groupById() Group by the id column
 * @method SeguimientoQuery groupByDescripcion() Group by the descripcion column
 * @method SeguimientoQuery groupByFechaSeguimiento() Group by the fecha_seguimiento column
 * @method SeguimientoQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method SeguimientoQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method SeguimientoQuery groupByIncidenciaId() Group by the incidencia_id column
 *
 * @method SeguimientoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SeguimientoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SeguimientoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SeguimientoQuery leftJoinIncidencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Incidencia relation
 * @method SeguimientoQuery rightJoinIncidencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Incidencia relation
 * @method SeguimientoQuery innerJoinIncidencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Incidencia relation
 *
 * @method Seguimiento findOne(PropelPDO $con = null) Return the first Seguimiento matching the query
 * @method Seguimiento findOneOrCreate(PropelPDO $con = null) Return the first Seguimiento matching the query, or a new Seguimiento object populated from the query conditions when no match is found
 *
 * @method Seguimiento findOneByDescripcion(string $descripcion) Return the first Seguimiento filtered by the descripcion column
 * @method Seguimiento findOneByFechaSeguimiento(string $fecha_seguimiento) Return the first Seguimiento filtered by the fecha_seguimiento column
 * @method Seguimiento findOneByFechaCreacion(string $fecha_creacion) Return the first Seguimiento filtered by the fecha_creacion column
 * @method Seguimiento findOneByFechaModificacion(string $fecha_modificacion) Return the first Seguimiento filtered by the fecha_modificacion column
 * @method Seguimiento findOneByIncidenciaId(int $incidencia_id) Return the first Seguimiento filtered by the incidencia_id column
 *
 * @method array findById(int $id) Return Seguimiento objects filtered by the id column
 * @method array findByDescripcion(string $descripcion) Return Seguimiento objects filtered by the descripcion column
 * @method array findByFechaSeguimiento(string $fecha_seguimiento) Return Seguimiento objects filtered by the fecha_seguimiento column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Seguimiento objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Seguimiento objects filtered by the fecha_modificacion column
 * @method array findByIncidenciaId(int $incidencia_id) Return Seguimiento objects filtered by the incidencia_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseSeguimientoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSeguimientoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Seguimiento';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SeguimientoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SeguimientoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SeguimientoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SeguimientoQuery) {
            return $criteria;
        }
        $query = new SeguimientoQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Seguimiento|Seguimiento[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SeguimientoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SeguimientoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Seguimiento A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Seguimiento A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `descripcion`, `fecha_seguimiento`, `fecha_creacion`, `fecha_modificacion`, `incidencia_id` FROM `seguimiento` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Seguimiento();
            $obj->hydrate($row);
            SeguimientoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Seguimiento|Seguimiento[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Seguimiento[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SeguimientoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SeguimientoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SeguimientoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SeguimientoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the fecha_seguimiento column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaSeguimiento('2011-03-14'); // WHERE fecha_seguimiento = '2011-03-14'
     * $query->filterByFechaSeguimiento('now'); // WHERE fecha_seguimiento = '2011-03-14'
     * $query->filterByFechaSeguimiento(array('max' => 'yesterday')); // WHERE fecha_seguimiento < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaSeguimiento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByFechaSeguimiento($fechaSeguimiento = null, $comparison = null)
    {
        if (is_array($fechaSeguimiento)) {
            $useMinMax = false;
            if (isset($fechaSeguimiento['min'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_SEGUIMIENTO, $fechaSeguimiento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaSeguimiento['max'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_SEGUIMIENTO, $fechaSeguimiento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::FECHA_SEGUIMIENTO, $fechaSeguimiento, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(SeguimientoPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the incidencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIncidenciaId(1234); // WHERE incidencia_id = 1234
     * $query->filterByIncidenciaId(array(12, 34)); // WHERE incidencia_id IN (12, 34)
     * $query->filterByIncidenciaId(array('min' => 12)); // WHERE incidencia_id >= 12
     * $query->filterByIncidenciaId(array('max' => 12)); // WHERE incidencia_id <= 12
     * </code>
     *
     * @see       filterByIncidencia()
     *
     * @param     mixed $incidenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function filterByIncidenciaId($incidenciaId = null, $comparison = null)
    {
        if (is_array($incidenciaId)) {
            $useMinMax = false;
            if (isset($incidenciaId['min'])) {
                $this->addUsingAlias(SeguimientoPeer::INCIDENCIA_ID, $incidenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($incidenciaId['max'])) {
                $this->addUsingAlias(SeguimientoPeer::INCIDENCIA_ID, $incidenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SeguimientoPeer::INCIDENCIA_ID, $incidenciaId, $comparison);
    }

    /**
     * Filter the query by a related Incidencia object
     *
     * @param   Incidencia|PropelObjectCollection $incidencia The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SeguimientoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIncidencia($incidencia, $comparison = null)
    {
        if ($incidencia instanceof Incidencia) {
            return $this
                ->addUsingAlias(SeguimientoPeer::INCIDENCIA_ID, $incidencia->getId(), $comparison);
        } elseif ($incidencia instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SeguimientoPeer::INCIDENCIA_ID, $incidencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByIncidencia() only accepts arguments of type Incidencia or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Incidencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function joinIncidencia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Incidencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Incidencia');
        }

        return $this;
    }

    /**
     * Use the Incidencia relation Incidencia object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   IncidenciaQuery A secondary query class using the current class as primary query
     */
    public function useIncidenciaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIncidencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Incidencia', 'IncidenciaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Seguimiento $seguimiento Object to remove from the list of results
     *
     * @return SeguimientoQuery The current query, for fluid interface
     */
    public function prune($seguimiento = null)
    {
        if ($seguimiento) {
            $this->addUsingAlias(SeguimientoPeer::ID, $seguimiento->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
