<?php


/**
 * Base class that represents a row from the 'incidencia' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseIncidencia extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'IncidenciaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        IncidenciaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the origen field.
     * @var        string
     */
    protected $origen;

    /**
     * The value for the nombre_persona field.
     * @var        string
     */
    protected $nombre_persona;

    /**
     * The value for the breve field.
     * @var        string
     */
    protected $breve;

    /**
     * The value for the expediente field.
     * @var        string
     */
    protected $expediente;

    /**
     * The value for the descripcion field.
     * @var        string
     */
    protected $descripcion;

    /**
     * The value for the atencion field.
     * @var        string
     */
    protected $atencion;

    /**
     * The value for the prioridad field.
     * @var        int
     */
    protected $prioridad;

    /**
     * The value for the resuelta field.
     * @var        boolean
     */
    protected $resuelta;

    /**
     * The value for the marcada field.
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the eliminado field.
     * @var        boolean
     */
    protected $eliminado;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * The value for the fecha_incidencia field.
     * @var        string
     */
    protected $fecha_incidencia;

    /**
     * The value for the fecha_resolucion field.
     * @var        string
     */
    protected $fecha_resolucion;

    /**
     * The value for the comunidad_id field.
     * @var        int
     */
    protected $comunidad_id;

    /**
     * The value for the persona_id field.
     * @var        int
     */
    protected $persona_id;

    /**
     * @var        Comunidad
     */
    protected $aComunidad;

    /**
     * @var        Persona
     */
    protected $aPersona;

    /**
     * @var        PropelObjectCollection|Aviso[] Collection to store aggregation of Aviso objects.
     */
    protected $collAvisoIncs;
    protected $collAvisoIncsPartial;

    /**
     * @var        PropelObjectCollection|Tarea[] Collection to store aggregation of Tarea objects.
     */
    protected $collTareas;
    protected $collTareasPartial;

    /**
     * @var        PropelObjectCollection|Seguimiento[] Collection to store aggregation of Seguimiento objects.
     */
    protected $collSeguimientos;
    protected $collSeguimientosPartial;

    /**
     * @var        PropelObjectCollection|Telefono[] Collection to store aggregation of Telefono objects.
     */
    protected $collTelefonoIncidencias;
    protected $collTelefonoIncidenciasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $avisoIncsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $tareasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $seguimientosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $telefonoIncidenciasScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [origen] column value.
     * 
     * @return string
     */
    public function getOrigen()
    {

        return $this->origen;
    }

    /**
     * Get the [nombre_persona] column value.
     * 
     * @return string
     */
    public function getNombrePersona()
    {

        return $this->nombre_persona;
    }

    /**
     * Get the [breve] column value.
     * 
     * @return string
     */
    public function getBreve()
    {

        return $this->breve;
    }

    /**
     * Get the [expediente] column value.
     * 
     * @return string
     */
    public function getExpediente()
    {

        return $this->expediente;
    }

    /**
     * Get the [descripcion] column value.
     * 
     * @return string
     */
    public function getDescripcion()
    {

        return $this->descripcion;
    }

    /**
     * Get the [atencion] column value.
     * 
     * @return string
     */
    public function getAtencion()
    {

        return $this->atencion;
    }

    /**
     * Get the [prioridad] column value.
     * 
     * @return int
     */
    public function getPrioridad()
    {

        return $this->prioridad;
    }

    /**
     * Get the [resuelta] column value.
     * 
     * @return boolean
     */
    public function getResuelta()
    {

        return $this->resuelta;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [eliminado] column value.
     * 
     * @return boolean
     */
    public function getEliminado()
    {

        return $this->eliminado;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_incidencia] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaIncidencia($format = '%x')
    {
        if ($this->fecha_incidencia === null) {
            return null;
        }

        if ($this->fecha_incidencia === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_incidencia);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_incidencia, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_resolucion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaResolucion($format = '%x')
    {
        if ($this->fecha_resolucion === null) {
            return null;
        }

        if ($this->fecha_resolucion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_resolucion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_resolucion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [comunidad_id] column value.
     * 
     * @return int
     */
    public function getComunidadId()
    {

        return $this->comunidad_id;
    }

    /**
     * Get the [persona_id] column value.
     * 
     * @return int
     */
    public function getPersonaId()
    {

        return $this->persona_id;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = IncidenciaPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [origen] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setOrigen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->origen !== $v) {
            $this->origen = $v;
            $this->modifiedColumns[] = IncidenciaPeer::ORIGEN;
        }


        return $this;
    } // setOrigen()

    /**
     * Set the value of [nombre_persona] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setNombrePersona($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre_persona !== $v) {
            $this->nombre_persona = $v;
            $this->modifiedColumns[] = IncidenciaPeer::NOMBRE_PERSONA;
        }


        return $this;
    } // setNombrePersona()

    /**
     * Set the value of [breve] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setBreve($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->breve !== $v) {
            $this->breve = $v;
            $this->modifiedColumns[] = IncidenciaPeer::BREVE;
        }


        return $this;
    } // setBreve()

    /**
     * Set the value of [expediente] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setExpediente($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->expediente !== $v) {
            $this->expediente = $v;
            $this->modifiedColumns[] = IncidenciaPeer::EXPEDIENTE;
        }


        return $this;
    } // setExpediente()

    /**
     * Set the value of [descripcion] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[] = IncidenciaPeer::DESCRIPCION;
        }


        return $this;
    } // setDescripcion()

    /**
     * Set the value of [atencion] column.
     * 
     * @param  string $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setAtencion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->atencion !== $v) {
            $this->atencion = $v;
            $this->modifiedColumns[] = IncidenciaPeer::ATENCION;
        }


        return $this;
    } // setAtencion()

    /**
     * Set the value of [prioridad] column.
     * 
     * @param  int $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setPrioridad($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->prioridad !== $v) {
            $this->prioridad = $v;
            $this->modifiedColumns[] = IncidenciaPeer::PRIORIDAD;
        }


        return $this;
    } // setPrioridad()

    /**
     * Sets the value of the [resuelta] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setResuelta($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->resuelta !== $v) {
            $this->resuelta = $v;
            $this->modifiedColumns[] = IncidenciaPeer::RESUELTA;
        }


        return $this;
    } // setResuelta()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = IncidenciaPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Sets the value of the [eliminado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setEliminado($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->eliminado !== $v) {
            $this->eliminado = $v;
            $this->modifiedColumns[] = IncidenciaPeer::ELIMINADO;
        }


        return $this;
    } // setEliminado()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencia The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = IncidenciaPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencia The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = IncidenciaPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Sets the value of [fecha_incidencia] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencia The current object (for fluent API support)
     */
    public function setFechaIncidencia($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_incidencia !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_incidencia !== null && $tmpDt = new DateTime($this->fecha_incidencia)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_incidencia = $newDateAsString;
                $this->modifiedColumns[] = IncidenciaPeer::FECHA_INCIDENCIA;
            }
        } // if either are not null


        return $this;
    } // setFechaIncidencia()

    /**
     * Sets the value of [fecha_resolucion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Incidencia The current object (for fluent API support)
     */
    public function setFechaResolucion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_resolucion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_resolucion !== null && $tmpDt = new DateTime($this->fecha_resolucion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_resolucion = $newDateAsString;
                $this->modifiedColumns[] = IncidenciaPeer::FECHA_RESOLUCION;
            }
        } // if either are not null


        return $this;
    } // setFechaResolucion()

    /**
     * Set the value of [comunidad_id] column.
     * 
     * @param  int $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setComunidadId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comunidad_id !== $v) {
            $this->comunidad_id = $v;
            $this->modifiedColumns[] = IncidenciaPeer::COMUNIDAD_ID;
        }

        if ($this->aComunidad !== null && $this->aComunidad->getId() !== $v) {
            $this->aComunidad = null;
        }


        return $this;
    } // setComunidadId()

    /**
     * Set the value of [persona_id] column.
     * 
     * @param  int $v new value
     * @return Incidencia The current object (for fluent API support)
     */
    public function setPersonaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->persona_id !== $v) {
            $this->persona_id = $v;
            $this->modifiedColumns[] = IncidenciaPeer::PERSONA_ID;
        }

        if ($this->aPersona !== null && $this->aPersona->getId() !== $v) {
            $this->aPersona = null;
        }


        return $this;
    } // setPersonaId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->origen = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nombre_persona = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->breve = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->expediente = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->descripcion = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->atencion = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->prioridad = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->resuelta = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->marcada = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
            $this->eliminado = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
            $this->fecha_creacion = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->fecha_modificacion = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->fecha_incidencia = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->fecha_resolucion = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->comunidad_id = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->persona_id = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 17; // 17 = IncidenciaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Incidencia object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aComunidad !== null && $this->comunidad_id !== $this->aComunidad->getId()) {
            $this->aComunidad = null;
        }
        if ($this->aPersona !== null && $this->persona_id !== $this->aPersona->getId()) {
            $this->aPersona = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = IncidenciaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aComunidad = null;
            $this->aPersona = null;
            $this->collAvisoIncs = null;

            $this->collTareas = null;

            $this->collSeguimientos = null;

            $this->collTelefonoIncidencias = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = IncidenciaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(IncidenciaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                IncidenciaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if ($this->aComunidad->isModified() || $this->aComunidad->isNew()) {
                    $affectedRows += $this->aComunidad->save($con);
                }
                $this->setComunidad($this->aComunidad);
            }

            if ($this->aPersona !== null) {
                if ($this->aPersona->isModified() || $this->aPersona->isNew()) {
                    $affectedRows += $this->aPersona->save($con);
                }
                $this->setPersona($this->aPersona);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->avisoIncsScheduledForDeletion !== null) {
                if (!$this->avisoIncsScheduledForDeletion->isEmpty()) {
                    foreach ($this->avisoIncsScheduledForDeletion as $avisoInc) {
                        // need to save related object because we set the relation to null
                        $avisoInc->save($con);
                    }
                    $this->avisoIncsScheduledForDeletion = null;
                }
            }

            if ($this->collAvisoIncs !== null) {
                foreach ($this->collAvisoIncs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tareasScheduledForDeletion !== null) {
                if (!$this->tareasScheduledForDeletion->isEmpty()) {
                    foreach ($this->tareasScheduledForDeletion as $tarea) {
                        // need to save related object because we set the relation to null
                        $tarea->save($con);
                    }
                    $this->tareasScheduledForDeletion = null;
                }
            }

            if ($this->collTareas !== null) {
                foreach ($this->collTareas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->seguimientosScheduledForDeletion !== null) {
                if (!$this->seguimientosScheduledForDeletion->isEmpty()) {
                    foreach ($this->seguimientosScheduledForDeletion as $seguimiento) {
                        // need to save related object because we set the relation to null
                        $seguimiento->save($con);
                    }
                    $this->seguimientosScheduledForDeletion = null;
                }
            }

            if ($this->collSeguimientos !== null) {
                foreach ($this->collSeguimientos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->telefonoIncidenciasScheduledForDeletion !== null) {
                if (!$this->telefonoIncidenciasScheduledForDeletion->isEmpty()) {
                    foreach ($this->telefonoIncidenciasScheduledForDeletion as $telefonoIncidencia) {
                        // need to save related object because we set the relation to null
                        $telefonoIncidencia->save($con);
                    }
                    $this->telefonoIncidenciasScheduledForDeletion = null;
                }
            }

            if ($this->collTelefonoIncidencias !== null) {
                foreach ($this->collTelefonoIncidencias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = IncidenciaPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . IncidenciaPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(IncidenciaPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(IncidenciaPeer::ORIGEN)) {
            $modifiedColumns[':p' . $index++]  = '`origen`';
        }
        if ($this->isColumnModified(IncidenciaPeer::NOMBRE_PERSONA)) {
            $modifiedColumns[':p' . $index++]  = '`nombre_persona`';
        }
        if ($this->isColumnModified(IncidenciaPeer::BREVE)) {
            $modifiedColumns[':p' . $index++]  = '`breve`';
        }
        if ($this->isColumnModified(IncidenciaPeer::EXPEDIENTE)) {
            $modifiedColumns[':p' . $index++]  = '`expediente`';
        }
        if ($this->isColumnModified(IncidenciaPeer::DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = '`descripcion`';
        }
        if ($this->isColumnModified(IncidenciaPeer::ATENCION)) {
            $modifiedColumns[':p' . $index++]  = '`atencion`';
        }
        if ($this->isColumnModified(IncidenciaPeer::PRIORIDAD)) {
            $modifiedColumns[':p' . $index++]  = '`prioridad`';
        }
        if ($this->isColumnModified(IncidenciaPeer::RESUELTA)) {
            $modifiedColumns[':p' . $index++]  = '`resuelta`';
        }
        if ($this->isColumnModified(IncidenciaPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(IncidenciaPeer::ELIMINADO)) {
            $modifiedColumns[':p' . $index++]  = '`eliminado`';
        }
        if ($this->isColumnModified(IncidenciaPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(IncidenciaPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }
        if ($this->isColumnModified(IncidenciaPeer::FECHA_INCIDENCIA)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_incidencia`';
        }
        if ($this->isColumnModified(IncidenciaPeer::FECHA_RESOLUCION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_resolucion`';
        }
        if ($this->isColumnModified(IncidenciaPeer::COMUNIDAD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`comunidad_id`';
        }
        if ($this->isColumnModified(IncidenciaPeer::PERSONA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`persona_id`';
        }

        $sql = sprintf(
            'INSERT INTO `incidencia` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`origen`':						
                        $stmt->bindValue($identifier, $this->origen, PDO::PARAM_STR);
                        break;
                    case '`nombre_persona`':						
                        $stmt->bindValue($identifier, $this->nombre_persona, PDO::PARAM_STR);
                        break;
                    case '`breve`':						
                        $stmt->bindValue($identifier, $this->breve, PDO::PARAM_STR);
                        break;
                    case '`expediente`':						
                        $stmt->bindValue($identifier, $this->expediente, PDO::PARAM_STR);
                        break;
                    case '`descripcion`':						
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case '`atencion`':						
                        $stmt->bindValue($identifier, $this->atencion, PDO::PARAM_STR);
                        break;
                    case '`prioridad`':						
                        $stmt->bindValue($identifier, $this->prioridad, PDO::PARAM_INT);
                        break;
                    case '`resuelta`':
                        $stmt->bindValue($identifier, (int) $this->resuelta, PDO::PARAM_INT);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`eliminado`':
                        $stmt->bindValue($identifier, (int) $this->eliminado, PDO::PARAM_INT);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_incidencia`':						
                        $stmt->bindValue($identifier, $this->fecha_incidencia, PDO::PARAM_STR);
                        break;
                    case '`fecha_resolucion`':						
                        $stmt->bindValue($identifier, $this->fecha_resolucion, PDO::PARAM_STR);
                        break;
                    case '`comunidad_id`':						
                        $stmt->bindValue($identifier, $this->comunidad_id, PDO::PARAM_INT);
                        break;
                    case '`persona_id`':						
                        $stmt->bindValue($identifier, $this->persona_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if (!$this->aComunidad->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aComunidad->getValidationFailures());
                }
            }

            if ($this->aPersona !== null) {
                if (!$this->aPersona->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPersona->getValidationFailures());
                }
            }


            if (($retval = IncidenciaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collAvisoIncs !== null) {
                    foreach ($this->collAvisoIncs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTareas !== null) {
                    foreach ($this->collTareas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSeguimientos !== null) {
                    foreach ($this->collSeguimientos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collTelefonoIncidencias !== null) {
                    foreach ($this->collTelefonoIncidencias as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = IncidenciaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOrigen();
                break;
            case 2:
                return $this->getNombrePersona();
                break;
            case 3:
                return $this->getBreve();
                break;
            case 4:
                return $this->getExpediente();
                break;
            case 5:
                return $this->getDescripcion();
                break;
            case 6:
                return $this->getAtencion();
                break;
            case 7:
                return $this->getPrioridad();
                break;
            case 8:
                return $this->getResuelta();
                break;
            case 9:
                return $this->getMarcada();
                break;
            case 10:
                return $this->getEliminado();
                break;
            case 11:
                return $this->getFechaCreacion();
                break;
            case 12:
                return $this->getFechaModificacion();
                break;
            case 13:
                return $this->getFechaIncidencia();
                break;
            case 14:
                return $this->getFechaResolucion();
                break;
            case 15:
                return $this->getComunidadId();
                break;
            case 16:
                return $this->getPersonaId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Incidencia'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Incidencia'][$this->getPrimaryKey()] = true;
        $keys = IncidenciaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOrigen(),
            $keys[2] => $this->getNombrePersona(),
            $keys[3] => $this->getBreve(),
            $keys[4] => $this->getExpediente(),
            $keys[5] => $this->getDescripcion(),
            $keys[6] => $this->getAtencion(),
            $keys[7] => $this->getPrioridad(),
            $keys[8] => $this->getResuelta(),
            $keys[9] => $this->getMarcada(),
            $keys[10] => $this->getEliminado(),
            $keys[11] => $this->getFechaCreacion(),
            $keys[12] => $this->getFechaModificacion(),
            $keys[13] => $this->getFechaIncidencia(),
            $keys[14] => $this->getFechaResolucion(),
            $keys[15] => $this->getComunidadId(),
            $keys[16] => $this->getPersonaId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aComunidad) {
                $result['Comunidad'] = $this->aComunidad->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPersona) {
                $result['Persona'] = $this->aPersona->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collAvisoIncs) {
                $result['AvisoIncs'] = $this->collAvisoIncs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTareas) {
                $result['Tareas'] = $this->collTareas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSeguimientos) {
                $result['Seguimientos'] = $this->collSeguimientos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTelefonoIncidencias) {
                $result['TelefonoIncidencias'] = $this->collTelefonoIncidencias->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = IncidenciaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOrigen($value);
                break;
            case 2:
                $this->setNombrePersona($value);
                break;
            case 3:
                $this->setBreve($value);
                break;
            case 4:
                $this->setExpediente($value);
                break;
            case 5:
                $this->setDescripcion($value);
                break;
            case 6:
                $this->setAtencion($value);
                break;
            case 7:
                $this->setPrioridad($value);
                break;
            case 8:
                $this->setResuelta($value);
                break;
            case 9:
                $this->setMarcada($value);
                break;
            case 10:
                $this->setEliminado($value);
                break;
            case 11:
                $this->setFechaCreacion($value);
                break;
            case 12:
                $this->setFechaModificacion($value);
                break;
            case 13:
                $this->setFechaIncidencia($value);
                break;
            case 14:
                $this->setFechaResolucion($value);
                break;
            case 15:
                $this->setComunidadId($value);
                break;
            case 16:
                $this->setPersonaId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = IncidenciaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrigen($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNombrePersona($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setBreve($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setExpediente($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDescripcion($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAtencion($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPrioridad($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setResuelta($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMarcada($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setEliminado($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setFechaCreacion($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setFechaModificacion($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setFechaIncidencia($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setFechaResolucion($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setComunidadId($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setPersonaId($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(IncidenciaPeer::DATABASE_NAME);

        if ($this->isColumnModified(IncidenciaPeer::ID)) $criteria->add(IncidenciaPeer::ID, $this->id);
        if ($this->isColumnModified(IncidenciaPeer::ORIGEN)) $criteria->add(IncidenciaPeer::ORIGEN, $this->origen);
        if ($this->isColumnModified(IncidenciaPeer::NOMBRE_PERSONA)) $criteria->add(IncidenciaPeer::NOMBRE_PERSONA, $this->nombre_persona);
        if ($this->isColumnModified(IncidenciaPeer::BREVE)) $criteria->add(IncidenciaPeer::BREVE, $this->breve);
        if ($this->isColumnModified(IncidenciaPeer::EXPEDIENTE)) $criteria->add(IncidenciaPeer::EXPEDIENTE, $this->expediente);
        if ($this->isColumnModified(IncidenciaPeer::DESCRIPCION)) $criteria->add(IncidenciaPeer::DESCRIPCION, $this->descripcion);
        if ($this->isColumnModified(IncidenciaPeer::ATENCION)) $criteria->add(IncidenciaPeer::ATENCION, $this->atencion);
        if ($this->isColumnModified(IncidenciaPeer::PRIORIDAD)) $criteria->add(IncidenciaPeer::PRIORIDAD, $this->prioridad);
        if ($this->isColumnModified(IncidenciaPeer::RESUELTA)) $criteria->add(IncidenciaPeer::RESUELTA, $this->resuelta);
        if ($this->isColumnModified(IncidenciaPeer::MARCADA)) $criteria->add(IncidenciaPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(IncidenciaPeer::ELIMINADO)) $criteria->add(IncidenciaPeer::ELIMINADO, $this->eliminado);
        if ($this->isColumnModified(IncidenciaPeer::FECHA_CREACION)) $criteria->add(IncidenciaPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(IncidenciaPeer::FECHA_MODIFICACION)) $criteria->add(IncidenciaPeer::FECHA_MODIFICACION, $this->fecha_modificacion);
        if ($this->isColumnModified(IncidenciaPeer::FECHA_INCIDENCIA)) $criteria->add(IncidenciaPeer::FECHA_INCIDENCIA, $this->fecha_incidencia);
        if ($this->isColumnModified(IncidenciaPeer::FECHA_RESOLUCION)) $criteria->add(IncidenciaPeer::FECHA_RESOLUCION, $this->fecha_resolucion);
        if ($this->isColumnModified(IncidenciaPeer::COMUNIDAD_ID)) $criteria->add(IncidenciaPeer::COMUNIDAD_ID, $this->comunidad_id);
        if ($this->isColumnModified(IncidenciaPeer::PERSONA_ID)) $criteria->add(IncidenciaPeer::PERSONA_ID, $this->persona_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(IncidenciaPeer::DATABASE_NAME);
        $criteria->add(IncidenciaPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Incidencia (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrigen($this->getOrigen());
        $copyObj->setNombrePersona($this->getNombrePersona());
        $copyObj->setBreve($this->getBreve());
        $copyObj->setExpediente($this->getExpediente());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setAtencion($this->getAtencion());
        $copyObj->setPrioridad($this->getPrioridad());
        $copyObj->setResuelta($this->getResuelta());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setEliminado($this->getEliminado());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setFechaIncidencia($this->getFechaIncidencia());
        $copyObj->setFechaResolucion($this->getFechaResolucion());
        $copyObj->setComunidadId($this->getComunidadId());
        $copyObj->setPersonaId($this->getPersonaId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getAvisoIncs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAvisoInc($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTareas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTarea($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSeguimientos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSeguimiento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTelefonoIncidencias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTelefonoIncidencia($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Incidencia Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return IncidenciaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new IncidenciaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Comunidad object.
     *
     * @param                  Comunidad $v
     * @return Incidencia The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComunidad(Comunidad $v = null)
    {
        if ($v === null) {
            $this->setComunidadId(NULL);
        } else {
            $this->setComunidadId($v->getId());
        }

        $this->aComunidad = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Comunidad object, it will not be re-added.
        if ($v !== null) {
            $v->addIncidenciaCom($this);
        }


        return $this;
    }


    /**
     * Get the associated Comunidad object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Comunidad The associated Comunidad object.
     * @throws PropelException
     */
    public function getComunidad(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aComunidad === null && ($this->comunidad_id !== null) && $doQuery) {
            $this->aComunidad = ComunidadQuery::create()->findPk($this->comunidad_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComunidad->addIncidenciaComs($this);
             */
        }

        return $this->aComunidad;
    }

    /**
     * Declares an association between this object and a Persona object.
     *
     * @param                  Persona $v
     * @return Incidencia The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPersona(Persona $v = null)
    {
        if ($v === null) {
            $this->setPersonaId(NULL);
        } else {
            $this->setPersonaId($v->getId());
        }

        $this->aPersona = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Persona object, it will not be re-added.
        if ($v !== null) {
            $v->addIncidenciaPer($this);
        }


        return $this;
    }


    /**
     * Get the associated Persona object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Persona The associated Persona object.
     * @throws PropelException
     */
    public function getPersona(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPersona === null && ($this->persona_id !== null) && $doQuery) {
            $this->aPersona = PersonaQuery::create()->findPk($this->persona_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPersona->addIncidenciaPers($this);
             */
        }

        return $this->aPersona;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('AvisoInc' == $relationName) {
            $this->initAvisoIncs();
        }
        if ('Tarea' == $relationName) {
            $this->initTareas();
        }
        if ('Seguimiento' == $relationName) {
            $this->initSeguimientos();
        }
        if ('TelefonoIncidencia' == $relationName) {
            $this->initTelefonoIncidencias();
        }
    }

    /**
     * Clears out the collAvisoIncs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Incidencia The current object (for fluent API support)
     * @see        addAvisoIncs()
     */
    public function clearAvisoIncs()
    {
        $this->collAvisoIncs = null; // important to set this to null since that means it is uninitialized
        $this->collAvisoIncsPartial = null;

        return $this;
    }

    /**
     * reset is the collAvisoIncs collection loaded partially
     *
     * @return void
     */
    public function resetPartialAvisoIncs($v = true)
    {
        $this->collAvisoIncsPartial = $v;
    }

    /**
     * Initializes the collAvisoIncs collection.
     *
     * By default this just sets the collAvisoIncs collection to an empty array (like clearcollAvisoIncs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAvisoIncs($overrideExisting = true)
    {
        if (null !== $this->collAvisoIncs && !$overrideExisting) {
            return;
        }
        $this->collAvisoIncs = new PropelObjectCollection();
        $this->collAvisoIncs->setModel('Aviso');
    }

    /**
     * Gets an array of Aviso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Incidencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     * @throws PropelException
     */
    public function getAvisoIncs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAvisoIncsPartial && !$this->isNew();
        if (null === $this->collAvisoIncs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAvisoIncs) {
                // return empty collection
                $this->initAvisoIncs();
            } else {
                $collAvisoIncs = AvisoQuery::create(null, $criteria)
                    ->filterByIncidencia($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAvisoIncsPartial && count($collAvisoIncs)) {
                      $this->initAvisoIncs(false);

                      foreach ($collAvisoIncs as $obj) {
                        if (false == $this->collAvisoIncs->contains($obj)) {
                          $this->collAvisoIncs->append($obj);
                        }
                      }

                      $this->collAvisoIncsPartial = true;
                    }

                    $collAvisoIncs->getInternalIterator()->rewind();

                    return $collAvisoIncs;
                }

                if ($partial && $this->collAvisoIncs) {
                    foreach ($this->collAvisoIncs as $obj) {
                        if ($obj->isNew()) {
                            $collAvisoIncs[] = $obj;
                        }
                    }
                }

                $this->collAvisoIncs = $collAvisoIncs;
                $this->collAvisoIncsPartial = false;
            }
        }

        return $this->collAvisoIncs;
    }

    /**
     * Sets a collection of AvisoInc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $avisoIncs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Incidencia The current object (for fluent API support)
     */
    public function setAvisoIncs(PropelCollection $avisoIncs, PropelPDO $con = null)
    {
        $avisoIncsToDelete = $this->getAvisoIncs(new Criteria(), $con)->diff($avisoIncs);


        $this->avisoIncsScheduledForDeletion = $avisoIncsToDelete;

        foreach ($avisoIncsToDelete as $avisoIncRemoved) {
            $avisoIncRemoved->setIncidencia(null);
        }

        $this->collAvisoIncs = null;
        foreach ($avisoIncs as $avisoInc) {
            $this->addAvisoInc($avisoInc);
        }

        $this->collAvisoIncs = $avisoIncs;
        $this->collAvisoIncsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Aviso objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Aviso objects.
     * @throws PropelException
     */
    public function countAvisoIncs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAvisoIncsPartial && !$this->isNew();
        if (null === $this->collAvisoIncs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAvisoIncs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAvisoIncs());
            }
            $query = AvisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIncidencia($this)
                ->count($con);
        }

        return count($this->collAvisoIncs);
    }

    /**
     * Method called to associate a Aviso object to this object
     * through the Aviso foreign key attribute.
     *
     * @param    Aviso $l Aviso
     * @return Incidencia The current object (for fluent API support)
     */
    public function addAvisoInc(Aviso $l)
    {
        if ($this->collAvisoIncs === null) {
            $this->initAvisoIncs();
            $this->collAvisoIncsPartial = true;
        }

        if (!in_array($l, $this->collAvisoIncs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAvisoInc($l);

            if ($this->avisoIncsScheduledForDeletion and $this->avisoIncsScheduledForDeletion->contains($l)) {
                $this->avisoIncsScheduledForDeletion->remove($this->avisoIncsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	AvisoInc $avisoInc The avisoInc object to add.
     */
    protected function doAddAvisoInc($avisoInc)
    {
        $this->collAvisoIncs[]= $avisoInc;
        $avisoInc->setIncidencia($this);
    }

    /**
     * @param	AvisoInc $avisoInc The avisoInc object to remove.
     * @return Incidencia The current object (for fluent API support)
     */
    public function removeAvisoInc($avisoInc)
    {
        if ($this->getAvisoIncs()->contains($avisoInc)) {
            $this->collAvisoIncs->remove($this->collAvisoIncs->search($avisoInc));
            if (null === $this->avisoIncsScheduledForDeletion) {
                $this->avisoIncsScheduledForDeletion = clone $this->collAvisoIncs;
                $this->avisoIncsScheduledForDeletion->clear();
            }
            $this->avisoIncsScheduledForDeletion[]= $avisoInc;
            $avisoInc->setIncidencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related AvisoIncs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoIncsJoinTarea($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Tarea', $join_behavior);

        return $this->getAvisoIncs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related AvisoIncs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoIncsJoinComunidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Comunidad', $join_behavior);

        return $this->getAvisoIncs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related AvisoIncs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoIncsJoinPersona($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Persona', $join_behavior);

        return $this->getAvisoIncs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related AvisoIncs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoIncsJoinEntrega($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Entrega', $join_behavior);

        return $this->getAvisoIncs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related AvisoIncs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoIncsJoinContabilidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Contabilidad', $join_behavior);

        return $this->getAvisoIncs($query, $con);
    }

    /**
     * Clears out the collTareas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Incidencia The current object (for fluent API support)
     * @see        addTareas()
     */
    public function clearTareas()
    {
        $this->collTareas = null; // important to set this to null since that means it is uninitialized
        $this->collTareasPartial = null;

        return $this;
    }

    /**
     * reset is the collTareas collection loaded partially
     *
     * @return void
     */
    public function resetPartialTareas($v = true)
    {
        $this->collTareasPartial = $v;
    }

    /**
     * Initializes the collTareas collection.
     *
     * By default this just sets the collTareas collection to an empty array (like clearcollTareas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTareas($overrideExisting = true)
    {
        if (null !== $this->collTareas && !$overrideExisting) {
            return;
        }
        $this->collTareas = new PropelObjectCollection();
        $this->collTareas->setModel('Tarea');
    }

    /**
     * Gets an array of Tarea objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Incidencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Tarea[] List of Tarea objects
     * @throws PropelException
     */
    public function getTareas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTareasPartial && !$this->isNew();
        if (null === $this->collTareas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTareas) {
                // return empty collection
                $this->initTareas();
            } else {
                $collTareas = TareaQuery::create(null, $criteria)
                    ->filterByIncidencia($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTareasPartial && count($collTareas)) {
                      $this->initTareas(false);

                      foreach ($collTareas as $obj) {
                        if (false == $this->collTareas->contains($obj)) {
                          $this->collTareas->append($obj);
                        }
                      }

                      $this->collTareasPartial = true;
                    }

                    $collTareas->getInternalIterator()->rewind();

                    return $collTareas;
                }

                if ($partial && $this->collTareas) {
                    foreach ($this->collTareas as $obj) {
                        if ($obj->isNew()) {
                            $collTareas[] = $obj;
                        }
                    }
                }

                $this->collTareas = $collTareas;
                $this->collTareasPartial = false;
            }
        }

        return $this->collTareas;
    }

    /**
     * Sets a collection of Tarea objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $tareas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Incidencia The current object (for fluent API support)
     */
    public function setTareas(PropelCollection $tareas, PropelPDO $con = null)
    {
        $tareasToDelete = $this->getTareas(new Criteria(), $con)->diff($tareas);


        $this->tareasScheduledForDeletion = $tareasToDelete;

        foreach ($tareasToDelete as $tareaRemoved) {
            $tareaRemoved->setIncidencia(null);
        }

        $this->collTareas = null;
        foreach ($tareas as $tarea) {
            $this->addTarea($tarea);
        }

        $this->collTareas = $tareas;
        $this->collTareasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tarea objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Tarea objects.
     * @throws PropelException
     */
    public function countTareas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTareasPartial && !$this->isNew();
        if (null === $this->collTareas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTareas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTareas());
            }
            $query = TareaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIncidencia($this)
                ->count($con);
        }

        return count($this->collTareas);
    }

    /**
     * Method called to associate a Tarea object to this object
     * through the Tarea foreign key attribute.
     *
     * @param    Tarea $l Tarea
     * @return Incidencia The current object (for fluent API support)
     */
    public function addTarea(Tarea $l)
    {
        if ($this->collTareas === null) {
            $this->initTareas();
            $this->collTareasPartial = true;
        }

        if (!in_array($l, $this->collTareas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTarea($l);

            if ($this->tareasScheduledForDeletion and $this->tareasScheduledForDeletion->contains($l)) {
                $this->tareasScheduledForDeletion->remove($this->tareasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Tarea $tarea The tarea object to add.
     */
    protected function doAddTarea($tarea)
    {
        $this->collTareas[]= $tarea;
        $tarea->setIncidencia($this);
    }

    /**
     * @param	Tarea $tarea The tarea object to remove.
     * @return Incidencia The current object (for fluent API support)
     */
    public function removeTarea($tarea)
    {
        if ($this->getTareas()->contains($tarea)) {
            $this->collTareas->remove($this->collTareas->search($tarea));
            if (null === $this->tareasScheduledForDeletion) {
                $this->tareasScheduledForDeletion = clone $this->collTareas;
                $this->tareasScheduledForDeletion->clear();
            }
            $this->tareasScheduledForDeletion[]= $tarea;
            $tarea->setIncidencia(null);
        }

        return $this;
    }

    /**
     * Clears out the collSeguimientos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Incidencia The current object (for fluent API support)
     * @see        addSeguimientos()
     */
    public function clearSeguimientos()
    {
        $this->collSeguimientos = null; // important to set this to null since that means it is uninitialized
        $this->collSeguimientosPartial = null;

        return $this;
    }

    /**
     * reset is the collSeguimientos collection loaded partially
     *
     * @return void
     */
    public function resetPartialSeguimientos($v = true)
    {
        $this->collSeguimientosPartial = $v;
    }

    /**
     * Initializes the collSeguimientos collection.
     *
     * By default this just sets the collSeguimientos collection to an empty array (like clearcollSeguimientos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSeguimientos($overrideExisting = true)
    {
        if (null !== $this->collSeguimientos && !$overrideExisting) {
            return;
        }
        $this->collSeguimientos = new PropelObjectCollection();
        $this->collSeguimientos->setModel('Seguimiento');
    }

    /**
     * Gets an array of Seguimiento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Incidencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Seguimiento[] List of Seguimiento objects
     * @throws PropelException
     */
    public function getSeguimientos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSeguimientosPartial && !$this->isNew();
        if (null === $this->collSeguimientos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSeguimientos) {
                // return empty collection
                $this->initSeguimientos();
            } else {
                $collSeguimientos = SeguimientoQuery::create(null, $criteria)
                    ->filterByIncidencia($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSeguimientosPartial && count($collSeguimientos)) {
                      $this->initSeguimientos(false);

                      foreach ($collSeguimientos as $obj) {
                        if (false == $this->collSeguimientos->contains($obj)) {
                          $this->collSeguimientos->append($obj);
                        }
                      }

                      $this->collSeguimientosPartial = true;
                    }

                    $collSeguimientos->getInternalIterator()->rewind();

                    return $collSeguimientos;
                }

                if ($partial && $this->collSeguimientos) {
                    foreach ($this->collSeguimientos as $obj) {
                        if ($obj->isNew()) {
                            $collSeguimientos[] = $obj;
                        }
                    }
                }

                $this->collSeguimientos = $collSeguimientos;
                $this->collSeguimientosPartial = false;
            }
        }

        return $this->collSeguimientos;
    }

    /**
     * Sets a collection of Seguimiento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $seguimientos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Incidencia The current object (for fluent API support)
     */
    public function setSeguimientos(PropelCollection $seguimientos, PropelPDO $con = null)
    {
        $seguimientosToDelete = $this->getSeguimientos(new Criteria(), $con)->diff($seguimientos);


        $this->seguimientosScheduledForDeletion = $seguimientosToDelete;

        foreach ($seguimientosToDelete as $seguimientoRemoved) {
            $seguimientoRemoved->setIncidencia(null);
        }

        $this->collSeguimientos = null;
        foreach ($seguimientos as $seguimiento) {
            $this->addSeguimiento($seguimiento);
        }

        $this->collSeguimientos = $seguimientos;
        $this->collSeguimientosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Seguimiento objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Seguimiento objects.
     * @throws PropelException
     */
    public function countSeguimientos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSeguimientosPartial && !$this->isNew();
        if (null === $this->collSeguimientos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSeguimientos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSeguimientos());
            }
            $query = SeguimientoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIncidencia($this)
                ->count($con);
        }

        return count($this->collSeguimientos);
    }

    /**
     * Method called to associate a Seguimiento object to this object
     * through the Seguimiento foreign key attribute.
     *
     * @param    Seguimiento $l Seguimiento
     * @return Incidencia The current object (for fluent API support)
     */
    public function addSeguimiento(Seguimiento $l)
    {
        if ($this->collSeguimientos === null) {
            $this->initSeguimientos();
            $this->collSeguimientosPartial = true;
        }

        if (!in_array($l, $this->collSeguimientos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSeguimiento($l);

            if ($this->seguimientosScheduledForDeletion and $this->seguimientosScheduledForDeletion->contains($l)) {
                $this->seguimientosScheduledForDeletion->remove($this->seguimientosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Seguimiento $seguimiento The seguimiento object to add.
     */
    protected function doAddSeguimiento($seguimiento)
    {
        $this->collSeguimientos[]= $seguimiento;
        $seguimiento->setIncidencia($this);
    }

    /**
     * @param	Seguimiento $seguimiento The seguimiento object to remove.
     * @return Incidencia The current object (for fluent API support)
     */
    public function removeSeguimiento($seguimiento)
    {
        if ($this->getSeguimientos()->contains($seguimiento)) {
            $this->collSeguimientos->remove($this->collSeguimientos->search($seguimiento));
            if (null === $this->seguimientosScheduledForDeletion) {
                $this->seguimientosScheduledForDeletion = clone $this->collSeguimientos;
                $this->seguimientosScheduledForDeletion->clear();
            }
            $this->seguimientosScheduledForDeletion[]= $seguimiento;
            $seguimiento->setIncidencia(null);
        }

        return $this;
    }

    /**
     * Clears out the collTelefonoIncidencias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Incidencia The current object (for fluent API support)
     * @see        addTelefonoIncidencias()
     */
    public function clearTelefonoIncidencias()
    {
        $this->collTelefonoIncidencias = null; // important to set this to null since that means it is uninitialized
        $this->collTelefonoIncidenciasPartial = null;

        return $this;
    }

    /**
     * reset is the collTelefonoIncidencias collection loaded partially
     *
     * @return void
     */
    public function resetPartialTelefonoIncidencias($v = true)
    {
        $this->collTelefonoIncidenciasPartial = $v;
    }

    /**
     * Initializes the collTelefonoIncidencias collection.
     *
     * By default this just sets the collTelefonoIncidencias collection to an empty array (like clearcollTelefonoIncidencias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTelefonoIncidencias($overrideExisting = true)
    {
        if (null !== $this->collTelefonoIncidencias && !$overrideExisting) {
            return;
        }
        $this->collTelefonoIncidencias = new PropelObjectCollection();
        $this->collTelefonoIncidencias->setModel('Telefono');
    }

    /**
     * Gets an array of Telefono objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Incidencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     * @throws PropelException
     */
    public function getTelefonoIncidencias($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoIncidenciasPartial && !$this->isNew();
        if (null === $this->collTelefonoIncidencias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTelefonoIncidencias) {
                // return empty collection
                $this->initTelefonoIncidencias();
            } else {
                $collTelefonoIncidencias = TelefonoQuery::create(null, $criteria)
                    ->filterByIncidencia($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTelefonoIncidenciasPartial && count($collTelefonoIncidencias)) {
                      $this->initTelefonoIncidencias(false);

                      foreach ($collTelefonoIncidencias as $obj) {
                        if (false == $this->collTelefonoIncidencias->contains($obj)) {
                          $this->collTelefonoIncidencias->append($obj);
                        }
                      }

                      $this->collTelefonoIncidenciasPartial = true;
                    }

                    $collTelefonoIncidencias->getInternalIterator()->rewind();

                    return $collTelefonoIncidencias;
                }

                if ($partial && $this->collTelefonoIncidencias) {
                    foreach ($this->collTelefonoIncidencias as $obj) {
                        if ($obj->isNew()) {
                            $collTelefonoIncidencias[] = $obj;
                        }
                    }
                }

                $this->collTelefonoIncidencias = $collTelefonoIncidencias;
                $this->collTelefonoIncidenciasPartial = false;
            }
        }

        return $this->collTelefonoIncidencias;
    }

    /**
     * Sets a collection of TelefonoIncidencia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $telefonoIncidencias A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Incidencia The current object (for fluent API support)
     */
    public function setTelefonoIncidencias(PropelCollection $telefonoIncidencias, PropelPDO $con = null)
    {
        $telefonoIncidenciasToDelete = $this->getTelefonoIncidencias(new Criteria(), $con)->diff($telefonoIncidencias);


        $this->telefonoIncidenciasScheduledForDeletion = $telefonoIncidenciasToDelete;

        foreach ($telefonoIncidenciasToDelete as $telefonoIncidenciaRemoved) {
            $telefonoIncidenciaRemoved->setIncidencia(null);
        }

        $this->collTelefonoIncidencias = null;
        foreach ($telefonoIncidencias as $telefonoIncidencia) {
            $this->addTelefonoIncidencia($telefonoIncidencia);
        }

        $this->collTelefonoIncidencias = $telefonoIncidencias;
        $this->collTelefonoIncidenciasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Telefono objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Telefono objects.
     * @throws PropelException
     */
    public function countTelefonoIncidencias(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoIncidenciasPartial && !$this->isNew();
        if (null === $this->collTelefonoIncidencias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTelefonoIncidencias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTelefonoIncidencias());
            }
            $query = TelefonoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByIncidencia($this)
                ->count($con);
        }

        return count($this->collTelefonoIncidencias);
    }

    /**
     * Method called to associate a Telefono object to this object
     * through the Telefono foreign key attribute.
     *
     * @param    Telefono $l Telefono
     * @return Incidencia The current object (for fluent API support)
     */
    public function addTelefonoIncidencia(Telefono $l)
    {
        if ($this->collTelefonoIncidencias === null) {
            $this->initTelefonoIncidencias();
            $this->collTelefonoIncidenciasPartial = true;
        }

        if (!in_array($l, $this->collTelefonoIncidencias->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTelefonoIncidencia($l);

            if ($this->telefonoIncidenciasScheduledForDeletion and $this->telefonoIncidenciasScheduledForDeletion->contains($l)) {
                $this->telefonoIncidenciasScheduledForDeletion->remove($this->telefonoIncidenciasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	TelefonoIncidencia $telefonoIncidencia The telefonoIncidencia object to add.
     */
    protected function doAddTelefonoIncidencia($telefonoIncidencia)
    {
        $this->collTelefonoIncidencias[]= $telefonoIncidencia;
        $telefonoIncidencia->setIncidencia($this);
    }

    /**
     * @param	TelefonoIncidencia $telefonoIncidencia The telefonoIncidencia object to remove.
     * @return Incidencia The current object (for fluent API support)
     */
    public function removeTelefonoIncidencia($telefonoIncidencia)
    {
        if ($this->getTelefonoIncidencias()->contains($telefonoIncidencia)) {
            $this->collTelefonoIncidencias->remove($this->collTelefonoIncidencias->search($telefonoIncidencia));
            if (null === $this->telefonoIncidenciasScheduledForDeletion) {
                $this->telefonoIncidenciasScheduledForDeletion = clone $this->collTelefonoIncidencias;
                $this->telefonoIncidenciasScheduledForDeletion->clear();
            }
            $this->telefonoIncidenciasScheduledForDeletion[]= $telefonoIncidencia;
            $telefonoIncidencia->setIncidencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related TelefonoIncidencias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoIncidenciasJoinPersona($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Persona', $join_behavior);

        return $this->getTelefonoIncidencias($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Incidencia is new, it will return
     * an empty collection; or if this Incidencia has previously
     * been saved, it will retrieve related TelefonoIncidencias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Incidencia.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoIncidenciasJoinEmpresa($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Empresa', $join_behavior);

        return $this->getTelefonoIncidencias($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->origen = null;
        $this->nombre_persona = null;
        $this->breve = null;
        $this->expediente = null;
        $this->descripcion = null;
        $this->atencion = null;
        $this->prioridad = null;
        $this->resuelta = null;
        $this->marcada = null;
        $this->eliminado = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->fecha_incidencia = null;
        $this->fecha_resolucion = null;
        $this->comunidad_id = null;
        $this->persona_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collAvisoIncs) {
                foreach ($this->collAvisoIncs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTareas) {
                foreach ($this->collTareas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSeguimientos) {
                foreach ($this->collSeguimientos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTelefonoIncidencias) {
                foreach ($this->collTelefonoIncidencias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aComunidad instanceof Persistent) {
              $this->aComunidad->clearAllReferences($deep);
            }
            if ($this->aPersona instanceof Persistent) {
              $this->aPersona->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collAvisoIncs instanceof PropelCollection) {
            $this->collAvisoIncs->clearIterator();
        }
        $this->collAvisoIncs = null;
        if ($this->collTareas instanceof PropelCollection) {
            $this->collTareas->clearIterator();
        }
        $this->collTareas = null;
        if ($this->collSeguimientos instanceof PropelCollection) {
            $this->collSeguimientos->clearIterator();
        }
        $this->collSeguimientos = null;
        if ($this->collTelefonoIncidencias instanceof PropelCollection) {
            $this->collTelefonoIncidencias->clearIterator();
        }
        $this->collTelefonoIncidencias = null;
        $this->aComunidad = null;
        $this->aPersona = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(IncidenciaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
