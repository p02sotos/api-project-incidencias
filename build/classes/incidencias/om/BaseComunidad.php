<?php


/**
 * Base class that represents a row from the 'comunidad' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseComunidad extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ComunidadPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ComunidadPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the direccion field.
     * @var        string
     */
    protected $direccion;

    /**
     * The value for the presidente field.
     * @var        string
     */
    protected $presidente;

    /**
     * The value for the cif field.
     * @var        string
     */
    protected $cif;

    /**
     * The value for the cuenta field.
     * @var        string
     */
    protected $cuenta;

    /**
     * The value for the marcada field.
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * @var        PropelObjectCollection|Incidencia[] Collection to store aggregation of Incidencia objects.
     */
    protected $collIncidenciaComs;
    protected $collIncidenciaComsPartial;

    /**
     * @var        PropelObjectCollection|Aviso[] Collection to store aggregation of Aviso objects.
     */
    protected $collAvisoComs;
    protected $collAvisoComsPartial;

    /**
     * @var        PropelObjectCollection|Entrega[] Collection to store aggregation of Entrega objects.
     */
    protected $collEntregaCs;
    protected $collEntregaCsPartial;

    /**
     * @var        PropelObjectCollection|Contabilidad[] Collection to store aggregation of Contabilidad objects.
     */
    protected $collContabilidads;
    protected $collContabilidadsPartial;

    /**
     * @var        PropelObjectCollection|Persona[] Collection to store aggregation of Persona objects.
     */
    protected $collPersonas;
    protected $collPersonasPartial;

    /**
     * @var        PropelObjectCollection|Empresa[] Collection to store aggregation of Empresa objects.
     */
    protected $collEmpresas;
    protected $collEmpresasPartial;

    /**
     * @var        PropelObjectCollection|Caja[] Collection to store aggregation of Caja objects.
     */
    protected $collCajas;
    protected $collCajasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $incidenciaComsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $avisoComsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $entregaCsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $contabilidadsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $personasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $empresasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cajasScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     * 
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [direccion] column value.
     * 
     * @return string
     */
    public function getDireccion()
    {

        return $this->direccion;
    }

    /**
     * Get the [presidente] column value.
     * 
     * @return string
     */
    public function getPresidente()
    {

        return $this->presidente;
    }

    /**
     * Get the [cif] column value.
     * 
     * @return string
     */
    public function getCif()
    {

        return $this->cif;
    }

    /**
     * Get the [cuenta] column value.
     * 
     * @return string
     */
    public function getCuenta()
    {

        return $this->cuenta;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ComunidadPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     * 
     * @param  string $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = ComunidadPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [direccion] column.
     * 
     * @param  string $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setDireccion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->direccion !== $v) {
            $this->direccion = $v;
            $this->modifiedColumns[] = ComunidadPeer::DIRECCION;
        }


        return $this;
    } // setDireccion()

    /**
     * Set the value of [presidente] column.
     * 
     * @param  string $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setPresidente($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->presidente !== $v) {
            $this->presidente = $v;
            $this->modifiedColumns[] = ComunidadPeer::PRESIDENTE;
        }


        return $this;
    } // setPresidente()

    /**
     * Set the value of [cif] column.
     * 
     * @param  string $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setCif($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cif !== $v) {
            $this->cif = $v;
            $this->modifiedColumns[] = ComunidadPeer::CIF;
        }


        return $this;
    } // setCif()

    /**
     * Set the value of [cuenta] column.
     * 
     * @param  string $v new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setCuenta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cuenta !== $v) {
            $this->cuenta = $v;
            $this->modifiedColumns[] = ComunidadPeer::CUENTA;
        }


        return $this;
    } // setCuenta()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Comunidad The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = ComunidadPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Comunidad The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = ComunidadPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Comunidad The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = ComunidadPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->direccion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->presidente = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->cif = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->cuenta = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->marcada = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->fecha_creacion = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->fecha_modificacion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = ComunidadPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Comunidad object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComunidadPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ComunidadPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collIncidenciaComs = null;

            $this->collAvisoComs = null;

            $this->collEntregaCs = null;

            $this->collContabilidads = null;

            $this->collPersonas = null;

            $this->collEmpresas = null;

            $this->collCajas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComunidadPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ComunidadQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComunidadPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ComunidadPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->incidenciaComsScheduledForDeletion !== null) {
                if (!$this->incidenciaComsScheduledForDeletion->isEmpty()) {
                    foreach ($this->incidenciaComsScheduledForDeletion as $incidenciaCom) {
                        // need to save related object because we set the relation to null
                        $incidenciaCom->save($con);
                    }
                    $this->incidenciaComsScheduledForDeletion = null;
                }
            }

            if ($this->collIncidenciaComs !== null) {
                foreach ($this->collIncidenciaComs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->avisoComsScheduledForDeletion !== null) {
                if (!$this->avisoComsScheduledForDeletion->isEmpty()) {
                    foreach ($this->avisoComsScheduledForDeletion as $avisoCom) {
                        // need to save related object because we set the relation to null
                        $avisoCom->save($con);
                    }
                    $this->avisoComsScheduledForDeletion = null;
                }
            }

            if ($this->collAvisoComs !== null) {
                foreach ($this->collAvisoComs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->entregaCsScheduledForDeletion !== null) {
                if (!$this->entregaCsScheduledForDeletion->isEmpty()) {
                    foreach ($this->entregaCsScheduledForDeletion as $entregaC) {
                        // need to save related object because we set the relation to null
                        $entregaC->save($con);
                    }
                    $this->entregaCsScheduledForDeletion = null;
                }
            }

            if ($this->collEntregaCs !== null) {
                foreach ($this->collEntregaCs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contabilidadsScheduledForDeletion !== null) {
                if (!$this->contabilidadsScheduledForDeletion->isEmpty()) {
                    ContabilidadQuery::create()
                        ->filterByPrimaryKeys($this->contabilidadsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contabilidadsScheduledForDeletion = null;
                }
            }

            if ($this->collContabilidads !== null) {
                foreach ($this->collContabilidads as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->personasScheduledForDeletion !== null) {
                if (!$this->personasScheduledForDeletion->isEmpty()) {
                    foreach ($this->personasScheduledForDeletion as $persona) {
                        // need to save related object because we set the relation to null
                        $persona->save($con);
                    }
                    $this->personasScheduledForDeletion = null;
                }
            }

            if ($this->collPersonas !== null) {
                foreach ($this->collPersonas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->empresasScheduledForDeletion !== null) {
                if (!$this->empresasScheduledForDeletion->isEmpty()) {
                    foreach ($this->empresasScheduledForDeletion as $empresa) {
                        // need to save related object because we set the relation to null
                        $empresa->save($con);
                    }
                    $this->empresasScheduledForDeletion = null;
                }
            }

            if ($this->collEmpresas !== null) {
                foreach ($this->collEmpresas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cajasScheduledForDeletion !== null) {
                if (!$this->cajasScheduledForDeletion->isEmpty()) {
                    foreach ($this->cajasScheduledForDeletion as $caja) {
                        // need to save related object because we set the relation to null
                        $caja->save($con);
                    }
                    $this->cajasScheduledForDeletion = null;
                }
            }

            if ($this->collCajas !== null) {
                foreach ($this->collCajas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ComunidadPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ComunidadPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ComunidadPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ComunidadPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(ComunidadPeer::DIRECCION)) {
            $modifiedColumns[':p' . $index++]  = '`direccion`';
        }
        if ($this->isColumnModified(ComunidadPeer::PRESIDENTE)) {
            $modifiedColumns[':p' . $index++]  = '`presidente`';
        }
        if ($this->isColumnModified(ComunidadPeer::CIF)) {
            $modifiedColumns[':p' . $index++]  = '`cif`';
        }
        if ($this->isColumnModified(ComunidadPeer::CUENTA)) {
            $modifiedColumns[':p' . $index++]  = '`cuenta`';
        }
        if ($this->isColumnModified(ComunidadPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(ComunidadPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(ComunidadPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }

        $sql = sprintf(
            'INSERT INTO `comunidad` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':						
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`direccion`':						
                        $stmt->bindValue($identifier, $this->direccion, PDO::PARAM_STR);
                        break;
                    case '`presidente`':						
                        $stmt->bindValue($identifier, $this->presidente, PDO::PARAM_STR);
                        break;
                    case '`cif`':						
                        $stmt->bindValue($identifier, $this->cif, PDO::PARAM_STR);
                        break;
                    case '`cuenta`':						
                        $stmt->bindValue($identifier, $this->cuenta, PDO::PARAM_STR);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ComunidadPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collIncidenciaComs !== null) {
                    foreach ($this->collIncidenciaComs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collAvisoComs !== null) {
                    foreach ($this->collAvisoComs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collEntregaCs !== null) {
                    foreach ($this->collEntregaCs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collContabilidads !== null) {
                    foreach ($this->collContabilidads as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPersonas !== null) {
                    foreach ($this->collPersonas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collEmpresas !== null) {
                    foreach ($this->collEmpresas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCajas !== null) {
                    foreach ($this->collCajas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ComunidadPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDireccion();
                break;
            case 3:
                return $this->getPresidente();
                break;
            case 4:
                return $this->getCif();
                break;
            case 5:
                return $this->getCuenta();
                break;
            case 6:
                return $this->getMarcada();
                break;
            case 7:
                return $this->getFechaCreacion();
                break;
            case 8:
                return $this->getFechaModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Comunidad'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Comunidad'][$this->getPrimaryKey()] = true;
        $keys = ComunidadPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDireccion(),
            $keys[3] => $this->getPresidente(),
            $keys[4] => $this->getCif(),
            $keys[5] => $this->getCuenta(),
            $keys[6] => $this->getMarcada(),
            $keys[7] => $this->getFechaCreacion(),
            $keys[8] => $this->getFechaModificacion(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->collIncidenciaComs) {
                $result['IncidenciaComs'] = $this->collIncidenciaComs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAvisoComs) {
                $result['AvisoComs'] = $this->collAvisoComs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEntregaCs) {
                $result['EntregaCs'] = $this->collEntregaCs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContabilidads) {
                $result['Contabilidads'] = $this->collContabilidads->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPersonas) {
                $result['Personas'] = $this->collPersonas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEmpresas) {
                $result['Empresas'] = $this->collEmpresas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCajas) {
                $result['Cajas'] = $this->collCajas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ComunidadPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDireccion($value);
                break;
            case 3:
                $this->setPresidente($value);
                break;
            case 4:
                $this->setCif($value);
                break;
            case 5:
                $this->setCuenta($value);
                break;
            case 6:
                $this->setMarcada($value);
                break;
            case 7:
                $this->setFechaCreacion($value);
                break;
            case 8:
                $this->setFechaModificacion($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ComunidadPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDireccion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPresidente($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCif($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCuenta($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMarcada($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFechaCreacion($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFechaModificacion($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ComunidadPeer::DATABASE_NAME);

        if ($this->isColumnModified(ComunidadPeer::ID)) $criteria->add(ComunidadPeer::ID, $this->id);
        if ($this->isColumnModified(ComunidadPeer::NOMBRE)) $criteria->add(ComunidadPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(ComunidadPeer::DIRECCION)) $criteria->add(ComunidadPeer::DIRECCION, $this->direccion);
        if ($this->isColumnModified(ComunidadPeer::PRESIDENTE)) $criteria->add(ComunidadPeer::PRESIDENTE, $this->presidente);
        if ($this->isColumnModified(ComunidadPeer::CIF)) $criteria->add(ComunidadPeer::CIF, $this->cif);
        if ($this->isColumnModified(ComunidadPeer::CUENTA)) $criteria->add(ComunidadPeer::CUENTA, $this->cuenta);
        if ($this->isColumnModified(ComunidadPeer::MARCADA)) $criteria->add(ComunidadPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(ComunidadPeer::FECHA_CREACION)) $criteria->add(ComunidadPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(ComunidadPeer::FECHA_MODIFICACION)) $criteria->add(ComunidadPeer::FECHA_MODIFICACION, $this->fecha_modificacion);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ComunidadPeer::DATABASE_NAME);
        $criteria->add(ComunidadPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Comunidad (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDireccion($this->getDireccion());
        $copyObj->setPresidente($this->getPresidente());
        $copyObj->setCif($this->getCif());
        $copyObj->setCuenta($this->getCuenta());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getIncidenciaComs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIncidenciaCom($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAvisoComs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAvisoCom($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEntregaCs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEntregaC($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContabilidads() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContabilidad($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPersonas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPersona($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEmpresas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEmpresa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCajas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCaja($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Comunidad Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ComunidadPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ComunidadPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('IncidenciaCom' == $relationName) {
            $this->initIncidenciaComs();
        }
        if ('AvisoCom' == $relationName) {
            $this->initAvisoComs();
        }
        if ('EntregaC' == $relationName) {
            $this->initEntregaCs();
        }
        if ('Contabilidad' == $relationName) {
            $this->initContabilidads();
        }
        if ('Persona' == $relationName) {
            $this->initPersonas();
        }
        if ('Empresa' == $relationName) {
            $this->initEmpresas();
        }
        if ('Caja' == $relationName) {
            $this->initCajas();
        }
    }

    /**
     * Clears out the collIncidenciaComs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addIncidenciaComs()
     */
    public function clearIncidenciaComs()
    {
        $this->collIncidenciaComs = null; // important to set this to null since that means it is uninitialized
        $this->collIncidenciaComsPartial = null;

        return $this;
    }

    /**
     * reset is the collIncidenciaComs collection loaded partially
     *
     * @return void
     */
    public function resetPartialIncidenciaComs($v = true)
    {
        $this->collIncidenciaComsPartial = $v;
    }

    /**
     * Initializes the collIncidenciaComs collection.
     *
     * By default this just sets the collIncidenciaComs collection to an empty array (like clearcollIncidenciaComs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIncidenciaComs($overrideExisting = true)
    {
        if (null !== $this->collIncidenciaComs && !$overrideExisting) {
            return;
        }
        $this->collIncidenciaComs = new PropelObjectCollection();
        $this->collIncidenciaComs->setModel('Incidencia');
    }

    /**
     * Gets an array of Incidencia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Incidencia[] List of Incidencia objects
     * @throws PropelException
     */
    public function getIncidenciaComs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collIncidenciaComsPartial && !$this->isNew();
        if (null === $this->collIncidenciaComs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIncidenciaComs) {
                // return empty collection
                $this->initIncidenciaComs();
            } else {
                $collIncidenciaComs = IncidenciaQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collIncidenciaComsPartial && count($collIncidenciaComs)) {
                      $this->initIncidenciaComs(false);

                      foreach ($collIncidenciaComs as $obj) {
                        if (false == $this->collIncidenciaComs->contains($obj)) {
                          $this->collIncidenciaComs->append($obj);
                        }
                      }

                      $this->collIncidenciaComsPartial = true;
                    }

                    $collIncidenciaComs->getInternalIterator()->rewind();

                    return $collIncidenciaComs;
                }

                if ($partial && $this->collIncidenciaComs) {
                    foreach ($this->collIncidenciaComs as $obj) {
                        if ($obj->isNew()) {
                            $collIncidenciaComs[] = $obj;
                        }
                    }
                }

                $this->collIncidenciaComs = $collIncidenciaComs;
                $this->collIncidenciaComsPartial = false;
            }
        }

        return $this->collIncidenciaComs;
    }

    /**
     * Sets a collection of IncidenciaCom objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $incidenciaComs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setIncidenciaComs(PropelCollection $incidenciaComs, PropelPDO $con = null)
    {
        $incidenciaComsToDelete = $this->getIncidenciaComs(new Criteria(), $con)->diff($incidenciaComs);


        $this->incidenciaComsScheduledForDeletion = $incidenciaComsToDelete;

        foreach ($incidenciaComsToDelete as $incidenciaComRemoved) {
            $incidenciaComRemoved->setComunidad(null);
        }

        $this->collIncidenciaComs = null;
        foreach ($incidenciaComs as $incidenciaCom) {
            $this->addIncidenciaCom($incidenciaCom);
        }

        $this->collIncidenciaComs = $incidenciaComs;
        $this->collIncidenciaComsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Incidencia objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Incidencia objects.
     * @throws PropelException
     */
    public function countIncidenciaComs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collIncidenciaComsPartial && !$this->isNew();
        if (null === $this->collIncidenciaComs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIncidenciaComs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIncidenciaComs());
            }
            $query = IncidenciaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collIncidenciaComs);
    }

    /**
     * Method called to associate a Incidencia object to this object
     * through the Incidencia foreign key attribute.
     *
     * @param    Incidencia $l Incidencia
     * @return Comunidad The current object (for fluent API support)
     */
    public function addIncidenciaCom(Incidencia $l)
    {
        if ($this->collIncidenciaComs === null) {
            $this->initIncidenciaComs();
            $this->collIncidenciaComsPartial = true;
        }

        if (!in_array($l, $this->collIncidenciaComs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddIncidenciaCom($l);

            if ($this->incidenciaComsScheduledForDeletion and $this->incidenciaComsScheduledForDeletion->contains($l)) {
                $this->incidenciaComsScheduledForDeletion->remove($this->incidenciaComsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	IncidenciaCom $incidenciaCom The incidenciaCom object to add.
     */
    protected function doAddIncidenciaCom($incidenciaCom)
    {
        $this->collIncidenciaComs[]= $incidenciaCom;
        $incidenciaCom->setComunidad($this);
    }

    /**
     * @param	IncidenciaCom $incidenciaCom The incidenciaCom object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeIncidenciaCom($incidenciaCom)
    {
        if ($this->getIncidenciaComs()->contains($incidenciaCom)) {
            $this->collIncidenciaComs->remove($this->collIncidenciaComs->search($incidenciaCom));
            if (null === $this->incidenciaComsScheduledForDeletion) {
                $this->incidenciaComsScheduledForDeletion = clone $this->collIncidenciaComs;
                $this->incidenciaComsScheduledForDeletion->clear();
            }
            $this->incidenciaComsScheduledForDeletion[]= $incidenciaCom;
            $incidenciaCom->setComunidad(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related IncidenciaComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Incidencia[] List of Incidencia objects
     */
    public function getIncidenciaComsJoinPersona($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = IncidenciaQuery::create(null, $criteria);
        $query->joinWith('Persona', $join_behavior);

        return $this->getIncidenciaComs($query, $con);
    }

    /**
     * Clears out the collAvisoComs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addAvisoComs()
     */
    public function clearAvisoComs()
    {
        $this->collAvisoComs = null; // important to set this to null since that means it is uninitialized
        $this->collAvisoComsPartial = null;

        return $this;
    }

    /**
     * reset is the collAvisoComs collection loaded partially
     *
     * @return void
     */
    public function resetPartialAvisoComs($v = true)
    {
        $this->collAvisoComsPartial = $v;
    }

    /**
     * Initializes the collAvisoComs collection.
     *
     * By default this just sets the collAvisoComs collection to an empty array (like clearcollAvisoComs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAvisoComs($overrideExisting = true)
    {
        if (null !== $this->collAvisoComs && !$overrideExisting) {
            return;
        }
        $this->collAvisoComs = new PropelObjectCollection();
        $this->collAvisoComs->setModel('Aviso');
    }

    /**
     * Gets an array of Aviso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     * @throws PropelException
     */
    public function getAvisoComs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collAvisoComsPartial && !$this->isNew();
        if (null === $this->collAvisoComs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collAvisoComs) {
                // return empty collection
                $this->initAvisoComs();
            } else {
                $collAvisoComs = AvisoQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collAvisoComsPartial && count($collAvisoComs)) {
                      $this->initAvisoComs(false);

                      foreach ($collAvisoComs as $obj) {
                        if (false == $this->collAvisoComs->contains($obj)) {
                          $this->collAvisoComs->append($obj);
                        }
                      }

                      $this->collAvisoComsPartial = true;
                    }

                    $collAvisoComs->getInternalIterator()->rewind();

                    return $collAvisoComs;
                }

                if ($partial && $this->collAvisoComs) {
                    foreach ($this->collAvisoComs as $obj) {
                        if ($obj->isNew()) {
                            $collAvisoComs[] = $obj;
                        }
                    }
                }

                $this->collAvisoComs = $collAvisoComs;
                $this->collAvisoComsPartial = false;
            }
        }

        return $this->collAvisoComs;
    }

    /**
     * Sets a collection of AvisoCom objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $avisoComs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setAvisoComs(PropelCollection $avisoComs, PropelPDO $con = null)
    {
        $avisoComsToDelete = $this->getAvisoComs(new Criteria(), $con)->diff($avisoComs);


        $this->avisoComsScheduledForDeletion = $avisoComsToDelete;

        foreach ($avisoComsToDelete as $avisoComRemoved) {
            $avisoComRemoved->setComunidad(null);
        }

        $this->collAvisoComs = null;
        foreach ($avisoComs as $avisoCom) {
            $this->addAvisoCom($avisoCom);
        }

        $this->collAvisoComs = $avisoComs;
        $this->collAvisoComsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Aviso objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Aviso objects.
     * @throws PropelException
     */
    public function countAvisoComs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collAvisoComsPartial && !$this->isNew();
        if (null === $this->collAvisoComs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAvisoComs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAvisoComs());
            }
            $query = AvisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collAvisoComs);
    }

    /**
     * Method called to associate a Aviso object to this object
     * through the Aviso foreign key attribute.
     *
     * @param    Aviso $l Aviso
     * @return Comunidad The current object (for fluent API support)
     */
    public function addAvisoCom(Aviso $l)
    {
        if ($this->collAvisoComs === null) {
            $this->initAvisoComs();
            $this->collAvisoComsPartial = true;
        }

        if (!in_array($l, $this->collAvisoComs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddAvisoCom($l);

            if ($this->avisoComsScheduledForDeletion and $this->avisoComsScheduledForDeletion->contains($l)) {
                $this->avisoComsScheduledForDeletion->remove($this->avisoComsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	AvisoCom $avisoCom The avisoCom object to add.
     */
    protected function doAddAvisoCom($avisoCom)
    {
        $this->collAvisoComs[]= $avisoCom;
        $avisoCom->setComunidad($this);
    }

    /**
     * @param	AvisoCom $avisoCom The avisoCom object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeAvisoCom($avisoCom)
    {
        if ($this->getAvisoComs()->contains($avisoCom)) {
            $this->collAvisoComs->remove($this->collAvisoComs->search($avisoCom));
            if (null === $this->avisoComsScheduledForDeletion) {
                $this->avisoComsScheduledForDeletion = clone $this->collAvisoComs;
                $this->avisoComsScheduledForDeletion->clear();
            }
            $this->avisoComsScheduledForDeletion[]= $avisoCom;
            $avisoCom->setComunidad(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related AvisoComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoComsJoinIncidencia($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Incidencia', $join_behavior);

        return $this->getAvisoComs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related AvisoComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoComsJoinTarea($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Tarea', $join_behavior);

        return $this->getAvisoComs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related AvisoComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoComsJoinPersona($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Persona', $join_behavior);

        return $this->getAvisoComs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related AvisoComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoComsJoinEntrega($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Entrega', $join_behavior);

        return $this->getAvisoComs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related AvisoComs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Aviso[] List of Aviso objects
     */
    public function getAvisoComsJoinContabilidad($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = AvisoQuery::create(null, $criteria);
        $query->joinWith('Contabilidad', $join_behavior);

        return $this->getAvisoComs($query, $con);
    }

    /**
     * Clears out the collEntregaCs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addEntregaCs()
     */
    public function clearEntregaCs()
    {
        $this->collEntregaCs = null; // important to set this to null since that means it is uninitialized
        $this->collEntregaCsPartial = null;

        return $this;
    }

    /**
     * reset is the collEntregaCs collection loaded partially
     *
     * @return void
     */
    public function resetPartialEntregaCs($v = true)
    {
        $this->collEntregaCsPartial = $v;
    }

    /**
     * Initializes the collEntregaCs collection.
     *
     * By default this just sets the collEntregaCs collection to an empty array (like clearcollEntregaCs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEntregaCs($overrideExisting = true)
    {
        if (null !== $this->collEntregaCs && !$overrideExisting) {
            return;
        }
        $this->collEntregaCs = new PropelObjectCollection();
        $this->collEntregaCs->setModel('Entrega');
    }

    /**
     * Gets an array of Entrega objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Entrega[] List of Entrega objects
     * @throws PropelException
     */
    public function getEntregaCs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collEntregaCsPartial && !$this->isNew();
        if (null === $this->collEntregaCs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEntregaCs) {
                // return empty collection
                $this->initEntregaCs();
            } else {
                $collEntregaCs = EntregaQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collEntregaCsPartial && count($collEntregaCs)) {
                      $this->initEntregaCs(false);

                      foreach ($collEntregaCs as $obj) {
                        if (false == $this->collEntregaCs->contains($obj)) {
                          $this->collEntregaCs->append($obj);
                        }
                      }

                      $this->collEntregaCsPartial = true;
                    }

                    $collEntregaCs->getInternalIterator()->rewind();

                    return $collEntregaCs;
                }

                if ($partial && $this->collEntregaCs) {
                    foreach ($this->collEntregaCs as $obj) {
                        if ($obj->isNew()) {
                            $collEntregaCs[] = $obj;
                        }
                    }
                }

                $this->collEntregaCs = $collEntregaCs;
                $this->collEntregaCsPartial = false;
            }
        }

        return $this->collEntregaCs;
    }

    /**
     * Sets a collection of EntregaC objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $entregaCs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setEntregaCs(PropelCollection $entregaCs, PropelPDO $con = null)
    {
        $entregaCsToDelete = $this->getEntregaCs(new Criteria(), $con)->diff($entregaCs);


        $this->entregaCsScheduledForDeletion = $entregaCsToDelete;

        foreach ($entregaCsToDelete as $entregaCRemoved) {
            $entregaCRemoved->setComunidad(null);
        }

        $this->collEntregaCs = null;
        foreach ($entregaCs as $entregaC) {
            $this->addEntregaC($entregaC);
        }

        $this->collEntregaCs = $entregaCs;
        $this->collEntregaCsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Entrega objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Entrega objects.
     * @throws PropelException
     */
    public function countEntregaCs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collEntregaCsPartial && !$this->isNew();
        if (null === $this->collEntregaCs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEntregaCs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEntregaCs());
            }
            $query = EntregaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collEntregaCs);
    }

    /**
     * Method called to associate a Entrega object to this object
     * through the Entrega foreign key attribute.
     *
     * @param    Entrega $l Entrega
     * @return Comunidad The current object (for fluent API support)
     */
    public function addEntregaC(Entrega $l)
    {
        if ($this->collEntregaCs === null) {
            $this->initEntregaCs();
            $this->collEntregaCsPartial = true;
        }

        if (!in_array($l, $this->collEntregaCs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddEntregaC($l);

            if ($this->entregaCsScheduledForDeletion and $this->entregaCsScheduledForDeletion->contains($l)) {
                $this->entregaCsScheduledForDeletion->remove($this->entregaCsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	EntregaC $entregaC The entregaC object to add.
     */
    protected function doAddEntregaC($entregaC)
    {
        $this->collEntregaCs[]= $entregaC;
        $entregaC->setComunidad($this);
    }

    /**
     * @param	EntregaC $entregaC The entregaC object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeEntregaC($entregaC)
    {
        if ($this->getEntregaCs()->contains($entregaC)) {
            $this->collEntregaCs->remove($this->collEntregaCs->search($entregaC));
            if (null === $this->entregaCsScheduledForDeletion) {
                $this->entregaCsScheduledForDeletion = clone $this->collEntregaCs;
                $this->entregaCsScheduledForDeletion->clear();
            }
            $this->entregaCsScheduledForDeletion[]= $entregaC;
            $entregaC->setComunidad(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comunidad is new, it will return
     * an empty collection; or if this Comunidad has previously
     * been saved, it will retrieve related EntregaCs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comunidad.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Entrega[] List of Entrega objects
     */
    public function getEntregaCsJoinTipo($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = EntregaQuery::create(null, $criteria);
        $query->joinWith('Tipo', $join_behavior);

        return $this->getEntregaCs($query, $con);
    }

    /**
     * Clears out the collContabilidads collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addContabilidads()
     */
    public function clearContabilidads()
    {
        $this->collContabilidads = null; // important to set this to null since that means it is uninitialized
        $this->collContabilidadsPartial = null;

        return $this;
    }

    /**
     * reset is the collContabilidads collection loaded partially
     *
     * @return void
     */
    public function resetPartialContabilidads($v = true)
    {
        $this->collContabilidadsPartial = $v;
    }

    /**
     * Initializes the collContabilidads collection.
     *
     * By default this just sets the collContabilidads collection to an empty array (like clearcollContabilidads());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContabilidads($overrideExisting = true)
    {
        if (null !== $this->collContabilidads && !$overrideExisting) {
            return;
        }
        $this->collContabilidads = new PropelObjectCollection();
        $this->collContabilidads->setModel('Contabilidad');
    }

    /**
     * Gets an array of Contabilidad objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Contabilidad[] List of Contabilidad objects
     * @throws PropelException
     */
    public function getContabilidads($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collContabilidadsPartial && !$this->isNew();
        if (null === $this->collContabilidads || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContabilidads) {
                // return empty collection
                $this->initContabilidads();
            } else {
                $collContabilidads = ContabilidadQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collContabilidadsPartial && count($collContabilidads)) {
                      $this->initContabilidads(false);

                      foreach ($collContabilidads as $obj) {
                        if (false == $this->collContabilidads->contains($obj)) {
                          $this->collContabilidads->append($obj);
                        }
                      }

                      $this->collContabilidadsPartial = true;
                    }

                    $collContabilidads->getInternalIterator()->rewind();

                    return $collContabilidads;
                }

                if ($partial && $this->collContabilidads) {
                    foreach ($this->collContabilidads as $obj) {
                        if ($obj->isNew()) {
                            $collContabilidads[] = $obj;
                        }
                    }
                }

                $this->collContabilidads = $collContabilidads;
                $this->collContabilidadsPartial = false;
            }
        }

        return $this->collContabilidads;
    }

    /**
     * Sets a collection of Contabilidad objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $contabilidads A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setContabilidads(PropelCollection $contabilidads, PropelPDO $con = null)
    {
        $contabilidadsToDelete = $this->getContabilidads(new Criteria(), $con)->diff($contabilidads);


        $this->contabilidadsScheduledForDeletion = $contabilidadsToDelete;

        foreach ($contabilidadsToDelete as $contabilidadRemoved) {
            $contabilidadRemoved->setComunidad(null);
        }

        $this->collContabilidads = null;
        foreach ($contabilidads as $contabilidad) {
            $this->addContabilidad($contabilidad);
        }

        $this->collContabilidads = $contabilidads;
        $this->collContabilidadsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Contabilidad objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Contabilidad objects.
     * @throws PropelException
     */
    public function countContabilidads(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collContabilidadsPartial && !$this->isNew();
        if (null === $this->collContabilidads || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContabilidads) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContabilidads());
            }
            $query = ContabilidadQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collContabilidads);
    }

    /**
     * Method called to associate a Contabilidad object to this object
     * through the Contabilidad foreign key attribute.
     *
     * @param    Contabilidad $l Contabilidad
     * @return Comunidad The current object (for fluent API support)
     */
    public function addContabilidad(Contabilidad $l)
    {
        if ($this->collContabilidads === null) {
            $this->initContabilidads();
            $this->collContabilidadsPartial = true;
        }

        if (!in_array($l, $this->collContabilidads->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddContabilidad($l);

            if ($this->contabilidadsScheduledForDeletion and $this->contabilidadsScheduledForDeletion->contains($l)) {
                $this->contabilidadsScheduledForDeletion->remove($this->contabilidadsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Contabilidad $contabilidad The contabilidad object to add.
     */
    protected function doAddContabilidad($contabilidad)
    {
        $this->collContabilidads[]= $contabilidad;
        $contabilidad->setComunidad($this);
    }

    /**
     * @param	Contabilidad $contabilidad The contabilidad object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeContabilidad($contabilidad)
    {
        if ($this->getContabilidads()->contains($contabilidad)) {
            $this->collContabilidads->remove($this->collContabilidads->search($contabilidad));
            if (null === $this->contabilidadsScheduledForDeletion) {
                $this->contabilidadsScheduledForDeletion = clone $this->collContabilidads;
                $this->contabilidadsScheduledForDeletion->clear();
            }
            $this->contabilidadsScheduledForDeletion[]= clone $contabilidad;
            $contabilidad->setComunidad(null);
        }

        return $this;
    }

    /**
     * Clears out the collPersonas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addPersonas()
     */
    public function clearPersonas()
    {
        $this->collPersonas = null; // important to set this to null since that means it is uninitialized
        $this->collPersonasPartial = null;

        return $this;
    }

    /**
     * reset is the collPersonas collection loaded partially
     *
     * @return void
     */
    public function resetPartialPersonas($v = true)
    {
        $this->collPersonasPartial = $v;
    }

    /**
     * Initializes the collPersonas collection.
     *
     * By default this just sets the collPersonas collection to an empty array (like clearcollPersonas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPersonas($overrideExisting = true)
    {
        if (null !== $this->collPersonas && !$overrideExisting) {
            return;
        }
        $this->collPersonas = new PropelObjectCollection();
        $this->collPersonas->setModel('Persona');
    }

    /**
     * Gets an array of Persona objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Persona[] List of Persona objects
     * @throws PropelException
     */
    public function getPersonas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPersonasPartial && !$this->isNew();
        if (null === $this->collPersonas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPersonas) {
                // return empty collection
                $this->initPersonas();
            } else {
                $collPersonas = PersonaQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPersonasPartial && count($collPersonas)) {
                      $this->initPersonas(false);

                      foreach ($collPersonas as $obj) {
                        if (false == $this->collPersonas->contains($obj)) {
                          $this->collPersonas->append($obj);
                        }
                      }

                      $this->collPersonasPartial = true;
                    }

                    $collPersonas->getInternalIterator()->rewind();

                    return $collPersonas;
                }

                if ($partial && $this->collPersonas) {
                    foreach ($this->collPersonas as $obj) {
                        if ($obj->isNew()) {
                            $collPersonas[] = $obj;
                        }
                    }
                }

                $this->collPersonas = $collPersonas;
                $this->collPersonasPartial = false;
            }
        }

        return $this->collPersonas;
    }

    /**
     * Sets a collection of Persona objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $personas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setPersonas(PropelCollection $personas, PropelPDO $con = null)
    {
        $personasToDelete = $this->getPersonas(new Criteria(), $con)->diff($personas);


        $this->personasScheduledForDeletion = $personasToDelete;

        foreach ($personasToDelete as $personaRemoved) {
            $personaRemoved->setComunidad(null);
        }

        $this->collPersonas = null;
        foreach ($personas as $persona) {
            $this->addPersona($persona);
        }

        $this->collPersonas = $personas;
        $this->collPersonasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Persona objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Persona objects.
     * @throws PropelException
     */
    public function countPersonas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPersonasPartial && !$this->isNew();
        if (null === $this->collPersonas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPersonas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPersonas());
            }
            $query = PersonaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collPersonas);
    }

    /**
     * Method called to associate a Persona object to this object
     * through the Persona foreign key attribute.
     *
     * @param    Persona $l Persona
     * @return Comunidad The current object (for fluent API support)
     */
    public function addPersona(Persona $l)
    {
        if ($this->collPersonas === null) {
            $this->initPersonas();
            $this->collPersonasPartial = true;
        }

        if (!in_array($l, $this->collPersonas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPersona($l);

            if ($this->personasScheduledForDeletion and $this->personasScheduledForDeletion->contains($l)) {
                $this->personasScheduledForDeletion->remove($this->personasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Persona $persona The persona object to add.
     */
    protected function doAddPersona($persona)
    {
        $this->collPersonas[]= $persona;
        $persona->setComunidad($this);
    }

    /**
     * @param	Persona $persona The persona object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removePersona($persona)
    {
        if ($this->getPersonas()->contains($persona)) {
            $this->collPersonas->remove($this->collPersonas->search($persona));
            if (null === $this->personasScheduledForDeletion) {
                $this->personasScheduledForDeletion = clone $this->collPersonas;
                $this->personasScheduledForDeletion->clear();
            }
            $this->personasScheduledForDeletion[]= $persona;
            $persona->setComunidad(null);
        }

        return $this;
    }

    /**
     * Clears out the collEmpresas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addEmpresas()
     */
    public function clearEmpresas()
    {
        $this->collEmpresas = null; // important to set this to null since that means it is uninitialized
        $this->collEmpresasPartial = null;

        return $this;
    }

    /**
     * reset is the collEmpresas collection loaded partially
     *
     * @return void
     */
    public function resetPartialEmpresas($v = true)
    {
        $this->collEmpresasPartial = $v;
    }

    /**
     * Initializes the collEmpresas collection.
     *
     * By default this just sets the collEmpresas collection to an empty array (like clearcollEmpresas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEmpresas($overrideExisting = true)
    {
        if (null !== $this->collEmpresas && !$overrideExisting) {
            return;
        }
        $this->collEmpresas = new PropelObjectCollection();
        $this->collEmpresas->setModel('Empresa');
    }

    /**
     * Gets an array of Empresa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Empresa[] List of Empresa objects
     * @throws PropelException
     */
    public function getEmpresas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collEmpresasPartial && !$this->isNew();
        if (null === $this->collEmpresas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEmpresas) {
                // return empty collection
                $this->initEmpresas();
            } else {
                $collEmpresas = EmpresaQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collEmpresasPartial && count($collEmpresas)) {
                      $this->initEmpresas(false);

                      foreach ($collEmpresas as $obj) {
                        if (false == $this->collEmpresas->contains($obj)) {
                          $this->collEmpresas->append($obj);
                        }
                      }

                      $this->collEmpresasPartial = true;
                    }

                    $collEmpresas->getInternalIterator()->rewind();

                    return $collEmpresas;
                }

                if ($partial && $this->collEmpresas) {
                    foreach ($this->collEmpresas as $obj) {
                        if ($obj->isNew()) {
                            $collEmpresas[] = $obj;
                        }
                    }
                }

                $this->collEmpresas = $collEmpresas;
                $this->collEmpresasPartial = false;
            }
        }

        return $this->collEmpresas;
    }

    /**
     * Sets a collection of Empresa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $empresas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setEmpresas(PropelCollection $empresas, PropelPDO $con = null)
    {
        $empresasToDelete = $this->getEmpresas(new Criteria(), $con)->diff($empresas);


        $this->empresasScheduledForDeletion = $empresasToDelete;

        foreach ($empresasToDelete as $empresaRemoved) {
            $empresaRemoved->setComunidad(null);
        }

        $this->collEmpresas = null;
        foreach ($empresas as $empresa) {
            $this->addEmpresa($empresa);
        }

        $this->collEmpresas = $empresas;
        $this->collEmpresasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Empresa objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Empresa objects.
     * @throws PropelException
     */
    public function countEmpresas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collEmpresasPartial && !$this->isNew();
        if (null === $this->collEmpresas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEmpresas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEmpresas());
            }
            $query = EmpresaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collEmpresas);
    }

    /**
     * Method called to associate a Empresa object to this object
     * through the Empresa foreign key attribute.
     *
     * @param    Empresa $l Empresa
     * @return Comunidad The current object (for fluent API support)
     */
    public function addEmpresa(Empresa $l)
    {
        if ($this->collEmpresas === null) {
            $this->initEmpresas();
            $this->collEmpresasPartial = true;
        }

        if (!in_array($l, $this->collEmpresas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddEmpresa($l);

            if ($this->empresasScheduledForDeletion and $this->empresasScheduledForDeletion->contains($l)) {
                $this->empresasScheduledForDeletion->remove($this->empresasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Empresa $empresa The empresa object to add.
     */
    protected function doAddEmpresa($empresa)
    {
        $this->collEmpresas[]= $empresa;
        $empresa->setComunidad($this);
    }

    /**
     * @param	Empresa $empresa The empresa object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeEmpresa($empresa)
    {
        if ($this->getEmpresas()->contains($empresa)) {
            $this->collEmpresas->remove($this->collEmpresas->search($empresa));
            if (null === $this->empresasScheduledForDeletion) {
                $this->empresasScheduledForDeletion = clone $this->collEmpresas;
                $this->empresasScheduledForDeletion->clear();
            }
            $this->empresasScheduledForDeletion[]= $empresa;
            $empresa->setComunidad(null);
        }

        return $this;
    }

    /**
     * Clears out the collCajas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comunidad The current object (for fluent API support)
     * @see        addCajas()
     */
    public function clearCajas()
    {
        $this->collCajas = null; // important to set this to null since that means it is uninitialized
        $this->collCajasPartial = null;

        return $this;
    }

    /**
     * reset is the collCajas collection loaded partially
     *
     * @return void
     */
    public function resetPartialCajas($v = true)
    {
        $this->collCajasPartial = $v;
    }

    /**
     * Initializes the collCajas collection.
     *
     * By default this just sets the collCajas collection to an empty array (like clearcollCajas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCajas($overrideExisting = true)
    {
        if (null !== $this->collCajas && !$overrideExisting) {
            return;
        }
        $this->collCajas = new PropelObjectCollection();
        $this->collCajas->setModel('Caja');
    }

    /**
     * Gets an array of Caja objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comunidad is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Caja[] List of Caja objects
     * @throws PropelException
     */
    public function getCajas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCajasPartial && !$this->isNew();
        if (null === $this->collCajas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCajas) {
                // return empty collection
                $this->initCajas();
            } else {
                $collCajas = CajaQuery::create(null, $criteria)
                    ->filterByComunidad($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCajasPartial && count($collCajas)) {
                      $this->initCajas(false);

                      foreach ($collCajas as $obj) {
                        if (false == $this->collCajas->contains($obj)) {
                          $this->collCajas->append($obj);
                        }
                      }

                      $this->collCajasPartial = true;
                    }

                    $collCajas->getInternalIterator()->rewind();

                    return $collCajas;
                }

                if ($partial && $this->collCajas) {
                    foreach ($this->collCajas as $obj) {
                        if ($obj->isNew()) {
                            $collCajas[] = $obj;
                        }
                    }
                }

                $this->collCajas = $collCajas;
                $this->collCajasPartial = false;
            }
        }

        return $this->collCajas;
    }

    /**
     * Sets a collection of Caja objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cajas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comunidad The current object (for fluent API support)
     */
    public function setCajas(PropelCollection $cajas, PropelPDO $con = null)
    {
        $cajasToDelete = $this->getCajas(new Criteria(), $con)->diff($cajas);


        $this->cajasScheduledForDeletion = $cajasToDelete;

        foreach ($cajasToDelete as $cajaRemoved) {
            $cajaRemoved->setComunidad(null);
        }

        $this->collCajas = null;
        foreach ($cajas as $caja) {
            $this->addCaja($caja);
        }

        $this->collCajas = $cajas;
        $this->collCajasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Caja objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Caja objects.
     * @throws PropelException
     */
    public function countCajas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCajasPartial && !$this->isNew();
        if (null === $this->collCajas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCajas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCajas());
            }
            $query = CajaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComunidad($this)
                ->count($con);
        }

        return count($this->collCajas);
    }

    /**
     * Method called to associate a Caja object to this object
     * through the Caja foreign key attribute.
     *
     * @param    Caja $l Caja
     * @return Comunidad The current object (for fluent API support)
     */
    public function addCaja(Caja $l)
    {
        if ($this->collCajas === null) {
            $this->initCajas();
            $this->collCajasPartial = true;
        }

        if (!in_array($l, $this->collCajas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCaja($l);

            if ($this->cajasScheduledForDeletion and $this->cajasScheduledForDeletion->contains($l)) {
                $this->cajasScheduledForDeletion->remove($this->cajasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Caja $caja The caja object to add.
     */
    protected function doAddCaja($caja)
    {
        $this->collCajas[]= $caja;
        $caja->setComunidad($this);
    }

    /**
     * @param	Caja $caja The caja object to remove.
     * @return Comunidad The current object (for fluent API support)
     */
    public function removeCaja($caja)
    {
        if ($this->getCajas()->contains($caja)) {
            $this->collCajas->remove($this->collCajas->search($caja));
            if (null === $this->cajasScheduledForDeletion) {
                $this->cajasScheduledForDeletion = clone $this->collCajas;
                $this->cajasScheduledForDeletion->clear();
            }
            $this->cajasScheduledForDeletion[]= $caja;
            $caja->setComunidad(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->direccion = null;
        $this->presidente = null;
        $this->cif = null;
        $this->cuenta = null;
        $this->marcada = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collIncidenciaComs) {
                foreach ($this->collIncidenciaComs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAvisoComs) {
                foreach ($this->collAvisoComs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEntregaCs) {
                foreach ($this->collEntregaCs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContabilidads) {
                foreach ($this->collContabilidads as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPersonas) {
                foreach ($this->collPersonas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEmpresas) {
                foreach ($this->collEmpresas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCajas) {
                foreach ($this->collCajas as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collIncidenciaComs instanceof PropelCollection) {
            $this->collIncidenciaComs->clearIterator();
        }
        $this->collIncidenciaComs = null;
        if ($this->collAvisoComs instanceof PropelCollection) {
            $this->collAvisoComs->clearIterator();
        }
        $this->collAvisoComs = null;
        if ($this->collEntregaCs instanceof PropelCollection) {
            $this->collEntregaCs->clearIterator();
        }
        $this->collEntregaCs = null;
        if ($this->collContabilidads instanceof PropelCollection) {
            $this->collContabilidads->clearIterator();
        }
        $this->collContabilidads = null;
        if ($this->collPersonas instanceof PropelCollection) {
            $this->collPersonas->clearIterator();
        }
        $this->collPersonas = null;
        if ($this->collEmpresas instanceof PropelCollection) {
            $this->collEmpresas->clearIterator();
        }
        $this->collEmpresas = null;
        if ($this->collCajas instanceof PropelCollection) {
            $this->collCajas->clearIterator();
        }
        $this->collCajas = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ComunidadPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
