<?php


/**
 * Base class that represents a query for the 'incidencia' table.
 *
 * 
 *
 * @method IncidenciaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method IncidenciaQuery orderByOrigen($order = Criteria::ASC) Order by the origen column
 * @method IncidenciaQuery orderByNombrePersona($order = Criteria::ASC) Order by the nombre_persona column
 * @method IncidenciaQuery orderByBreve($order = Criteria::ASC) Order by the breve column
 * @method IncidenciaQuery orderByExpediente($order = Criteria::ASC) Order by the expediente column
 * @method IncidenciaQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method IncidenciaQuery orderByAtencion($order = Criteria::ASC) Order by the atencion column
 * @method IncidenciaQuery orderByPrioridad($order = Criteria::ASC) Order by the prioridad column
 * @method IncidenciaQuery orderByResuelta($order = Criteria::ASC) Order by the resuelta column
 * @method IncidenciaQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method IncidenciaQuery orderByEliminado($order = Criteria::ASC) Order by the eliminado column
 * @method IncidenciaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method IncidenciaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method IncidenciaQuery orderByFechaIncidencia($order = Criteria::ASC) Order by the fecha_incidencia column
 * @method IncidenciaQuery orderByFechaResolucion($order = Criteria::ASC) Order by the fecha_resolucion column
 * @method IncidenciaQuery orderByComunidadId($order = Criteria::ASC) Order by the comunidad_id column
 * @method IncidenciaQuery orderByPersonaId($order = Criteria::ASC) Order by the persona_id column
 *
 * @method IncidenciaQuery groupById() Group by the id column
 * @method IncidenciaQuery groupByOrigen() Group by the origen column
 * @method IncidenciaQuery groupByNombrePersona() Group by the nombre_persona column
 * @method IncidenciaQuery groupByBreve() Group by the breve column
 * @method IncidenciaQuery groupByExpediente() Group by the expediente column
 * @method IncidenciaQuery groupByDescripcion() Group by the descripcion column
 * @method IncidenciaQuery groupByAtencion() Group by the atencion column
 * @method IncidenciaQuery groupByPrioridad() Group by the prioridad column
 * @method IncidenciaQuery groupByResuelta() Group by the resuelta column
 * @method IncidenciaQuery groupByMarcada() Group by the marcada column
 * @method IncidenciaQuery groupByEliminado() Group by the eliminado column
 * @method IncidenciaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method IncidenciaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method IncidenciaQuery groupByFechaIncidencia() Group by the fecha_incidencia column
 * @method IncidenciaQuery groupByFechaResolucion() Group by the fecha_resolucion column
 * @method IncidenciaQuery groupByComunidadId() Group by the comunidad_id column
 * @method IncidenciaQuery groupByPersonaId() Group by the persona_id column
 *
 * @method IncidenciaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method IncidenciaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method IncidenciaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method IncidenciaQuery leftJoinComunidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comunidad relation
 * @method IncidenciaQuery rightJoinComunidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comunidad relation
 * @method IncidenciaQuery innerJoinComunidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Comunidad relation
 *
 * @method IncidenciaQuery leftJoinPersona($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persona relation
 * @method IncidenciaQuery rightJoinPersona($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persona relation
 * @method IncidenciaQuery innerJoinPersona($relationAlias = null) Adds a INNER JOIN clause to the query using the Persona relation
 *
 * @method IncidenciaQuery leftJoinAvisoInc($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvisoInc relation
 * @method IncidenciaQuery rightJoinAvisoInc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvisoInc relation
 * @method IncidenciaQuery innerJoinAvisoInc($relationAlias = null) Adds a INNER JOIN clause to the query using the AvisoInc relation
 *
 * @method IncidenciaQuery leftJoinTarea($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tarea relation
 * @method IncidenciaQuery rightJoinTarea($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tarea relation
 * @method IncidenciaQuery innerJoinTarea($relationAlias = null) Adds a INNER JOIN clause to the query using the Tarea relation
 *
 * @method IncidenciaQuery leftJoinSeguimiento($relationAlias = null) Adds a LEFT JOIN clause to the query using the Seguimiento relation
 * @method IncidenciaQuery rightJoinSeguimiento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Seguimiento relation
 * @method IncidenciaQuery innerJoinSeguimiento($relationAlias = null) Adds a INNER JOIN clause to the query using the Seguimiento relation
 *
 * @method IncidenciaQuery leftJoinTelefonoIncidencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the TelefonoIncidencia relation
 * @method IncidenciaQuery rightJoinTelefonoIncidencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TelefonoIncidencia relation
 * @method IncidenciaQuery innerJoinTelefonoIncidencia($relationAlias = null) Adds a INNER JOIN clause to the query using the TelefonoIncidencia relation
 *
 * @method Incidencia findOne(PropelPDO $con = null) Return the first Incidencia matching the query
 * @method Incidencia findOneOrCreate(PropelPDO $con = null) Return the first Incidencia matching the query, or a new Incidencia object populated from the query conditions when no match is found
 *
 * @method Incidencia findOneByOrigen(string $origen) Return the first Incidencia filtered by the origen column
 * @method Incidencia findOneByNombrePersona(string $nombre_persona) Return the first Incidencia filtered by the nombre_persona column
 * @method Incidencia findOneByBreve(string $breve) Return the first Incidencia filtered by the breve column
 * @method Incidencia findOneByExpediente(string $expediente) Return the first Incidencia filtered by the expediente column
 * @method Incidencia findOneByDescripcion(string $descripcion) Return the first Incidencia filtered by the descripcion column
 * @method Incidencia findOneByAtencion(string $atencion) Return the first Incidencia filtered by the atencion column
 * @method Incidencia findOneByPrioridad(int $prioridad) Return the first Incidencia filtered by the prioridad column
 * @method Incidencia findOneByResuelta(boolean $resuelta) Return the first Incidencia filtered by the resuelta column
 * @method Incidencia findOneByMarcada(boolean $marcada) Return the first Incidencia filtered by the marcada column
 * @method Incidencia findOneByEliminado(boolean $eliminado) Return the first Incidencia filtered by the eliminado column
 * @method Incidencia findOneByFechaCreacion(string $fecha_creacion) Return the first Incidencia filtered by the fecha_creacion column
 * @method Incidencia findOneByFechaModificacion(string $fecha_modificacion) Return the first Incidencia filtered by the fecha_modificacion column
 * @method Incidencia findOneByFechaIncidencia(string $fecha_incidencia) Return the first Incidencia filtered by the fecha_incidencia column
 * @method Incidencia findOneByFechaResolucion(string $fecha_resolucion) Return the first Incidencia filtered by the fecha_resolucion column
 * @method Incidencia findOneByComunidadId(int $comunidad_id) Return the first Incidencia filtered by the comunidad_id column
 * @method Incidencia findOneByPersonaId(int $persona_id) Return the first Incidencia filtered by the persona_id column
 *
 * @method array findById(int $id) Return Incidencia objects filtered by the id column
 * @method array findByOrigen(string $origen) Return Incidencia objects filtered by the origen column
 * @method array findByNombrePersona(string $nombre_persona) Return Incidencia objects filtered by the nombre_persona column
 * @method array findByBreve(string $breve) Return Incidencia objects filtered by the breve column
 * @method array findByExpediente(string $expediente) Return Incidencia objects filtered by the expediente column
 * @method array findByDescripcion(string $descripcion) Return Incidencia objects filtered by the descripcion column
 * @method array findByAtencion(string $atencion) Return Incidencia objects filtered by the atencion column
 * @method array findByPrioridad(int $prioridad) Return Incidencia objects filtered by the prioridad column
 * @method array findByResuelta(boolean $resuelta) Return Incidencia objects filtered by the resuelta column
 * @method array findByMarcada(boolean $marcada) Return Incidencia objects filtered by the marcada column
 * @method array findByEliminado(boolean $eliminado) Return Incidencia objects filtered by the eliminado column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Incidencia objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Incidencia objects filtered by the fecha_modificacion column
 * @method array findByFechaIncidencia(string $fecha_incidencia) Return Incidencia objects filtered by the fecha_incidencia column
 * @method array findByFechaResolucion(string $fecha_resolucion) Return Incidencia objects filtered by the fecha_resolucion column
 * @method array findByComunidadId(int $comunidad_id) Return Incidencia objects filtered by the comunidad_id column
 * @method array findByPersonaId(int $persona_id) Return Incidencia objects filtered by the persona_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseIncidenciaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseIncidenciaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Incidencia';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new IncidenciaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   IncidenciaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return IncidenciaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof IncidenciaQuery) {
            return $criteria;
        }
        $query = new IncidenciaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Incidencia|Incidencia[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = IncidenciaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(IncidenciaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Incidencia A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Incidencia A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `origen`, `nombre_persona`, `breve`, `expediente`, `descripcion`, `atencion`, `prioridad`, `resuelta`, `marcada`, `eliminado`, `fecha_creacion`, `fecha_modificacion`, `fecha_incidencia`, `fecha_resolucion`, `comunidad_id`, `persona_id` FROM `incidencia` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Incidencia();
            $obj->hydrate($row);
            IncidenciaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Incidencia|Incidencia[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Incidencia[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IncidenciaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IncidenciaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(IncidenciaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(IncidenciaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the origen column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigen('fooValue');   // WHERE origen = 'fooValue'
     * $query->filterByOrigen('%fooValue%'); // WHERE origen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $origen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByOrigen($origen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($origen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $origen)) {
                $origen = str_replace('*', '%', $origen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::ORIGEN, $origen, $comparison);
    }

    /**
     * Filter the query on the nombre_persona column
     *
     * Example usage:
     * <code>
     * $query->filterByNombrePersona('fooValue');   // WHERE nombre_persona = 'fooValue'
     * $query->filterByNombrePersona('%fooValue%'); // WHERE nombre_persona LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombrePersona The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByNombrePersona($nombrePersona = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombrePersona)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombrePersona)) {
                $nombrePersona = str_replace('*', '%', $nombrePersona);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::NOMBRE_PERSONA, $nombrePersona, $comparison);
    }

    /**
     * Filter the query on the breve column
     *
     * Example usage:
     * <code>
     * $query->filterByBreve('fooValue');   // WHERE breve = 'fooValue'
     * $query->filterByBreve('%fooValue%'); // WHERE breve LIKE '%fooValue%'
     * </code>
     *
     * @param     string $breve The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByBreve($breve = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($breve)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $breve)) {
                $breve = str_replace('*', '%', $breve);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::BREVE, $breve, $comparison);
    }

    /**
     * Filter the query on the expediente column
     *
     * Example usage:
     * <code>
     * $query->filterByExpediente('fooValue');   // WHERE expediente = 'fooValue'
     * $query->filterByExpediente('%fooValue%'); // WHERE expediente LIKE '%fooValue%'
     * </code>
     *
     * @param     string $expediente The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByExpediente($expediente = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($expediente)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $expediente)) {
                $expediente = str_replace('*', '%', $expediente);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::EXPEDIENTE, $expediente, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the atencion column
     *
     * Example usage:
     * <code>
     * $query->filterByAtencion('fooValue');   // WHERE atencion = 'fooValue'
     * $query->filterByAtencion('%fooValue%'); // WHERE atencion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $atencion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByAtencion($atencion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($atencion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $atencion)) {
                $atencion = str_replace('*', '%', $atencion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::ATENCION, $atencion, $comparison);
    }

    /**
     * Filter the query on the prioridad column
     *
     * Example usage:
     * <code>
     * $query->filterByPrioridad(1234); // WHERE prioridad = 1234
     * $query->filterByPrioridad(array(12, 34)); // WHERE prioridad IN (12, 34)
     * $query->filterByPrioridad(array('min' => 12)); // WHERE prioridad >= 12
     * $query->filterByPrioridad(array('max' => 12)); // WHERE prioridad <= 12
     * </code>
     *
     * @param     mixed $prioridad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByPrioridad($prioridad = null, $comparison = null)
    {
        if (is_array($prioridad)) {
            $useMinMax = false;
            if (isset($prioridad['min'])) {
                $this->addUsingAlias(IncidenciaPeer::PRIORIDAD, $prioridad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prioridad['max'])) {
                $this->addUsingAlias(IncidenciaPeer::PRIORIDAD, $prioridad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::PRIORIDAD, $prioridad, $comparison);
    }

    /**
     * Filter the query on the resuelta column
     *
     * Example usage:
     * <code>
     * $query->filterByResuelta(true); // WHERE resuelta = true
     * $query->filterByResuelta('yes'); // WHERE resuelta = true
     * </code>
     *
     * @param     boolean|string $resuelta The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByResuelta($resuelta = null, $comparison = null)
    {
        if (is_string($resuelta)) {
            $resuelta = in_array(strtolower($resuelta), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(IncidenciaPeer::RESUELTA, $resuelta, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(IncidenciaPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the eliminado column
     *
     * Example usage:
     * <code>
     * $query->filterByEliminado(true); // WHERE eliminado = true
     * $query->filterByEliminado('yes'); // WHERE eliminado = true
     * </code>
     *
     * @param     boolean|string $eliminado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByEliminado($eliminado = null, $comparison = null)
    {
        if (is_string($eliminado)) {
            $eliminado = in_array(strtolower($eliminado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(IncidenciaPeer::ELIMINADO, $eliminado, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the fecha_incidencia column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaIncidencia('2011-03-14'); // WHERE fecha_incidencia = '2011-03-14'
     * $query->filterByFechaIncidencia('now'); // WHERE fecha_incidencia = '2011-03-14'
     * $query->filterByFechaIncidencia(array('max' => 'yesterday')); // WHERE fecha_incidencia < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaIncidencia The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByFechaIncidencia($fechaIncidencia = null, $comparison = null)
    {
        if (is_array($fechaIncidencia)) {
            $useMinMax = false;
            if (isset($fechaIncidencia['min'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_INCIDENCIA, $fechaIncidencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaIncidencia['max'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_INCIDENCIA, $fechaIncidencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::FECHA_INCIDENCIA, $fechaIncidencia, $comparison);
    }

    /**
     * Filter the query on the fecha_resolucion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaResolucion('2011-03-14'); // WHERE fecha_resolucion = '2011-03-14'
     * $query->filterByFechaResolucion('now'); // WHERE fecha_resolucion = '2011-03-14'
     * $query->filterByFechaResolucion(array('max' => 'yesterday')); // WHERE fecha_resolucion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaResolucion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByFechaResolucion($fechaResolucion = null, $comparison = null)
    {
        if (is_array($fechaResolucion)) {
            $useMinMax = false;
            if (isset($fechaResolucion['min'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_RESOLUCION, $fechaResolucion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaResolucion['max'])) {
                $this->addUsingAlias(IncidenciaPeer::FECHA_RESOLUCION, $fechaResolucion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::FECHA_RESOLUCION, $fechaResolucion, $comparison);
    }

    /**
     * Filter the query on the comunidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidadId(1234); // WHERE comunidad_id = 1234
     * $query->filterByComunidadId(array(12, 34)); // WHERE comunidad_id IN (12, 34)
     * $query->filterByComunidadId(array('min' => 12)); // WHERE comunidad_id >= 12
     * $query->filterByComunidadId(array('max' => 12)); // WHERE comunidad_id <= 12
     * </code>
     *
     * @see       filterByComunidad()
     *
     * @param     mixed $comunidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByComunidadId($comunidadId = null, $comparison = null)
    {
        if (is_array($comunidadId)) {
            $useMinMax = false;
            if (isset($comunidadId['min'])) {
                $this->addUsingAlias(IncidenciaPeer::COMUNIDAD_ID, $comunidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comunidadId['max'])) {
                $this->addUsingAlias(IncidenciaPeer::COMUNIDAD_ID, $comunidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::COMUNIDAD_ID, $comunidadId, $comparison);
    }

    /**
     * Filter the query on the persona_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonaId(1234); // WHERE persona_id = 1234
     * $query->filterByPersonaId(array(12, 34)); // WHERE persona_id IN (12, 34)
     * $query->filterByPersonaId(array('min' => 12)); // WHERE persona_id >= 12
     * $query->filterByPersonaId(array('max' => 12)); // WHERE persona_id <= 12
     * </code>
     *
     * @see       filterByPersona()
     *
     * @param     mixed $personaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function filterByPersonaId($personaId = null, $comparison = null)
    {
        if (is_array($personaId)) {
            $useMinMax = false;
            if (isset($personaId['min'])) {
                $this->addUsingAlias(IncidenciaPeer::PERSONA_ID, $personaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personaId['max'])) {
                $this->addUsingAlias(IncidenciaPeer::PERSONA_ID, $personaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IncidenciaPeer::PERSONA_ID, $personaId, $comparison);
    }

    /**
     * Filter the query by a related Comunidad object
     *
     * @param   Comunidad|PropelObjectCollection $comunidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComunidad($comunidad, $comparison = null)
    {
        if ($comunidad instanceof Comunidad) {
            return $this
                ->addUsingAlias(IncidenciaPeer::COMUNIDAD_ID, $comunidad->getId(), $comparison);
        } elseif ($comunidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IncidenciaPeer::COMUNIDAD_ID, $comunidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComunidad() only accepts arguments of type Comunidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comunidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinComunidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comunidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comunidad');
        }

        return $this;
    }

    /**
     * Use the Comunidad relation Comunidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComunidadQuery A secondary query class using the current class as primary query
     */
    public function useComunidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComunidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comunidad', 'ComunidadQuery');
    }

    /**
     * Filter the query by a related Persona object
     *
     * @param   Persona|PropelObjectCollection $persona The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPersona($persona, $comparison = null)
    {
        if ($persona instanceof Persona) {
            return $this
                ->addUsingAlias(IncidenciaPeer::PERSONA_ID, $persona->getId(), $comparison);
        } elseif ($persona instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IncidenciaPeer::PERSONA_ID, $persona->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPersona() only accepts arguments of type Persona or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persona relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinPersona($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persona');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persona');
        }

        return $this;
    }

    /**
     * Use the Persona relation Persona object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PersonaQuery A secondary query class using the current class as primary query
     */
    public function usePersonaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPersona($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persona', 'PersonaQuery');
    }

    /**
     * Filter the query by a related Aviso object
     *
     * @param   Aviso|PropelObjectCollection $aviso  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAvisoInc($aviso, $comparison = null)
    {
        if ($aviso instanceof Aviso) {
            return $this
                ->addUsingAlias(IncidenciaPeer::ID, $aviso->getIncidenciaId(), $comparison);
        } elseif ($aviso instanceof PropelObjectCollection) {
            return $this
                ->useAvisoIncQuery()
                ->filterByPrimaryKeys($aviso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvisoInc() only accepts arguments of type Aviso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvisoInc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinAvisoInc($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvisoInc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvisoInc');
        }

        return $this;
    }

    /**
     * Use the AvisoInc relation Aviso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AvisoQuery A secondary query class using the current class as primary query
     */
    public function useAvisoIncQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvisoInc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvisoInc', 'AvisoQuery');
    }

    /**
     * Filter the query by a related Tarea object
     *
     * @param   Tarea|PropelObjectCollection $tarea  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTarea($tarea, $comparison = null)
    {
        if ($tarea instanceof Tarea) {
            return $this
                ->addUsingAlias(IncidenciaPeer::ID, $tarea->getIncidenciaId(), $comparison);
        } elseif ($tarea instanceof PropelObjectCollection) {
            return $this
                ->useTareaQuery()
                ->filterByPrimaryKeys($tarea->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTarea() only accepts arguments of type Tarea or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tarea relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinTarea($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tarea');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tarea');
        }

        return $this;
    }

    /**
     * Use the Tarea relation Tarea object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TareaQuery A secondary query class using the current class as primary query
     */
    public function useTareaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTarea($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tarea', 'TareaQuery');
    }

    /**
     * Filter the query by a related Seguimiento object
     *
     * @param   Seguimiento|PropelObjectCollection $seguimiento  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySeguimiento($seguimiento, $comparison = null)
    {
        if ($seguimiento instanceof Seguimiento) {
            return $this
                ->addUsingAlias(IncidenciaPeer::ID, $seguimiento->getIncidenciaId(), $comparison);
        } elseif ($seguimiento instanceof PropelObjectCollection) {
            return $this
                ->useSeguimientoQuery()
                ->filterByPrimaryKeys($seguimiento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySeguimiento() only accepts arguments of type Seguimiento or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Seguimiento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinSeguimiento($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Seguimiento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Seguimiento');
        }

        return $this;
    }

    /**
     * Use the Seguimiento relation Seguimiento object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SeguimientoQuery A secondary query class using the current class as primary query
     */
    public function useSeguimientoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSeguimiento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Seguimiento', 'SeguimientoQuery');
    }

    /**
     * Filter the query by a related Telefono object
     *
     * @param   Telefono|PropelObjectCollection $telefono  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 IncidenciaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTelefonoIncidencia($telefono, $comparison = null)
    {
        if ($telefono instanceof Telefono) {
            return $this
                ->addUsingAlias(IncidenciaPeer::ID, $telefono->getIncidenciaId(), $comparison);
        } elseif ($telefono instanceof PropelObjectCollection) {
            return $this
                ->useTelefonoIncidenciaQuery()
                ->filterByPrimaryKeys($telefono->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTelefonoIncidencia() only accepts arguments of type Telefono or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TelefonoIncidencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function joinTelefonoIncidencia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TelefonoIncidencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TelefonoIncidencia');
        }

        return $this;
    }

    /**
     * Use the TelefonoIncidencia relation Telefono object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TelefonoQuery A secondary query class using the current class as primary query
     */
    public function useTelefonoIncidenciaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTelefonoIncidencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TelefonoIncidencia', 'TelefonoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Incidencia $incidencia Object to remove from the list of results
     *
     * @return IncidenciaQuery The current query, for fluid interface
     */
    public function prune($incidencia = null)
    {
        if ($incidencia) {
            $this->addUsingAlias(IncidenciaPeer::ID, $incidencia->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
