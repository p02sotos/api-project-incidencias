<?php


/**
 * Base class that represents a row from the 'telefono' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseTelefono extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'TelefonoPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        TelefonoPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the telefono field.
     * @var        string
     */
    protected $telefono;

    /**
     * The value for the tipo field.
     * Note: this column has a database default value of: 'CASA'
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the nota field.
     * @var        string
     */
    protected $nota;

    /**
     * The value for the marcada field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the persona_id field.
     * @var        int
     */
    protected $persona_id;

    /**
     * The value for the incidencia_id field.
     * @var        int
     */
    protected $incidencia_id;

    /**
     * The value for the empresa_id field.
     * @var        int
     */
    protected $empresa_id;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * @var        Persona
     */
    protected $aPersona;

    /**
     * @var        Incidencia
     */
    protected $aIncidencia;

    /**
     * @var        Empresa
     */
    protected $aEmpresa;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->tipo = 'CASA';
        $this->marcada = false;
    }

    /**
     * Initializes internal state of BaseTelefono object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [telefono] column value.
     * 
     * @return string
     */
    public function getTelefono()
    {

        return $this->telefono;
    }

    /**
     * Get the [tipo] column value.
     * 
     * @return string
     */
    public function getTipo()
    {

        return $this->tipo;
    }

    /**
     * Get the [nota] column value.
     * 
     * @return string
     */
    public function getNota()
    {

        return $this->nota;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [persona_id] column value.
     * 
     * @return int
     */
    public function getPersonaId()
    {

        return $this->persona_id;
    }

    /**
     * Get the [incidencia_id] column value.
     * 
     * @return int
     */
    public function getIncidenciaId()
    {

        return $this->incidencia_id;
    }

    /**
     * Get the [empresa_id] column value.
     * 
     * @return int
     */
    public function getEmpresaId()
    {

        return $this->empresa_id;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = TelefonoPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [telefono] column.
     * 
     * @param  string $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setTelefono($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefono !== $v) {
            $this->telefono = $v;
            $this->modifiedColumns[] = TelefonoPeer::TELEFONO;
        }


        return $this;
    } // setTelefono()

    /**
     * Set the value of [tipo] column.
     * 
     * @param  string $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[] = TelefonoPeer::TIPO;
        }


        return $this;
    } // setTipo()

    /**
     * Set the value of [nota] column.
     * 
     * @param  string $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setNota($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nota !== $v) {
            $this->nota = $v;
            $this->modifiedColumns[] = TelefonoPeer::NOTA;
        }


        return $this;
    } // setNota()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = TelefonoPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Set the value of [persona_id] column.
     * 
     * @param  int $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setPersonaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->persona_id !== $v) {
            $this->persona_id = $v;
            $this->modifiedColumns[] = TelefonoPeer::PERSONA_ID;
        }

        if ($this->aPersona !== null && $this->aPersona->getId() !== $v) {
            $this->aPersona = null;
        }


        return $this;
    } // setPersonaId()

    /**
     * Set the value of [incidencia_id] column.
     * 
     * @param  int $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setIncidenciaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->incidencia_id !== $v) {
            $this->incidencia_id = $v;
            $this->modifiedColumns[] = TelefonoPeer::INCIDENCIA_ID;
        }

        if ($this->aIncidencia !== null && $this->aIncidencia->getId() !== $v) {
            $this->aIncidencia = null;
        }


        return $this;
    } // setIncidenciaId()

    /**
     * Set the value of [empresa_id] column.
     * 
     * @param  int $v new value
     * @return Telefono The current object (for fluent API support)
     */
    public function setEmpresaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->empresa_id !== $v) {
            $this->empresa_id = $v;
            $this->modifiedColumns[] = TelefonoPeer::EMPRESA_ID;
        }

        if ($this->aEmpresa !== null && $this->aEmpresa->getId() !== $v) {
            $this->aEmpresa = null;
        }


        return $this;
    } // setEmpresaId()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Telefono The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = TelefonoPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Telefono The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = TelefonoPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->tipo !== 'CASA') {
                return false;
            }

            if ($this->marcada !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->telefono = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->tipo = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nota = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->marcada = ($row[$startcol + 4] !== null) ? (boolean) $row[$startcol + 4] : null;
            $this->persona_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->incidencia_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->empresa_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->fecha_creacion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->fecha_modificacion = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = TelefonoPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Telefono object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aPersona !== null && $this->persona_id !== $this->aPersona->getId()) {
            $this->aPersona = null;
        }
        if ($this->aIncidencia !== null && $this->incidencia_id !== $this->aIncidencia->getId()) {
            $this->aIncidencia = null;
        }
        if ($this->aEmpresa !== null && $this->empresa_id !== $this->aEmpresa->getId()) {
            $this->aEmpresa = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TelefonoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = TelefonoPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPersona = null;
            $this->aIncidencia = null;
            $this->aEmpresa = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TelefonoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = TelefonoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(TelefonoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TelefonoPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPersona !== null) {
                if ($this->aPersona->isModified() || $this->aPersona->isNew()) {
                    $affectedRows += $this->aPersona->save($con);
                }
                $this->setPersona($this->aPersona);
            }

            if ($this->aIncidencia !== null) {
                if ($this->aIncidencia->isModified() || $this->aIncidencia->isNew()) {
                    $affectedRows += $this->aIncidencia->save($con);
                }
                $this->setIncidencia($this->aIncidencia);
            }

            if ($this->aEmpresa !== null) {
                if ($this->aEmpresa->isModified() || $this->aEmpresa->isNew()) {
                    $affectedRows += $this->aEmpresa->save($con);
                }
                $this->setEmpresa($this->aEmpresa);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = TelefonoPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TelefonoPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TelefonoPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(TelefonoPeer::TELEFONO)) {
            $modifiedColumns[':p' . $index++]  = '`telefono`';
        }
        if ($this->isColumnModified(TelefonoPeer::TIPO)) {
            $modifiedColumns[':p' . $index++]  = '`tipo`';
        }
        if ($this->isColumnModified(TelefonoPeer::NOTA)) {
            $modifiedColumns[':p' . $index++]  = '`nota`';
        }
        if ($this->isColumnModified(TelefonoPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(TelefonoPeer::PERSONA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`persona_id`';
        }
        if ($this->isColumnModified(TelefonoPeer::INCIDENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`incidencia_id`';
        }
        if ($this->isColumnModified(TelefonoPeer::EMPRESA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`empresa_id`';
        }
        if ($this->isColumnModified(TelefonoPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(TelefonoPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }

        $sql = sprintf(
            'INSERT INTO `telefono` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`telefono`':						
                        $stmt->bindValue($identifier, $this->telefono, PDO::PARAM_STR);
                        break;
                    case '`tipo`':						
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case '`nota`':						
                        $stmt->bindValue($identifier, $this->nota, PDO::PARAM_STR);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`persona_id`':						
                        $stmt->bindValue($identifier, $this->persona_id, PDO::PARAM_INT);
                        break;
                    case '`incidencia_id`':						
                        $stmt->bindValue($identifier, $this->incidencia_id, PDO::PARAM_INT);
                        break;
                    case '`empresa_id`':						
                        $stmt->bindValue($identifier, $this->empresa_id, PDO::PARAM_INT);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPersona !== null) {
                if (!$this->aPersona->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPersona->getValidationFailures());
                }
            }

            if ($this->aIncidencia !== null) {
                if (!$this->aIncidencia->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aIncidencia->getValidationFailures());
                }
            }

            if ($this->aEmpresa !== null) {
                if (!$this->aEmpresa->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEmpresa->getValidationFailures());
                }
            }


            if (($retval = TelefonoPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TelefonoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTelefono();
                break;
            case 2:
                return $this->getTipo();
                break;
            case 3:
                return $this->getNota();
                break;
            case 4:
                return $this->getMarcada();
                break;
            case 5:
                return $this->getPersonaId();
                break;
            case 6:
                return $this->getIncidenciaId();
                break;
            case 7:
                return $this->getEmpresaId();
                break;
            case 8:
                return $this->getFechaCreacion();
                break;
            case 9:
                return $this->getFechaModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Telefono'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Telefono'][$this->getPrimaryKey()] = true;
        $keys = TelefonoPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTelefono(),
            $keys[2] => $this->getTipo(),
            $keys[3] => $this->getNota(),
            $keys[4] => $this->getMarcada(),
            $keys[5] => $this->getPersonaId(),
            $keys[6] => $this->getIncidenciaId(),
            $keys[7] => $this->getEmpresaId(),
            $keys[8] => $this->getFechaCreacion(),
            $keys[9] => $this->getFechaModificacion(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aPersona) {
                $result['Persona'] = $this->aPersona->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aIncidencia) {
                $result['Incidencia'] = $this->aIncidencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEmpresa) {
                $result['Empresa'] = $this->aEmpresa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = TelefonoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTelefono($value);
                break;
            case 2:
                $this->setTipo($value);
                break;
            case 3:
                $this->setNota($value);
                break;
            case 4:
                $this->setMarcada($value);
                break;
            case 5:
                $this->setPersonaId($value);
                break;
            case 6:
                $this->setIncidenciaId($value);
                break;
            case 7:
                $this->setEmpresaId($value);
                break;
            case 8:
                $this->setFechaCreacion($value);
                break;
            case 9:
                $this->setFechaModificacion($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = TelefonoPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTelefono($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTipo($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNota($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMarcada($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPersonaId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIncidenciaId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEmpresaId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFechaCreacion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setFechaModificacion($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TelefonoPeer::DATABASE_NAME);

        if ($this->isColumnModified(TelefonoPeer::ID)) $criteria->add(TelefonoPeer::ID, $this->id);
        if ($this->isColumnModified(TelefonoPeer::TELEFONO)) $criteria->add(TelefonoPeer::TELEFONO, $this->telefono);
        if ($this->isColumnModified(TelefonoPeer::TIPO)) $criteria->add(TelefonoPeer::TIPO, $this->tipo);
        if ($this->isColumnModified(TelefonoPeer::NOTA)) $criteria->add(TelefonoPeer::NOTA, $this->nota);
        if ($this->isColumnModified(TelefonoPeer::MARCADA)) $criteria->add(TelefonoPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(TelefonoPeer::PERSONA_ID)) $criteria->add(TelefonoPeer::PERSONA_ID, $this->persona_id);
        if ($this->isColumnModified(TelefonoPeer::INCIDENCIA_ID)) $criteria->add(TelefonoPeer::INCIDENCIA_ID, $this->incidencia_id);
        if ($this->isColumnModified(TelefonoPeer::EMPRESA_ID)) $criteria->add(TelefonoPeer::EMPRESA_ID, $this->empresa_id);
        if ($this->isColumnModified(TelefonoPeer::FECHA_CREACION)) $criteria->add(TelefonoPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(TelefonoPeer::FECHA_MODIFICACION)) $criteria->add(TelefonoPeer::FECHA_MODIFICACION, $this->fecha_modificacion);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(TelefonoPeer::DATABASE_NAME);
        $criteria->add(TelefonoPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Telefono (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTelefono($this->getTelefono());
        $copyObj->setTipo($this->getTipo());
        $copyObj->setNota($this->getNota());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setPersonaId($this->getPersonaId());
        $copyObj->setIncidenciaId($this->getIncidenciaId());
        $copyObj->setEmpresaId($this->getEmpresaId());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Telefono Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return TelefonoPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new TelefonoPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Persona object.
     *
     * @param                  Persona $v
     * @return Telefono The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPersona(Persona $v = null)
    {
        if ($v === null) {
            $this->setPersonaId(NULL);
        } else {
            $this->setPersonaId($v->getId());
        }

        $this->aPersona = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Persona object, it will not be re-added.
        if ($v !== null) {
            $v->addTelefonoPersona($this);
        }


        return $this;
    }


    /**
     * Get the associated Persona object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Persona The associated Persona object.
     * @throws PropelException
     */
    public function getPersona(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPersona === null && ($this->persona_id !== null) && $doQuery) {
            $this->aPersona = PersonaQuery::create()->findPk($this->persona_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPersona->addTelefonoPersonas($this);
             */
        }

        return $this->aPersona;
    }

    /**
     * Declares an association between this object and a Incidencia object.
     *
     * @param                  Incidencia $v
     * @return Telefono The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIncidencia(Incidencia $v = null)
    {
        if ($v === null) {
            $this->setIncidenciaId(NULL);
        } else {
            $this->setIncidenciaId($v->getId());
        }

        $this->aIncidencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Incidencia object, it will not be re-added.
        if ($v !== null) {
            $v->addTelefonoIncidencia($this);
        }


        return $this;
    }


    /**
     * Get the associated Incidencia object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Incidencia The associated Incidencia object.
     * @throws PropelException
     */
    public function getIncidencia(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aIncidencia === null && ($this->incidencia_id !== null) && $doQuery) {
            $this->aIncidencia = IncidenciaQuery::create()->findPk($this->incidencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIncidencia->addTelefonoIncidencias($this);
             */
        }

        return $this->aIncidencia;
    }

    /**
     * Declares an association between this object and a Empresa object.
     *
     * @param                  Empresa $v
     * @return Telefono The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEmpresa(Empresa $v = null)
    {
        if ($v === null) {
            $this->setEmpresaId(NULL);
        } else {
            $this->setEmpresaId($v->getId());
        }

        $this->aEmpresa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Empresa object, it will not be re-added.
        if ($v !== null) {
            $v->addTelefonoEmpresa($this);
        }


        return $this;
    }


    /**
     * Get the associated Empresa object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Empresa The associated Empresa object.
     * @throws PropelException
     */
    public function getEmpresa(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEmpresa === null && ($this->empresa_id !== null) && $doQuery) {
            $this->aEmpresa = EmpresaQuery::create()->findPk($this->empresa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEmpresa->addTelefonoEmpresas($this);
             */
        }

        return $this->aEmpresa;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->telefono = null;
        $this->tipo = null;
        $this->nota = null;
        $this->marcada = null;
        $this->persona_id = null;
        $this->incidencia_id = null;
        $this->empresa_id = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aPersona instanceof Persistent) {
              $this->aPersona->clearAllReferences($deep);
            }
            if ($this->aIncidencia instanceof Persistent) {
              $this->aIncidencia->clearAllReferences($deep);
            }
            if ($this->aEmpresa instanceof Persistent) {
              $this->aEmpresa->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aPersona = null;
        $this->aIncidencia = null;
        $this->aEmpresa = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TelefonoPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
