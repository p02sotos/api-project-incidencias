<?php


/**
 * Base class that represents a query for the 'aviso' table.
 *
 * 
 *
 * @method AvisoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method AvisoQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method AvisoQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method AvisoQuery orderByPrioridad($order = Criteria::ASC) Order by the prioridad column
 * @method AvisoQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method AvisoQuery orderByEliminado($order = Criteria::ASC) Order by the eliminado column
 * @method AvisoQuery orderByFechaAviso($order = Criteria::ASC) Order by the fecha_aviso column
 * @method AvisoQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method AvisoQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method AvisoQuery orderByIncidenciaId($order = Criteria::ASC) Order by the incidencia_id column
 * @method AvisoQuery orderByComunidadId($order = Criteria::ASC) Order by the comunidad_id column
 * @method AvisoQuery orderByPersonaId($order = Criteria::ASC) Order by the persona_id column
 * @method AvisoQuery orderByEntregaId($order = Criteria::ASC) Order by the entrega_id column
 * @method AvisoQuery orderByContabilidadId($order = Criteria::ASC) Order by the contabilidad_id column
 * @method AvisoQuery orderByTareaId($order = Criteria::ASC) Order by the tarea_id column
 *
 * @method AvisoQuery groupById() Group by the id column
 * @method AvisoQuery groupByNombre() Group by the nombre column
 * @method AvisoQuery groupByDescripcion() Group by the descripcion column
 * @method AvisoQuery groupByPrioridad() Group by the prioridad column
 * @method AvisoQuery groupByMarcada() Group by the marcada column
 * @method AvisoQuery groupByEliminado() Group by the eliminado column
 * @method AvisoQuery groupByFechaAviso() Group by the fecha_aviso column
 * @method AvisoQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method AvisoQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method AvisoQuery groupByIncidenciaId() Group by the incidencia_id column
 * @method AvisoQuery groupByComunidadId() Group by the comunidad_id column
 * @method AvisoQuery groupByPersonaId() Group by the persona_id column
 * @method AvisoQuery groupByEntregaId() Group by the entrega_id column
 * @method AvisoQuery groupByContabilidadId() Group by the contabilidad_id column
 * @method AvisoQuery groupByTareaId() Group by the tarea_id column
 *
 * @method AvisoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method AvisoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method AvisoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method AvisoQuery leftJoinIncidencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Incidencia relation
 * @method AvisoQuery rightJoinIncidencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Incidencia relation
 * @method AvisoQuery innerJoinIncidencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Incidencia relation
 *
 * @method AvisoQuery leftJoinTarea($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tarea relation
 * @method AvisoQuery rightJoinTarea($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tarea relation
 * @method AvisoQuery innerJoinTarea($relationAlias = null) Adds a INNER JOIN clause to the query using the Tarea relation
 *
 * @method AvisoQuery leftJoinComunidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comunidad relation
 * @method AvisoQuery rightJoinComunidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comunidad relation
 * @method AvisoQuery innerJoinComunidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Comunidad relation
 *
 * @method AvisoQuery leftJoinPersona($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persona relation
 * @method AvisoQuery rightJoinPersona($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persona relation
 * @method AvisoQuery innerJoinPersona($relationAlias = null) Adds a INNER JOIN clause to the query using the Persona relation
 *
 * @method AvisoQuery leftJoinEntrega($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entrega relation
 * @method AvisoQuery rightJoinEntrega($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entrega relation
 * @method AvisoQuery innerJoinEntrega($relationAlias = null) Adds a INNER JOIN clause to the query using the Entrega relation
 *
 * @method AvisoQuery leftJoinContabilidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contabilidad relation
 * @method AvisoQuery rightJoinContabilidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contabilidad relation
 * @method AvisoQuery innerJoinContabilidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Contabilidad relation
 *
 * @method Aviso findOne(PropelPDO $con = null) Return the first Aviso matching the query
 * @method Aviso findOneOrCreate(PropelPDO $con = null) Return the first Aviso matching the query, or a new Aviso object populated from the query conditions when no match is found
 *
 * @method Aviso findOneByNombre(string $nombre) Return the first Aviso filtered by the nombre column
 * @method Aviso findOneByDescripcion(string $descripcion) Return the first Aviso filtered by the descripcion column
 * @method Aviso findOneByPrioridad(int $prioridad) Return the first Aviso filtered by the prioridad column
 * @method Aviso findOneByMarcada(boolean $marcada) Return the first Aviso filtered by the marcada column
 * @method Aviso findOneByEliminado(boolean $eliminado) Return the first Aviso filtered by the eliminado column
 * @method Aviso findOneByFechaAviso(string $fecha_aviso) Return the first Aviso filtered by the fecha_aviso column
 * @method Aviso findOneByFechaCreacion(string $fecha_creacion) Return the first Aviso filtered by the fecha_creacion column
 * @method Aviso findOneByFechaModificacion(string $fecha_modificacion) Return the first Aviso filtered by the fecha_modificacion column
 * @method Aviso findOneByIncidenciaId(int $incidencia_id) Return the first Aviso filtered by the incidencia_id column
 * @method Aviso findOneByComunidadId(int $comunidad_id) Return the first Aviso filtered by the comunidad_id column
 * @method Aviso findOneByPersonaId(int $persona_id) Return the first Aviso filtered by the persona_id column
 * @method Aviso findOneByEntregaId(int $entrega_id) Return the first Aviso filtered by the entrega_id column
 * @method Aviso findOneByContabilidadId(int $contabilidad_id) Return the first Aviso filtered by the contabilidad_id column
 * @method Aviso findOneByTareaId(int $tarea_id) Return the first Aviso filtered by the tarea_id column
 *
 * @method array findById(int $id) Return Aviso objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Aviso objects filtered by the nombre column
 * @method array findByDescripcion(string $descripcion) Return Aviso objects filtered by the descripcion column
 * @method array findByPrioridad(int $prioridad) Return Aviso objects filtered by the prioridad column
 * @method array findByMarcada(boolean $marcada) Return Aviso objects filtered by the marcada column
 * @method array findByEliminado(boolean $eliminado) Return Aviso objects filtered by the eliminado column
 * @method array findByFechaAviso(string $fecha_aviso) Return Aviso objects filtered by the fecha_aviso column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Aviso objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Aviso objects filtered by the fecha_modificacion column
 * @method array findByIncidenciaId(int $incidencia_id) Return Aviso objects filtered by the incidencia_id column
 * @method array findByComunidadId(int $comunidad_id) Return Aviso objects filtered by the comunidad_id column
 * @method array findByPersonaId(int $persona_id) Return Aviso objects filtered by the persona_id column
 * @method array findByEntregaId(int $entrega_id) Return Aviso objects filtered by the entrega_id column
 * @method array findByContabilidadId(int $contabilidad_id) Return Aviso objects filtered by the contabilidad_id column
 * @method array findByTareaId(int $tarea_id) Return Aviso objects filtered by the tarea_id column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseAvisoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseAvisoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Aviso';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new AvisoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   AvisoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return AvisoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof AvisoQuery) {
            return $criteria;
        }
        $query = new AvisoQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Aviso|Aviso[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AvisoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(AvisoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Aviso A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Aviso A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `descripcion`, `prioridad`, `marcada`, `eliminado`, `fecha_aviso`, `fecha_creacion`, `fecha_modificacion`, `incidencia_id`, `comunidad_id`, `persona_id`, `entrega_id`, `contabilidad_id`, `tarea_id` FROM `aviso` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Aviso();
            $obj->hydrate($row);
            AvisoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Aviso|Aviso[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Aviso[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AvisoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AvisoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AvisoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AvisoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AvisoPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AvisoPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the prioridad column
     *
     * Example usage:
     * <code>
     * $query->filterByPrioridad(1234); // WHERE prioridad = 1234
     * $query->filterByPrioridad(array(12, 34)); // WHERE prioridad IN (12, 34)
     * $query->filterByPrioridad(array('min' => 12)); // WHERE prioridad >= 12
     * $query->filterByPrioridad(array('max' => 12)); // WHERE prioridad <= 12
     * </code>
     *
     * @param     mixed $prioridad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByPrioridad($prioridad = null, $comparison = null)
    {
        if (is_array($prioridad)) {
            $useMinMax = false;
            if (isset($prioridad['min'])) {
                $this->addUsingAlias(AvisoPeer::PRIORIDAD, $prioridad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($prioridad['max'])) {
                $this->addUsingAlias(AvisoPeer::PRIORIDAD, $prioridad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::PRIORIDAD, $prioridad, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AvisoPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the eliminado column
     *
     * Example usage:
     * <code>
     * $query->filterByEliminado(true); // WHERE eliminado = true
     * $query->filterByEliminado('yes'); // WHERE eliminado = true
     * </code>
     *
     * @param     boolean|string $eliminado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByEliminado($eliminado = null, $comparison = null)
    {
        if (is_string($eliminado)) {
            $eliminado = in_array(strtolower($eliminado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(AvisoPeer::ELIMINADO, $eliminado, $comparison);
    }

    /**
     * Filter the query on the fecha_aviso column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaAviso('2011-03-14'); // WHERE fecha_aviso = '2011-03-14'
     * $query->filterByFechaAviso('now'); // WHERE fecha_aviso = '2011-03-14'
     * $query->filterByFechaAviso(array('max' => 'yesterday')); // WHERE fecha_aviso < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaAviso The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByFechaAviso($fechaAviso = null, $comparison = null)
    {
        if (is_array($fechaAviso)) {
            $useMinMax = false;
            if (isset($fechaAviso['min'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_AVISO, $fechaAviso['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaAviso['max'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_AVISO, $fechaAviso['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::FECHA_AVISO, $fechaAviso, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(AvisoPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the incidencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIncidenciaId(1234); // WHERE incidencia_id = 1234
     * $query->filterByIncidenciaId(array(12, 34)); // WHERE incidencia_id IN (12, 34)
     * $query->filterByIncidenciaId(array('min' => 12)); // WHERE incidencia_id >= 12
     * $query->filterByIncidenciaId(array('max' => 12)); // WHERE incidencia_id <= 12
     * </code>
     *
     * @see       filterByIncidencia()
     *
     * @param     mixed $incidenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByIncidenciaId($incidenciaId = null, $comparison = null)
    {
        if (is_array($incidenciaId)) {
            $useMinMax = false;
            if (isset($incidenciaId['min'])) {
                $this->addUsingAlias(AvisoPeer::INCIDENCIA_ID, $incidenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($incidenciaId['max'])) {
                $this->addUsingAlias(AvisoPeer::INCIDENCIA_ID, $incidenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::INCIDENCIA_ID, $incidenciaId, $comparison);
    }

    /**
     * Filter the query on the comunidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComunidadId(1234); // WHERE comunidad_id = 1234
     * $query->filterByComunidadId(array(12, 34)); // WHERE comunidad_id IN (12, 34)
     * $query->filterByComunidadId(array('min' => 12)); // WHERE comunidad_id >= 12
     * $query->filterByComunidadId(array('max' => 12)); // WHERE comunidad_id <= 12
     * </code>
     *
     * @see       filterByComunidad()
     *
     * @param     mixed $comunidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByComunidadId($comunidadId = null, $comparison = null)
    {
        if (is_array($comunidadId)) {
            $useMinMax = false;
            if (isset($comunidadId['min'])) {
                $this->addUsingAlias(AvisoPeer::COMUNIDAD_ID, $comunidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comunidadId['max'])) {
                $this->addUsingAlias(AvisoPeer::COMUNIDAD_ID, $comunidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::COMUNIDAD_ID, $comunidadId, $comparison);
    }

    /**
     * Filter the query on the persona_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonaId(1234); // WHERE persona_id = 1234
     * $query->filterByPersonaId(array(12, 34)); // WHERE persona_id IN (12, 34)
     * $query->filterByPersonaId(array('min' => 12)); // WHERE persona_id >= 12
     * $query->filterByPersonaId(array('max' => 12)); // WHERE persona_id <= 12
     * </code>
     *
     * @see       filterByPersona()
     *
     * @param     mixed $personaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByPersonaId($personaId = null, $comparison = null)
    {
        if (is_array($personaId)) {
            $useMinMax = false;
            if (isset($personaId['min'])) {
                $this->addUsingAlias(AvisoPeer::PERSONA_ID, $personaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personaId['max'])) {
                $this->addUsingAlias(AvisoPeer::PERSONA_ID, $personaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::PERSONA_ID, $personaId, $comparison);
    }

    /**
     * Filter the query on the entrega_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEntregaId(1234); // WHERE entrega_id = 1234
     * $query->filterByEntregaId(array(12, 34)); // WHERE entrega_id IN (12, 34)
     * $query->filterByEntregaId(array('min' => 12)); // WHERE entrega_id >= 12
     * $query->filterByEntregaId(array('max' => 12)); // WHERE entrega_id <= 12
     * </code>
     *
     * @see       filterByEntrega()
     *
     * @param     mixed $entregaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByEntregaId($entregaId = null, $comparison = null)
    {
        if (is_array($entregaId)) {
            $useMinMax = false;
            if (isset($entregaId['min'])) {
                $this->addUsingAlias(AvisoPeer::ENTREGA_ID, $entregaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($entregaId['max'])) {
                $this->addUsingAlias(AvisoPeer::ENTREGA_ID, $entregaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::ENTREGA_ID, $entregaId, $comparison);
    }

    /**
     * Filter the query on the contabilidad_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContabilidadId(1234); // WHERE contabilidad_id = 1234
     * $query->filterByContabilidadId(array(12, 34)); // WHERE contabilidad_id IN (12, 34)
     * $query->filterByContabilidadId(array('min' => 12)); // WHERE contabilidad_id >= 12
     * $query->filterByContabilidadId(array('max' => 12)); // WHERE contabilidad_id <= 12
     * </code>
     *
     * @see       filterByContabilidad()
     *
     * @param     mixed $contabilidadId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByContabilidadId($contabilidadId = null, $comparison = null)
    {
        if (is_array($contabilidadId)) {
            $useMinMax = false;
            if (isset($contabilidadId['min'])) {
                $this->addUsingAlias(AvisoPeer::CONTABILIDAD_ID, $contabilidadId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contabilidadId['max'])) {
                $this->addUsingAlias(AvisoPeer::CONTABILIDAD_ID, $contabilidadId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::CONTABILIDAD_ID, $contabilidadId, $comparison);
    }

    /**
     * Filter the query on the tarea_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTareaId(1234); // WHERE tarea_id = 1234
     * $query->filterByTareaId(array(12, 34)); // WHERE tarea_id IN (12, 34)
     * $query->filterByTareaId(array('min' => 12)); // WHERE tarea_id >= 12
     * $query->filterByTareaId(array('max' => 12)); // WHERE tarea_id <= 12
     * </code>
     *
     * @see       filterByTarea()
     *
     * @param     mixed $tareaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function filterByTareaId($tareaId = null, $comparison = null)
    {
        if (is_array($tareaId)) {
            $useMinMax = false;
            if (isset($tareaId['min'])) {
                $this->addUsingAlias(AvisoPeer::TAREA_ID, $tareaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tareaId['max'])) {
                $this->addUsingAlias(AvisoPeer::TAREA_ID, $tareaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AvisoPeer::TAREA_ID, $tareaId, $comparison);
    }

    /**
     * Filter the query by a related Incidencia object
     *
     * @param   Incidencia|PropelObjectCollection $incidencia The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIncidencia($incidencia, $comparison = null)
    {
        if ($incidencia instanceof Incidencia) {
            return $this
                ->addUsingAlias(AvisoPeer::INCIDENCIA_ID, $incidencia->getId(), $comparison);
        } elseif ($incidencia instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::INCIDENCIA_ID, $incidencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByIncidencia() only accepts arguments of type Incidencia or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Incidencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinIncidencia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Incidencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Incidencia');
        }

        return $this;
    }

    /**
     * Use the Incidencia relation Incidencia object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   IncidenciaQuery A secondary query class using the current class as primary query
     */
    public function useIncidenciaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIncidencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Incidencia', 'IncidenciaQuery');
    }

    /**
     * Filter the query by a related Tarea object
     *
     * @param   Tarea|PropelObjectCollection $tarea The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTarea($tarea, $comparison = null)
    {
        if ($tarea instanceof Tarea) {
            return $this
                ->addUsingAlias(AvisoPeer::TAREA_ID, $tarea->getId(), $comparison);
        } elseif ($tarea instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::TAREA_ID, $tarea->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTarea() only accepts arguments of type Tarea or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tarea relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinTarea($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tarea');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tarea');
        }

        return $this;
    }

    /**
     * Use the Tarea relation Tarea object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TareaQuery A secondary query class using the current class as primary query
     */
    public function useTareaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTarea($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tarea', 'TareaQuery');
    }

    /**
     * Filter the query by a related Comunidad object
     *
     * @param   Comunidad|PropelObjectCollection $comunidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComunidad($comunidad, $comparison = null)
    {
        if ($comunidad instanceof Comunidad) {
            return $this
                ->addUsingAlias(AvisoPeer::COMUNIDAD_ID, $comunidad->getId(), $comparison);
        } elseif ($comunidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::COMUNIDAD_ID, $comunidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComunidad() only accepts arguments of type Comunidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comunidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinComunidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comunidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comunidad');
        }

        return $this;
    }

    /**
     * Use the Comunidad relation Comunidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComunidadQuery A secondary query class using the current class as primary query
     */
    public function useComunidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComunidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comunidad', 'ComunidadQuery');
    }

    /**
     * Filter the query by a related Persona object
     *
     * @param   Persona|PropelObjectCollection $persona The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPersona($persona, $comparison = null)
    {
        if ($persona instanceof Persona) {
            return $this
                ->addUsingAlias(AvisoPeer::PERSONA_ID, $persona->getId(), $comparison);
        } elseif ($persona instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::PERSONA_ID, $persona->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPersona() only accepts arguments of type Persona or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persona relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinPersona($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persona');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persona');
        }

        return $this;
    }

    /**
     * Use the Persona relation Persona object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PersonaQuery A secondary query class using the current class as primary query
     */
    public function usePersonaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPersona($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persona', 'PersonaQuery');
    }

    /**
     * Filter the query by a related Entrega object
     *
     * @param   Entrega|PropelObjectCollection $entrega The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntrega($entrega, $comparison = null)
    {
        if ($entrega instanceof Entrega) {
            return $this
                ->addUsingAlias(AvisoPeer::ENTREGA_ID, $entrega->getId(), $comparison);
        } elseif ($entrega instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::ENTREGA_ID, $entrega->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntrega() only accepts arguments of type Entrega or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entrega relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinEntrega($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entrega');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entrega');
        }

        return $this;
    }

    /**
     * Use the Entrega relation Entrega object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   EntregaQuery A secondary query class using the current class as primary query
     */
    public function useEntregaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEntrega($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entrega', 'EntregaQuery');
    }

    /**
     * Filter the query by a related Contabilidad object
     *
     * @param   Contabilidad|PropelObjectCollection $contabilidad The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 AvisoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContabilidad($contabilidad, $comparison = null)
    {
        if ($contabilidad instanceof Contabilidad) {
            return $this
                ->addUsingAlias(AvisoPeer::CONTABILIDAD_ID, $contabilidad->getId(), $comparison);
        } elseif ($contabilidad instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AvisoPeer::CONTABILIDAD_ID, $contabilidad->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContabilidad() only accepts arguments of type Contabilidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contabilidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function joinContabilidad($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contabilidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contabilidad');
        }

        return $this;
    }

    /**
     * Use the Contabilidad relation Contabilidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ContabilidadQuery A secondary query class using the current class as primary query
     */
    public function useContabilidadQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContabilidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contabilidad', 'ContabilidadQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Aviso $aviso Object to remove from the list of results
     *
     * @return AvisoQuery The current query, for fluid interface
     */
    public function prune($aviso = null)
    {
        if ($aviso) {
            $this->addUsingAlias(AvisoPeer::ID, $aviso->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
