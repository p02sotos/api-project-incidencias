<?php


/**
 * Base class that represents a row from the 'aviso' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseAviso extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'AvisoPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        AvisoPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the descripcion field.
     * @var        string
     */
    protected $descripcion;

    /**
     * The value for the prioridad field.
     * @var        int
     */
    protected $prioridad;

    /**
     * The value for the marcada field.
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the eliminado field.
     * @var        boolean
     */
    protected $eliminado;

    /**
     * The value for the fecha_aviso field.
     * @var        string
     */
    protected $fecha_aviso;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * The value for the incidencia_id field.
     * @var        int
     */
    protected $incidencia_id;

    /**
     * The value for the comunidad_id field.
     * @var        int
     */
    protected $comunidad_id;

    /**
     * The value for the persona_id field.
     * @var        int
     */
    protected $persona_id;

    /**
     * The value for the entrega_id field.
     * @var        int
     */
    protected $entrega_id;

    /**
     * The value for the contabilidad_id field.
     * @var        int
     */
    protected $contabilidad_id;

    /**
     * The value for the tarea_id field.
     * @var        int
     */
    protected $tarea_id;

    /**
     * @var        Incidencia
     */
    protected $aIncidencia;

    /**
     * @var        Tarea
     */
    protected $aTarea;

    /**
     * @var        Comunidad
     */
    protected $aComunidad;

    /**
     * @var        Persona
     */
    protected $aPersona;

    /**
     * @var        Entrega
     */
    protected $aEntrega;

    /**
     * @var        Contabilidad
     */
    protected $aContabilidad;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     * 
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [descripcion] column value.
     * 
     * @return string
     */
    public function getDescripcion()
    {

        return $this->descripcion;
    }

    /**
     * Get the [prioridad] column value.
     * 
     * @return int
     */
    public function getPrioridad()
    {

        return $this->prioridad;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [eliminado] column value.
     * 
     * @return boolean
     */
    public function getEliminado()
    {

        return $this->eliminado;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_aviso] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaAviso($format = '%x')
    {
        if ($this->fecha_aviso === null) {
            return null;
        }

        if ($this->fecha_aviso === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_aviso);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_aviso, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [incidencia_id] column value.
     * 
     * @return int
     */
    public function getIncidenciaId()
    {

        return $this->incidencia_id;
    }

    /**
     * Get the [comunidad_id] column value.
     * 
     * @return int
     */
    public function getComunidadId()
    {

        return $this->comunidad_id;
    }

    /**
     * Get the [persona_id] column value.
     * 
     * @return int
     */
    public function getPersonaId()
    {

        return $this->persona_id;
    }

    /**
     * Get the [entrega_id] column value.
     * 
     * @return int
     */
    public function getEntregaId()
    {

        return $this->entrega_id;
    }

    /**
     * Get the [contabilidad_id] column value.
     * 
     * @return int
     */
    public function getContabilidadId()
    {

        return $this->contabilidad_id;
    }

    /**
     * Get the [tarea_id] column value.
     * 
     * @return int
     */
    public function getTareaId()
    {

        return $this->tarea_id;
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = AvisoPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     * 
     * @param  string $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = AvisoPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [descripcion] column.
     * 
     * @param  string $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[] = AvisoPeer::DESCRIPCION;
        }


        return $this;
    } // setDescripcion()

    /**
     * Set the value of [prioridad] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setPrioridad($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->prioridad !== $v) {
            $this->prioridad = $v;
            $this->modifiedColumns[] = AvisoPeer::PRIORIDAD;
        }


        return $this;
    } // setPrioridad()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = AvisoPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Sets the value of the [eliminado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setEliminado($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->eliminado !== $v) {
            $this->eliminado = $v;
            $this->modifiedColumns[] = AvisoPeer::ELIMINADO;
        }


        return $this;
    } // setEliminado()

    /**
     * Sets the value of [fecha_aviso] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Aviso The current object (for fluent API support)
     */
    public function setFechaAviso($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_aviso !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_aviso !== null && $tmpDt = new DateTime($this->fecha_aviso)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_aviso = $newDateAsString;
                $this->modifiedColumns[] = AvisoPeer::FECHA_AVISO;
            }
        } // if either are not null


        return $this;
    } // setFechaAviso()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Aviso The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = AvisoPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Aviso The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = AvisoPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Set the value of [incidencia_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setIncidenciaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->incidencia_id !== $v) {
            $this->incidencia_id = $v;
            $this->modifiedColumns[] = AvisoPeer::INCIDENCIA_ID;
        }

        if ($this->aIncidencia !== null && $this->aIncidencia->getId() !== $v) {
            $this->aIncidencia = null;
        }


        return $this;
    } // setIncidenciaId()

    /**
     * Set the value of [comunidad_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setComunidadId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comunidad_id !== $v) {
            $this->comunidad_id = $v;
            $this->modifiedColumns[] = AvisoPeer::COMUNIDAD_ID;
        }

        if ($this->aComunidad !== null && $this->aComunidad->getId() !== $v) {
            $this->aComunidad = null;
        }


        return $this;
    } // setComunidadId()

    /**
     * Set the value of [persona_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setPersonaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->persona_id !== $v) {
            $this->persona_id = $v;
            $this->modifiedColumns[] = AvisoPeer::PERSONA_ID;
        }

        if ($this->aPersona !== null && $this->aPersona->getId() !== $v) {
            $this->aPersona = null;
        }


        return $this;
    } // setPersonaId()

    /**
     * Set the value of [entrega_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setEntregaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->entrega_id !== $v) {
            $this->entrega_id = $v;
            $this->modifiedColumns[] = AvisoPeer::ENTREGA_ID;
        }

        if ($this->aEntrega !== null && $this->aEntrega->getId() !== $v) {
            $this->aEntrega = null;
        }


        return $this;
    } // setEntregaId()

    /**
     * Set the value of [contabilidad_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setContabilidadId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->contabilidad_id !== $v) {
            $this->contabilidad_id = $v;
            $this->modifiedColumns[] = AvisoPeer::CONTABILIDAD_ID;
        }

        if ($this->aContabilidad !== null && $this->aContabilidad->getId() !== $v) {
            $this->aContabilidad = null;
        }


        return $this;
    } // setContabilidadId()

    /**
     * Set the value of [tarea_id] column.
     * 
     * @param  int $v new value
     * @return Aviso The current object (for fluent API support)
     */
    public function setTareaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->tarea_id !== $v) {
            $this->tarea_id = $v;
            $this->modifiedColumns[] = AvisoPeer::TAREA_ID;
        }

        if ($this->aTarea !== null && $this->aTarea->getId() !== $v) {
            $this->aTarea = null;
        }


        return $this;
    } // setTareaId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->descripcion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->prioridad = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->marcada = ($row[$startcol + 4] !== null) ? (boolean) $row[$startcol + 4] : null;
            $this->eliminado = ($row[$startcol + 5] !== null) ? (boolean) $row[$startcol + 5] : null;
            $this->fecha_aviso = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->fecha_creacion = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->fecha_modificacion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->incidencia_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->comunidad_id = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->persona_id = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->entrega_id = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->contabilidad_id = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->tarea_id = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 15; // 15 = AvisoPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Aviso object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aIncidencia !== null && $this->incidencia_id !== $this->aIncidencia->getId()) {
            $this->aIncidencia = null;
        }
        if ($this->aComunidad !== null && $this->comunidad_id !== $this->aComunidad->getId()) {
            $this->aComunidad = null;
        }
        if ($this->aPersona !== null && $this->persona_id !== $this->aPersona->getId()) {
            $this->aPersona = null;
        }
        if ($this->aEntrega !== null && $this->entrega_id !== $this->aEntrega->getId()) {
            $this->aEntrega = null;
        }
        if ($this->aContabilidad !== null && $this->contabilidad_id !== $this->aContabilidad->getId()) {
            $this->aContabilidad = null;
        }
        if ($this->aTarea !== null && $this->tarea_id !== $this->aTarea->getId()) {
            $this->aTarea = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AvisoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = AvisoPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aIncidencia = null;
            $this->aTarea = null;
            $this->aComunidad = null;
            $this->aPersona = null;
            $this->aEntrega = null;
            $this->aContabilidad = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AvisoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = AvisoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(AvisoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AvisoPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aIncidencia !== null) {
                if ($this->aIncidencia->isModified() || $this->aIncidencia->isNew()) {
                    $affectedRows += $this->aIncidencia->save($con);
                }
                $this->setIncidencia($this->aIncidencia);
            }

            if ($this->aTarea !== null) {
                if ($this->aTarea->isModified() || $this->aTarea->isNew()) {
                    $affectedRows += $this->aTarea->save($con);
                }
                $this->setTarea($this->aTarea);
            }

            if ($this->aComunidad !== null) {
                if ($this->aComunidad->isModified() || $this->aComunidad->isNew()) {
                    $affectedRows += $this->aComunidad->save($con);
                }
                $this->setComunidad($this->aComunidad);
            }

            if ($this->aPersona !== null) {
                if ($this->aPersona->isModified() || $this->aPersona->isNew()) {
                    $affectedRows += $this->aPersona->save($con);
                }
                $this->setPersona($this->aPersona);
            }

            if ($this->aEntrega !== null) {
                if ($this->aEntrega->isModified() || $this->aEntrega->isNew()) {
                    $affectedRows += $this->aEntrega->save($con);
                }
                $this->setEntrega($this->aEntrega);
            }

            if ($this->aContabilidad !== null) {
                if ($this->aContabilidad->isModified() || $this->aContabilidad->isNew()) {
                    $affectedRows += $this->aContabilidad->save($con);
                }
                $this->setContabilidad($this->aContabilidad);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = AvisoPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . AvisoPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AvisoPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(AvisoPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(AvisoPeer::DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = '`descripcion`';
        }
        if ($this->isColumnModified(AvisoPeer::PRIORIDAD)) {
            $modifiedColumns[':p' . $index++]  = '`prioridad`';
        }
        if ($this->isColumnModified(AvisoPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(AvisoPeer::ELIMINADO)) {
            $modifiedColumns[':p' . $index++]  = '`eliminado`';
        }
        if ($this->isColumnModified(AvisoPeer::FECHA_AVISO)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_aviso`';
        }
        if ($this->isColumnModified(AvisoPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(AvisoPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }
        if ($this->isColumnModified(AvisoPeer::INCIDENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`incidencia_id`';
        }
        if ($this->isColumnModified(AvisoPeer::COMUNIDAD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`comunidad_id`';
        }
        if ($this->isColumnModified(AvisoPeer::PERSONA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`persona_id`';
        }
        if ($this->isColumnModified(AvisoPeer::ENTREGA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`entrega_id`';
        }
        if ($this->isColumnModified(AvisoPeer::CONTABILIDAD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`contabilidad_id`';
        }
        if ($this->isColumnModified(AvisoPeer::TAREA_ID)) {
            $modifiedColumns[':p' . $index++]  = '`tarea_id`';
        }

        $sql = sprintf(
            'INSERT INTO `aviso` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':						
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`descripcion`':						
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case '`prioridad`':						
                        $stmt->bindValue($identifier, $this->prioridad, PDO::PARAM_INT);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`eliminado`':
                        $stmt->bindValue($identifier, (int) $this->eliminado, PDO::PARAM_INT);
                        break;
                    case '`fecha_aviso`':						
                        $stmt->bindValue($identifier, $this->fecha_aviso, PDO::PARAM_STR);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                    case '`incidencia_id`':						
                        $stmt->bindValue($identifier, $this->incidencia_id, PDO::PARAM_INT);
                        break;
                    case '`comunidad_id`':						
                        $stmt->bindValue($identifier, $this->comunidad_id, PDO::PARAM_INT);
                        break;
                    case '`persona_id`':						
                        $stmt->bindValue($identifier, $this->persona_id, PDO::PARAM_INT);
                        break;
                    case '`entrega_id`':						
                        $stmt->bindValue($identifier, $this->entrega_id, PDO::PARAM_INT);
                        break;
                    case '`contabilidad_id`':						
                        $stmt->bindValue($identifier, $this->contabilidad_id, PDO::PARAM_INT);
                        break;
                    case '`tarea_id`':						
                        $stmt->bindValue($identifier, $this->tarea_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aIncidencia !== null) {
                if (!$this->aIncidencia->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aIncidencia->getValidationFailures());
                }
            }

            if ($this->aTarea !== null) {
                if (!$this->aTarea->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTarea->getValidationFailures());
                }
            }

            if ($this->aComunidad !== null) {
                if (!$this->aComunidad->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aComunidad->getValidationFailures());
                }
            }

            if ($this->aPersona !== null) {
                if (!$this->aPersona->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPersona->getValidationFailures());
                }
            }

            if ($this->aEntrega !== null) {
                if (!$this->aEntrega->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntrega->getValidationFailures());
                }
            }

            if ($this->aContabilidad !== null) {
                if (!$this->aContabilidad->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aContabilidad->getValidationFailures());
                }
            }


            if (($retval = AvisoPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AvisoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDescripcion();
                break;
            case 3:
                return $this->getPrioridad();
                break;
            case 4:
                return $this->getMarcada();
                break;
            case 5:
                return $this->getEliminado();
                break;
            case 6:
                return $this->getFechaAviso();
                break;
            case 7:
                return $this->getFechaCreacion();
                break;
            case 8:
                return $this->getFechaModificacion();
                break;
            case 9:
                return $this->getIncidenciaId();
                break;
            case 10:
                return $this->getComunidadId();
                break;
            case 11:
                return $this->getPersonaId();
                break;
            case 12:
                return $this->getEntregaId();
                break;
            case 13:
                return $this->getContabilidadId();
                break;
            case 14:
                return $this->getTareaId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Aviso'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Aviso'][$this->getPrimaryKey()] = true;
        $keys = AvisoPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDescripcion(),
            $keys[3] => $this->getPrioridad(),
            $keys[4] => $this->getMarcada(),
            $keys[5] => $this->getEliminado(),
            $keys[6] => $this->getFechaAviso(),
            $keys[7] => $this->getFechaCreacion(),
            $keys[8] => $this->getFechaModificacion(),
            $keys[9] => $this->getIncidenciaId(),
            $keys[10] => $this->getComunidadId(),
            $keys[11] => $this->getPersonaId(),
            $keys[12] => $this->getEntregaId(),
            $keys[13] => $this->getContabilidadId(),
            $keys[14] => $this->getTareaId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aIncidencia) {
                $result['Incidencia'] = $this->aIncidencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTarea) {
                $result['Tarea'] = $this->aTarea->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComunidad) {
                $result['Comunidad'] = $this->aComunidad->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPersona) {
                $result['Persona'] = $this->aPersona->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEntrega) {
                $result['Entrega'] = $this->aEntrega->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContabilidad) {
                $result['Contabilidad'] = $this->aContabilidad->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = AvisoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDescripcion($value);
                break;
            case 3:
                $this->setPrioridad($value);
                break;
            case 4:
                $this->setMarcada($value);
                break;
            case 5:
                $this->setEliminado($value);
                break;
            case 6:
                $this->setFechaAviso($value);
                break;
            case 7:
                $this->setFechaCreacion($value);
                break;
            case 8:
                $this->setFechaModificacion($value);
                break;
            case 9:
                $this->setIncidenciaId($value);
                break;
            case 10:
                $this->setComunidadId($value);
                break;
            case 11:
                $this->setPersonaId($value);
                break;
            case 12:
                $this->setEntregaId($value);
                break;
            case 13:
                $this->setContabilidadId($value);
                break;
            case 14:
                $this->setTareaId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = AvisoPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDescripcion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPrioridad($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMarcada($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setEliminado($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setFechaAviso($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFechaCreacion($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFechaModificacion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setIncidenciaId($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setComunidadId($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setPersonaId($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setEntregaId($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setContabilidadId($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setTareaId($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AvisoPeer::DATABASE_NAME);

        if ($this->isColumnModified(AvisoPeer::ID)) $criteria->add(AvisoPeer::ID, $this->id);
        if ($this->isColumnModified(AvisoPeer::NOMBRE)) $criteria->add(AvisoPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(AvisoPeer::DESCRIPCION)) $criteria->add(AvisoPeer::DESCRIPCION, $this->descripcion);
        if ($this->isColumnModified(AvisoPeer::PRIORIDAD)) $criteria->add(AvisoPeer::PRIORIDAD, $this->prioridad);
        if ($this->isColumnModified(AvisoPeer::MARCADA)) $criteria->add(AvisoPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(AvisoPeer::ELIMINADO)) $criteria->add(AvisoPeer::ELIMINADO, $this->eliminado);
        if ($this->isColumnModified(AvisoPeer::FECHA_AVISO)) $criteria->add(AvisoPeer::FECHA_AVISO, $this->fecha_aviso);
        if ($this->isColumnModified(AvisoPeer::FECHA_CREACION)) $criteria->add(AvisoPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(AvisoPeer::FECHA_MODIFICACION)) $criteria->add(AvisoPeer::FECHA_MODIFICACION, $this->fecha_modificacion);
        if ($this->isColumnModified(AvisoPeer::INCIDENCIA_ID)) $criteria->add(AvisoPeer::INCIDENCIA_ID, $this->incidencia_id);
        if ($this->isColumnModified(AvisoPeer::COMUNIDAD_ID)) $criteria->add(AvisoPeer::COMUNIDAD_ID, $this->comunidad_id);
        if ($this->isColumnModified(AvisoPeer::PERSONA_ID)) $criteria->add(AvisoPeer::PERSONA_ID, $this->persona_id);
        if ($this->isColumnModified(AvisoPeer::ENTREGA_ID)) $criteria->add(AvisoPeer::ENTREGA_ID, $this->entrega_id);
        if ($this->isColumnModified(AvisoPeer::CONTABILIDAD_ID)) $criteria->add(AvisoPeer::CONTABILIDAD_ID, $this->contabilidad_id);
        if ($this->isColumnModified(AvisoPeer::TAREA_ID)) $criteria->add(AvisoPeer::TAREA_ID, $this->tarea_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(AvisoPeer::DATABASE_NAME);
        $criteria->add(AvisoPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Aviso (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setPrioridad($this->getPrioridad());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setEliminado($this->getEliminado());
        $copyObj->setFechaAviso($this->getFechaAviso());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setIncidenciaId($this->getIncidenciaId());
        $copyObj->setComunidadId($this->getComunidadId());
        $copyObj->setPersonaId($this->getPersonaId());
        $copyObj->setEntregaId($this->getEntregaId());
        $copyObj->setContabilidadId($this->getContabilidadId());
        $copyObj->setTareaId($this->getTareaId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Aviso Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return AvisoPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new AvisoPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Incidencia object.
     *
     * @param                  Incidencia $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIncidencia(Incidencia $v = null)
    {
        if ($v === null) {
            $this->setIncidenciaId(NULL);
        } else {
            $this->setIncidenciaId($v->getId());
        }

        $this->aIncidencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Incidencia object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoInc($this);
        }


        return $this;
    }


    /**
     * Get the associated Incidencia object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Incidencia The associated Incidencia object.
     * @throws PropelException
     */
    public function getIncidencia(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aIncidencia === null && ($this->incidencia_id !== null) && $doQuery) {
            $this->aIncidencia = IncidenciaQuery::create()->findPk($this->incidencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIncidencia->addAvisoIncs($this);
             */
        }

        return $this->aIncidencia;
    }

    /**
     * Declares an association between this object and a Tarea object.
     *
     * @param                  Tarea $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTarea(Tarea $v = null)
    {
        if ($v === null) {
            $this->setTareaId(NULL);
        } else {
            $this->setTareaId($v->getId());
        }

        $this->aTarea = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Tarea object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoTar($this);
        }


        return $this;
    }


    /**
     * Get the associated Tarea object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Tarea The associated Tarea object.
     * @throws PropelException
     */
    public function getTarea(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTarea === null && ($this->tarea_id !== null) && $doQuery) {
            $this->aTarea = TareaQuery::create()->findPk($this->tarea_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTarea->addAvisoTars($this);
             */
        }

        return $this->aTarea;
    }

    /**
     * Declares an association between this object and a Comunidad object.
     *
     * @param                  Comunidad $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComunidad(Comunidad $v = null)
    {
        if ($v === null) {
            $this->setComunidadId(NULL);
        } else {
            $this->setComunidadId($v->getId());
        }

        $this->aComunidad = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Comunidad object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoCom($this);
        }


        return $this;
    }


    /**
     * Get the associated Comunidad object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Comunidad The associated Comunidad object.
     * @throws PropelException
     */
    public function getComunidad(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aComunidad === null && ($this->comunidad_id !== null) && $doQuery) {
            $this->aComunidad = ComunidadQuery::create()->findPk($this->comunidad_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComunidad->addAvisoComs($this);
             */
        }

        return $this->aComunidad;
    }

    /**
     * Declares an association between this object and a Persona object.
     *
     * @param                  Persona $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPersona(Persona $v = null)
    {
        if ($v === null) {
            $this->setPersonaId(NULL);
        } else {
            $this->setPersonaId($v->getId());
        }

        $this->aPersona = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Persona object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoPer($this);
        }


        return $this;
    }


    /**
     * Get the associated Persona object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Persona The associated Persona object.
     * @throws PropelException
     */
    public function getPersona(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPersona === null && ($this->persona_id !== null) && $doQuery) {
            $this->aPersona = PersonaQuery::create()->findPk($this->persona_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPersona->addAvisoPers($this);
             */
        }

        return $this->aPersona;
    }

    /**
     * Declares an association between this object and a Entrega object.
     *
     * @param                  Entrega $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntrega(Entrega $v = null)
    {
        if ($v === null) {
            $this->setEntregaId(NULL);
        } else {
            $this->setEntregaId($v->getId());
        }

        $this->aEntrega = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entrega object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoEntrega($this);
        }


        return $this;
    }


    /**
     * Get the associated Entrega object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entrega The associated Entrega object.
     * @throws PropelException
     */
    public function getEntrega(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntrega === null && ($this->entrega_id !== null) && $doQuery) {
            $this->aEntrega = EntregaQuery::create()->findPk($this->entrega_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntrega->addAvisoEntregas($this);
             */
        }

        return $this->aEntrega;
    }

    /**
     * Declares an association between this object and a Contabilidad object.
     *
     * @param                  Contabilidad $v
     * @return Aviso The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContabilidad(Contabilidad $v = null)
    {
        if ($v === null) {
            $this->setContabilidadId(NULL);
        } else {
            $this->setContabilidadId($v->getId());
        }

        $this->aContabilidad = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Contabilidad object, it will not be re-added.
        if ($v !== null) {
            $v->addAvisoCon($this);
        }


        return $this;
    }


    /**
     * Get the associated Contabilidad object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Contabilidad The associated Contabilidad object.
     * @throws PropelException
     */
    public function getContabilidad(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aContabilidad === null && ($this->contabilidad_id !== null) && $doQuery) {
            $this->aContabilidad = ContabilidadQuery::create()->findPk($this->contabilidad_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContabilidad->addAvisoCons($this);
             */
        }

        return $this->aContabilidad;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->descripcion = null;
        $this->prioridad = null;
        $this->marcada = null;
        $this->eliminado = null;
        $this->fecha_aviso = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->incidencia_id = null;
        $this->comunidad_id = null;
        $this->persona_id = null;
        $this->entrega_id = null;
        $this->contabilidad_id = null;
        $this->tarea_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aIncidencia instanceof Persistent) {
              $this->aIncidencia->clearAllReferences($deep);
            }
            if ($this->aTarea instanceof Persistent) {
              $this->aTarea->clearAllReferences($deep);
            }
            if ($this->aComunidad instanceof Persistent) {
              $this->aComunidad->clearAllReferences($deep);
            }
            if ($this->aPersona instanceof Persistent) {
              $this->aPersona->clearAllReferences($deep);
            }
            if ($this->aEntrega instanceof Persistent) {
              $this->aEntrega->clearAllReferences($deep);
            }
            if ($this->aContabilidad instanceof Persistent) {
              $this->aContabilidad->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aIncidencia = null;
        $this->aTarea = null;
        $this->aComunidad = null;
        $this->aPersona = null;
        $this->aEntrega = null;
        $this->aContabilidad = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AvisoPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
