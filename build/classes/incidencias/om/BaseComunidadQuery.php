<?php


/**
 * Base class that represents a query for the 'comunidad' table.
 *
 * 
 *
 * @method ComunidadQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ComunidadQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method ComunidadQuery orderByDireccion($order = Criteria::ASC) Order by the direccion column
 * @method ComunidadQuery orderByPresidente($order = Criteria::ASC) Order by the presidente column
 * @method ComunidadQuery orderByCif($order = Criteria::ASC) Order by the cif column
 * @method ComunidadQuery orderByCuenta($order = Criteria::ASC) Order by the cuenta column
 * @method ComunidadQuery orderByMarcada($order = Criteria::ASC) Order by the marcada column
 * @method ComunidadQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method ComunidadQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 *
 * @method ComunidadQuery groupById() Group by the id column
 * @method ComunidadQuery groupByNombre() Group by the nombre column
 * @method ComunidadQuery groupByDireccion() Group by the direccion column
 * @method ComunidadQuery groupByPresidente() Group by the presidente column
 * @method ComunidadQuery groupByCif() Group by the cif column
 * @method ComunidadQuery groupByCuenta() Group by the cuenta column
 * @method ComunidadQuery groupByMarcada() Group by the marcada column
 * @method ComunidadQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method ComunidadQuery groupByFechaModificacion() Group by the fecha_modificacion column
 *
 * @method ComunidadQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ComunidadQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ComunidadQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ComunidadQuery leftJoinIncidenciaCom($relationAlias = null) Adds a LEFT JOIN clause to the query using the IncidenciaCom relation
 * @method ComunidadQuery rightJoinIncidenciaCom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the IncidenciaCom relation
 * @method ComunidadQuery innerJoinIncidenciaCom($relationAlias = null) Adds a INNER JOIN clause to the query using the IncidenciaCom relation
 *
 * @method ComunidadQuery leftJoinAvisoCom($relationAlias = null) Adds a LEFT JOIN clause to the query using the AvisoCom relation
 * @method ComunidadQuery rightJoinAvisoCom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AvisoCom relation
 * @method ComunidadQuery innerJoinAvisoCom($relationAlias = null) Adds a INNER JOIN clause to the query using the AvisoCom relation
 *
 * @method ComunidadQuery leftJoinEntregaC($relationAlias = null) Adds a LEFT JOIN clause to the query using the EntregaC relation
 * @method ComunidadQuery rightJoinEntregaC($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EntregaC relation
 * @method ComunidadQuery innerJoinEntregaC($relationAlias = null) Adds a INNER JOIN clause to the query using the EntregaC relation
 *
 * @method ComunidadQuery leftJoinContabilidad($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contabilidad relation
 * @method ComunidadQuery rightJoinContabilidad($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contabilidad relation
 * @method ComunidadQuery innerJoinContabilidad($relationAlias = null) Adds a INNER JOIN clause to the query using the Contabilidad relation
 *
 * @method ComunidadQuery leftJoinPersona($relationAlias = null) Adds a LEFT JOIN clause to the query using the Persona relation
 * @method ComunidadQuery rightJoinPersona($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Persona relation
 * @method ComunidadQuery innerJoinPersona($relationAlias = null) Adds a INNER JOIN clause to the query using the Persona relation
 *
 * @method ComunidadQuery leftJoinEmpresa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Empresa relation
 * @method ComunidadQuery rightJoinEmpresa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Empresa relation
 * @method ComunidadQuery innerJoinEmpresa($relationAlias = null) Adds a INNER JOIN clause to the query using the Empresa relation
 *
 * @method ComunidadQuery leftJoinCaja($relationAlias = null) Adds a LEFT JOIN clause to the query using the Caja relation
 * @method ComunidadQuery rightJoinCaja($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Caja relation
 * @method ComunidadQuery innerJoinCaja($relationAlias = null) Adds a INNER JOIN clause to the query using the Caja relation
 *
 * @method Comunidad findOne(PropelPDO $con = null) Return the first Comunidad matching the query
 * @method Comunidad findOneOrCreate(PropelPDO $con = null) Return the first Comunidad matching the query, or a new Comunidad object populated from the query conditions when no match is found
 *
 * @method Comunidad findOneByNombre(string $nombre) Return the first Comunidad filtered by the nombre column
 * @method Comunidad findOneByDireccion(string $direccion) Return the first Comunidad filtered by the direccion column
 * @method Comunidad findOneByPresidente(string $presidente) Return the first Comunidad filtered by the presidente column
 * @method Comunidad findOneByCif(string $cif) Return the first Comunidad filtered by the cif column
 * @method Comunidad findOneByCuenta(string $cuenta) Return the first Comunidad filtered by the cuenta column
 * @method Comunidad findOneByMarcada(boolean $marcada) Return the first Comunidad filtered by the marcada column
 * @method Comunidad findOneByFechaCreacion(string $fecha_creacion) Return the first Comunidad filtered by the fecha_creacion column
 * @method Comunidad findOneByFechaModificacion(string $fecha_modificacion) Return the first Comunidad filtered by the fecha_modificacion column
 *
 * @method array findById(int $id) Return Comunidad objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Comunidad objects filtered by the nombre column
 * @method array findByDireccion(string $direccion) Return Comunidad objects filtered by the direccion column
 * @method array findByPresidente(string $presidente) Return Comunidad objects filtered by the presidente column
 * @method array findByCif(string $cif) Return Comunidad objects filtered by the cif column
 * @method array findByCuenta(string $cuenta) Return Comunidad objects filtered by the cuenta column
 * @method array findByMarcada(boolean $marcada) Return Comunidad objects filtered by the marcada column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Comunidad objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Comunidad objects filtered by the fecha_modificacion column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseComunidadQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseComunidadQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Comunidad';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ComunidadQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ComunidadQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ComunidadQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ComunidadQuery) {
            return $criteria;
        }
        $query = new ComunidadQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Comunidad|Comunidad[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ComunidadPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ComunidadPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comunidad A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comunidad A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `direccion`, `presidente`, `cif`, `cuenta`, `marcada`, `fecha_creacion`, `fecha_modificacion` FROM `comunidad` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Comunidad();
            $obj->hydrate($row);
            ComunidadPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Comunidad|Comunidad[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Comunidad[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ComunidadPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ComunidadPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ComunidadPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ComunidadPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the direccion column
     *
     * Example usage:
     * <code>
     * $query->filterByDireccion('fooValue');   // WHERE direccion = 'fooValue'
     * $query->filterByDireccion('%fooValue%'); // WHERE direccion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $direccion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByDireccion($direccion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($direccion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $direccion)) {
                $direccion = str_replace('*', '%', $direccion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::DIRECCION, $direccion, $comparison);
    }

    /**
     * Filter the query on the presidente column
     *
     * Example usage:
     * <code>
     * $query->filterByPresidente('fooValue');   // WHERE presidente = 'fooValue'
     * $query->filterByPresidente('%fooValue%'); // WHERE presidente LIKE '%fooValue%'
     * </code>
     *
     * @param     string $presidente The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByPresidente($presidente = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($presidente)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $presidente)) {
                $presidente = str_replace('*', '%', $presidente);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::PRESIDENTE, $presidente, $comparison);
    }

    /**
     * Filter the query on the cif column
     *
     * Example usage:
     * <code>
     * $query->filterByCif('fooValue');   // WHERE cif = 'fooValue'
     * $query->filterByCif('%fooValue%'); // WHERE cif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByCif($cif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cif)) {
                $cif = str_replace('*', '%', $cif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::CIF, $cif, $comparison);
    }

    /**
     * Filter the query on the cuenta column
     *
     * Example usage:
     * <code>
     * $query->filterByCuenta('fooValue');   // WHERE cuenta = 'fooValue'
     * $query->filterByCuenta('%fooValue%'); // WHERE cuenta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cuenta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByCuenta($cuenta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cuenta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cuenta)) {
                $cuenta = str_replace('*', '%', $cuenta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::CUENTA, $cuenta, $comparison);
    }

    /**
     * Filter the query on the marcada column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcada(true); // WHERE marcada = true
     * $query->filterByMarcada('yes'); // WHERE marcada = true
     * </code>
     *
     * @param     boolean|string $marcada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByMarcada($marcada = null, $comparison = null)
    {
        if (is_string($marcada)) {
            $marcada = in_array(strtolower($marcada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ComunidadPeer::MARCADA, $marcada, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(ComunidadPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(ComunidadPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(ComunidadPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(ComunidadPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComunidadPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query by a related Incidencia object
     *
     * @param   Incidencia|PropelObjectCollection $incidencia  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIncidenciaCom($incidencia, $comparison = null)
    {
        if ($incidencia instanceof Incidencia) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $incidencia->getComunidadId(), $comparison);
        } elseif ($incidencia instanceof PropelObjectCollection) {
            return $this
                ->useIncidenciaComQuery()
                ->filterByPrimaryKeys($incidencia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByIncidenciaCom() only accepts arguments of type Incidencia or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the IncidenciaCom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinIncidenciaCom($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('IncidenciaCom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'IncidenciaCom');
        }

        return $this;
    }

    /**
     * Use the IncidenciaCom relation Incidencia object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   IncidenciaQuery A secondary query class using the current class as primary query
     */
    public function useIncidenciaComQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIncidenciaCom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'IncidenciaCom', 'IncidenciaQuery');
    }

    /**
     * Filter the query by a related Aviso object
     *
     * @param   Aviso|PropelObjectCollection $aviso  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAvisoCom($aviso, $comparison = null)
    {
        if ($aviso instanceof Aviso) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $aviso->getComunidadId(), $comparison);
        } elseif ($aviso instanceof PropelObjectCollection) {
            return $this
                ->useAvisoComQuery()
                ->filterByPrimaryKeys($aviso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAvisoCom() only accepts arguments of type Aviso or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AvisoCom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinAvisoCom($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AvisoCom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AvisoCom');
        }

        return $this;
    }

    /**
     * Use the AvisoCom relation Aviso object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   AvisoQuery A secondary query class using the current class as primary query
     */
    public function useAvisoComQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAvisoCom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AvisoCom', 'AvisoQuery');
    }

    /**
     * Filter the query by a related Entrega object
     *
     * @param   Entrega|PropelObjectCollection $entrega  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntregaC($entrega, $comparison = null)
    {
        if ($entrega instanceof Entrega) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $entrega->getComunidadId(), $comparison);
        } elseif ($entrega instanceof PropelObjectCollection) {
            return $this
                ->useEntregaCQuery()
                ->filterByPrimaryKeys($entrega->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEntregaC() only accepts arguments of type Entrega or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EntregaC relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinEntregaC($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EntregaC');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EntregaC');
        }

        return $this;
    }

    /**
     * Use the EntregaC relation Entrega object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   EntregaQuery A secondary query class using the current class as primary query
     */
    public function useEntregaCQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEntregaC($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EntregaC', 'EntregaQuery');
    }

    /**
     * Filter the query by a related Contabilidad object
     *
     * @param   Contabilidad|PropelObjectCollection $contabilidad  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContabilidad($contabilidad, $comparison = null)
    {
        if ($contabilidad instanceof Contabilidad) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $contabilidad->getComunidadId(), $comparison);
        } elseif ($contabilidad instanceof PropelObjectCollection) {
            return $this
                ->useContabilidadQuery()
                ->filterByPrimaryKeys($contabilidad->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContabilidad() only accepts arguments of type Contabilidad or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contabilidad relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinContabilidad($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contabilidad');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contabilidad');
        }

        return $this;
    }

    /**
     * Use the Contabilidad relation Contabilidad object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ContabilidadQuery A secondary query class using the current class as primary query
     */
    public function useContabilidadQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContabilidad($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contabilidad', 'ContabilidadQuery');
    }

    /**
     * Filter the query by a related Persona object
     *
     * @param   Persona|PropelObjectCollection $persona  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPersona($persona, $comparison = null)
    {
        if ($persona instanceof Persona) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $persona->getComunidadId(), $comparison);
        } elseif ($persona instanceof PropelObjectCollection) {
            return $this
                ->usePersonaQuery()
                ->filterByPrimaryKeys($persona->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPersona() only accepts arguments of type Persona or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Persona relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinPersona($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Persona');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Persona');
        }

        return $this;
    }

    /**
     * Use the Persona relation Persona object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PersonaQuery A secondary query class using the current class as primary query
     */
    public function usePersonaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPersona($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Persona', 'PersonaQuery');
    }

    /**
     * Filter the query by a related Empresa object
     *
     * @param   Empresa|PropelObjectCollection $empresa  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEmpresa($empresa, $comparison = null)
    {
        if ($empresa instanceof Empresa) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $empresa->getComunidadId(), $comparison);
        } elseif ($empresa instanceof PropelObjectCollection) {
            return $this
                ->useEmpresaQuery()
                ->filterByPrimaryKeys($empresa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEmpresa() only accepts arguments of type Empresa or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Empresa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinEmpresa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Empresa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Empresa');
        }

        return $this;
    }

    /**
     * Use the Empresa relation Empresa object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   EmpresaQuery A secondary query class using the current class as primary query
     */
    public function useEmpresaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEmpresa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Empresa', 'EmpresaQuery');
    }

    /**
     * Filter the query by a related Caja object
     *
     * @param   Caja|PropelObjectCollection $caja  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComunidadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCaja($caja, $comparison = null)
    {
        if ($caja instanceof Caja) {
            return $this
                ->addUsingAlias(ComunidadPeer::ID, $caja->getComunidadId(), $comparison);
        } elseif ($caja instanceof PropelObjectCollection) {
            return $this
                ->useCajaQuery()
                ->filterByPrimaryKeys($caja->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCaja() only accepts arguments of type Caja or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Caja relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function joinCaja($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Caja');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Caja');
        }

        return $this;
    }

    /**
     * Use the Caja relation Caja object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CajaQuery A secondary query class using the current class as primary query
     */
    public function useCajaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCaja($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Caja', 'CajaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Comunidad $comunidad Object to remove from the list of results
     *
     * @return ComunidadQuery The current query, for fluid interface
     */
    public function prune($comunidad = null)
    {
        if ($comunidad) {
            $this->addUsingAlias(ComunidadPeer::ID, $comunidad->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
