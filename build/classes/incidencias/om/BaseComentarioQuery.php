<?php


/**
 * Base class that represents a query for the 'comentario' table.
 *
 * 
 *
 * @method ComentarioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ComentarioQuery orderByOrigen($order = Criteria::ASC) Order by the origen column
 * @method ComentarioQuery orderByTexto($order = Criteria::ASC) Order by the texto column
 * @method ComentarioQuery orderByHoraEntrada($order = Criteria::ASC) Order by the hora_entrada column
 * @method ComentarioQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method ComentarioQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 *
 * @method ComentarioQuery groupById() Group by the id column
 * @method ComentarioQuery groupByOrigen() Group by the origen column
 * @method ComentarioQuery groupByTexto() Group by the texto column
 * @method ComentarioQuery groupByHoraEntrada() Group by the hora_entrada column
 * @method ComentarioQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method ComentarioQuery groupByFechaModificacion() Group by the fecha_modificacion column
 *
 * @method ComentarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ComentarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ComentarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Comentario findOne(PropelPDO $con = null) Return the first Comentario matching the query
 * @method Comentario findOneOrCreate(PropelPDO $con = null) Return the first Comentario matching the query, or a new Comentario object populated from the query conditions when no match is found
 *
 * @method Comentario findOneByOrigen(string $origen) Return the first Comentario filtered by the origen column
 * @method Comentario findOneByTexto(string $texto) Return the first Comentario filtered by the texto column
 * @method Comentario findOneByHoraEntrada(string $hora_entrada) Return the first Comentario filtered by the hora_entrada column
 * @method Comentario findOneByFechaCreacion(string $fecha_creacion) Return the first Comentario filtered by the fecha_creacion column
 * @method Comentario findOneByFechaModificacion(string $fecha_modificacion) Return the first Comentario filtered by the fecha_modificacion column
 *
 * @method array findById(int $id) Return Comentario objects filtered by the id column
 * @method array findByOrigen(string $origen) Return Comentario objects filtered by the origen column
 * @method array findByTexto(string $texto) Return Comentario objects filtered by the texto column
 * @method array findByHoraEntrada(string $hora_entrada) Return Comentario objects filtered by the hora_entrada column
 * @method array findByFechaCreacion(string $fecha_creacion) Return Comentario objects filtered by the fecha_creacion column
 * @method array findByFechaModificacion(string $fecha_modificacion) Return Comentario objects filtered by the fecha_modificacion column
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseComentarioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseComentarioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'incidencias';
        }
        if (null === $modelName) {
            $modelName = 'Comentario';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ComentarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ComentarioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ComentarioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ComentarioQuery) {
            return $criteria;
        }
        $query = new ComentarioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Comentario|Comentario[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ComentarioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ComentarioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comentario A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comentario A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `origen`, `texto`, `hora_entrada`, `fecha_creacion`, `fecha_modificacion` FROM `comentario` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Comentario();
            $obj->hydrate($row);
            ComentarioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Comentario|Comentario[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Comentario[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ComentarioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ComentarioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ComentarioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ComentarioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the origen column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigen('fooValue');   // WHERE origen = 'fooValue'
     * $query->filterByOrigen('%fooValue%'); // WHERE origen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $origen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByOrigen($origen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($origen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $origen)) {
                $origen = str_replace('*', '%', $origen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::ORIGEN, $origen, $comparison);
    }

    /**
     * Filter the query on the texto column
     *
     * Example usage:
     * <code>
     * $query->filterByTexto('fooValue');   // WHERE texto = 'fooValue'
     * $query->filterByTexto('%fooValue%'); // WHERE texto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $texto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByTexto($texto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($texto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $texto)) {
                $texto = str_replace('*', '%', $texto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::TEXTO, $texto, $comparison);
    }

    /**
     * Filter the query on the hora_entrada column
     *
     * Example usage:
     * <code>
     * $query->filterByHoraEntrada('2011-03-14'); // WHERE hora_entrada = '2011-03-14'
     * $query->filterByHoraEntrada('now'); // WHERE hora_entrada = '2011-03-14'
     * $query->filterByHoraEntrada(array('max' => 'yesterday')); // WHERE hora_entrada < '2011-03-13'
     * </code>
     *
     * @param     mixed $horaEntrada The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByHoraEntrada($horaEntrada = null, $comparison = null)
    {
        if (is_array($horaEntrada)) {
            $useMinMax = false;
            if (isset($horaEntrada['min'])) {
                $this->addUsingAlias(ComentarioPeer::HORA_ENTRADA, $horaEntrada['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($horaEntrada['max'])) {
                $this->addUsingAlias(ComentarioPeer::HORA_ENTRADA, $horaEntrada['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::HORA_ENTRADA, $horaEntrada, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(ComentarioPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(ComentarioPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(ComentarioPeer::FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(ComentarioPeer::FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComentarioPeer::FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Comentario $comentario Object to remove from the list of results
     *
     * @return ComentarioQuery The current query, for fluid interface
     */
    public function prune($comentario = null)
    {
        if ($comentario) {
            $this->addUsingAlias(ComentarioPeer::ID, $comentario->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
