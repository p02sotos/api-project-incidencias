<?php


/**
 * Base class that represents a row from the 'empresa' table.
 *
 * 
 *
 * @package    propel.generator.incidencias.om
 */
abstract class BaseEmpresa extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'EmpresaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        EmpresaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the direccion field.
     * @var        string
     */
    protected $direccion;

    /**
     * The value for the cif field.
     * @var        string
     */
    protected $cif;

    /**
     * The value for the contacto field.
     * @var        string
     */
    protected $contacto;

    /**
     * The value for the observaciones field.
     * @var        string
     */
    protected $observaciones;

    /**
     * The value for the marcada field.
     * @var        boolean
     */
    protected $marcada;

    /**
     * The value for the comunidad_id field.
     * @var        int
     */
    protected $comunidad_id;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     * @var        string
     */
    protected $fecha_modificacion;

    /**
     * @var        Comunidad
     */
    protected $aComunidad;

    /**
     * @var        PropelObjectCollection|Telefono[] Collection to store aggregation of Telefono objects.
     */
    protected $collTelefonoEmpresas;
    protected $collTelefonoEmpresasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $telefonoEmpresasScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     * 
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     * 
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [direccion] column value.
     * 
     * @return string
     */
    public function getDireccion()
    {

        return $this->direccion;
    }

    /**
     * Get the [cif] column value.
     * 
     * @return string
     */
    public function getCif()
    {

        return $this->cif;
    }

    /**
     * Get the [contacto] column value.
     * 
     * @return string
     */
    public function getContacto()
    {

        return $this->contacto;
    }

    /**
     * Get the [observaciones] column value.
     * 
     * @return string
     */
    public function getObservaciones()
    {

        return $this->observaciones;
    }

    /**
     * Get the [marcada] column value.
     * 
     * @return boolean
     */
    public function getMarcada()
    {

        return $this->marcada;
    }

    /**
     * Get the [comunidad_id] column value.
     * 
     * @return int
     */
    public function getComunidadId()
    {

        return $this->comunidad_id;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     * 
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaModificacion($format = '%x')
    {
        if ($this->fecha_modificacion === null) {
            return null;
        }

        if ($this->fecha_modificacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_modificacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_modificacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
        
    }

    /**
     * Set the value of [id] column.
     * 
     * @param  int $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = EmpresaPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     * 
     * @param  string $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = EmpresaPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [direccion] column.
     * 
     * @param  string $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setDireccion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->direccion !== $v) {
            $this->direccion = $v;
            $this->modifiedColumns[] = EmpresaPeer::DIRECCION;
        }


        return $this;
    } // setDireccion()

    /**
     * Set the value of [cif] column.
     * 
     * @param  string $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setCif($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cif !== $v) {
            $this->cif = $v;
            $this->modifiedColumns[] = EmpresaPeer::CIF;
        }


        return $this;
    } // setCif()

    /**
     * Set the value of [contacto] column.
     * 
     * @param  string $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setContacto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->contacto !== $v) {
            $this->contacto = $v;
            $this->modifiedColumns[] = EmpresaPeer::CONTACTO;
        }


        return $this;
    } // setContacto()

    /**
     * Set the value of [observaciones] column.
     * 
     * @param  string $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setObservaciones($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observaciones !== $v) {
            $this->observaciones = $v;
            $this->modifiedColumns[] = EmpresaPeer::OBSERVACIONES;
        }


        return $this;
    } // setObservaciones()

    /**
     * Sets the value of the [marcada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * 
     * @param boolean|integer|string $v The new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setMarcada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marcada !== $v) {
            $this->marcada = $v;
            $this->modifiedColumns[] = EmpresaPeer::MARCADA;
        }


        return $this;
    } // setMarcada()

    /**
     * Set the value of [comunidad_id] column.
     * 
     * @param  int $v new value
     * @return Empresa The current object (for fluent API support)
     */
    public function setComunidadId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->comunidad_id !== $v) {
            $this->comunidad_id = $v;
            $this->modifiedColumns[] = EmpresaPeer::COMUNIDAD_ID;
        }

        if ($this->aComunidad !== null && $this->aComunidad->getId() !== $v) {
            $this->aComunidad = null;
        }


        return $this;
    } // setComunidadId()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Empresa The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = EmpresaPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     * 
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Empresa The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_modificacion !== null && $tmpDt = new DateTime($this->fecha_modificacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_modificacion = $newDateAsString;
                $this->modifiedColumns[] = EmpresaPeer::FECHA_MODIFICACION;
            }
        } // if either are not null


        return $this;
    } // setFechaModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->direccion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->cif = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->contacto = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->observaciones = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->marcada = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->comunidad_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->fecha_creacion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->fecha_modificacion = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = EmpresaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Empresa object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aComunidad !== null && $this->comunidad_id !== $this->aComunidad->getId()) {
            $this->aComunidad = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(EmpresaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = EmpresaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aComunidad = null;
            $this->collTelefonoEmpresas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(EmpresaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = EmpresaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(EmpresaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                EmpresaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if ($this->aComunidad->isModified() || $this->aComunidad->isNew()) {
                    $affectedRows += $this->aComunidad->save($con);
                }
                $this->setComunidad($this->aComunidad);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->telefonoEmpresasScheduledForDeletion !== null) {
                if (!$this->telefonoEmpresasScheduledForDeletion->isEmpty()) {
                    foreach ($this->telefonoEmpresasScheduledForDeletion as $telefonoEmpresa) {
                        // need to save related object because we set the relation to null
                        $telefonoEmpresa->save($con);
                    }
                    $this->telefonoEmpresasScheduledForDeletion = null;
                }
            }

            if ($this->collTelefonoEmpresas !== null) {
                foreach ($this->collTelefonoEmpresas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = EmpresaPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . EmpresaPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(EmpresaPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(EmpresaPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(EmpresaPeer::DIRECCION)) {
            $modifiedColumns[':p' . $index++]  = '`direccion`';
        }
        if ($this->isColumnModified(EmpresaPeer::CIF)) {
            $modifiedColumns[':p' . $index++]  = '`cif`';
        }
        if ($this->isColumnModified(EmpresaPeer::CONTACTO)) {
            $modifiedColumns[':p' . $index++]  = '`contacto`';
        }
        if ($this->isColumnModified(EmpresaPeer::OBSERVACIONES)) {
            $modifiedColumns[':p' . $index++]  = '`observaciones`';
        }
        if ($this->isColumnModified(EmpresaPeer::MARCADA)) {
            $modifiedColumns[':p' . $index++]  = '`marcada`';
        }
        if ($this->isColumnModified(EmpresaPeer::COMUNIDAD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`comunidad_id`';
        }
        if ($this->isColumnModified(EmpresaPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(EmpresaPeer::FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_modificacion`';
        }

        $sql = sprintf(
            'INSERT INTO `empresa` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':						
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':						
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`direccion`':						
                        $stmt->bindValue($identifier, $this->direccion, PDO::PARAM_STR);
                        break;
                    case '`cif`':						
                        $stmt->bindValue($identifier, $this->cif, PDO::PARAM_STR);
                        break;
                    case '`contacto`':						
                        $stmt->bindValue($identifier, $this->contacto, PDO::PARAM_STR);
                        break;
                    case '`observaciones`':						
                        $stmt->bindValue($identifier, $this->observaciones, PDO::PARAM_STR);
                        break;
                    case '`marcada`':
                        $stmt->bindValue($identifier, (int) $this->marcada, PDO::PARAM_INT);
                        break;
                    case '`comunidad_id`':						
                        $stmt->bindValue($identifier, $this->comunidad_id, PDO::PARAM_INT);
                        break;
                    case '`fecha_creacion`':						
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_modificacion`':						
                        $stmt->bindValue($identifier, $this->fecha_modificacion, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComunidad !== null) {
                if (!$this->aComunidad->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aComunidad->getValidationFailures());
                }
            }


            if (($retval = EmpresaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collTelefonoEmpresas !== null) {
                    foreach ($this->collTelefonoEmpresas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = EmpresaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDireccion();
                break;
            case 3:
                return $this->getCif();
                break;
            case 4:
                return $this->getContacto();
                break;
            case 5:
                return $this->getObservaciones();
                break;
            case 6:
                return $this->getMarcada();
                break;
            case 7:
                return $this->getComunidadId();
                break;
            case 8:
                return $this->getFechaCreacion();
                break;
            case 9:
                return $this->getFechaModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Empresa'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Empresa'][$this->getPrimaryKey()] = true;
        $keys = EmpresaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDireccion(),
            $keys[3] => $this->getCif(),
            $keys[4] => $this->getContacto(),
            $keys[5] => $this->getObservaciones(),
            $keys[6] => $this->getMarcada(),
            $keys[7] => $this->getComunidadId(),
            $keys[8] => $this->getFechaCreacion(),
            $keys[9] => $this->getFechaModificacion(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }
        
        if ($includeForeignObjects) {
            if (null !== $this->aComunidad) {
                $result['Comunidad'] = $this->aComunidad->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collTelefonoEmpresas) {
                $result['TelefonoEmpresas'] = $this->collTelefonoEmpresas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = EmpresaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDireccion($value);
                break;
            case 3:
                $this->setCif($value);
                break;
            case 4:
                $this->setContacto($value);
                break;
            case 5:
                $this->setObservaciones($value);
                break;
            case 6:
                $this->setMarcada($value);
                break;
            case 7:
                $this->setComunidadId($value);
                break;
            case 8:
                $this->setFechaCreacion($value);
                break;
            case 9:
                $this->setFechaModificacion($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = EmpresaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDireccion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCif($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setContacto($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setObservaciones($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMarcada($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setComunidadId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFechaCreacion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setFechaModificacion($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(EmpresaPeer::DATABASE_NAME);

        if ($this->isColumnModified(EmpresaPeer::ID)) $criteria->add(EmpresaPeer::ID, $this->id);
        if ($this->isColumnModified(EmpresaPeer::NOMBRE)) $criteria->add(EmpresaPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(EmpresaPeer::DIRECCION)) $criteria->add(EmpresaPeer::DIRECCION, $this->direccion);
        if ($this->isColumnModified(EmpresaPeer::CIF)) $criteria->add(EmpresaPeer::CIF, $this->cif);
        if ($this->isColumnModified(EmpresaPeer::CONTACTO)) $criteria->add(EmpresaPeer::CONTACTO, $this->contacto);
        if ($this->isColumnModified(EmpresaPeer::OBSERVACIONES)) $criteria->add(EmpresaPeer::OBSERVACIONES, $this->observaciones);
        if ($this->isColumnModified(EmpresaPeer::MARCADA)) $criteria->add(EmpresaPeer::MARCADA, $this->marcada);
        if ($this->isColumnModified(EmpresaPeer::COMUNIDAD_ID)) $criteria->add(EmpresaPeer::COMUNIDAD_ID, $this->comunidad_id);
        if ($this->isColumnModified(EmpresaPeer::FECHA_CREACION)) $criteria->add(EmpresaPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(EmpresaPeer::FECHA_MODIFICACION)) $criteria->add(EmpresaPeer::FECHA_MODIFICACION, $this->fecha_modificacion);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(EmpresaPeer::DATABASE_NAME);
        $criteria->add(EmpresaPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Empresa (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDireccion($this->getDireccion());
        $copyObj->setCif($this->getCif());
        $copyObj->setContacto($this->getContacto());
        $copyObj->setObservaciones($this->getObservaciones());
        $copyObj->setMarcada($this->getMarcada());
        $copyObj->setComunidadId($this->getComunidadId());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getTelefonoEmpresas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTelefonoEmpresa($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Empresa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return EmpresaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new EmpresaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Comunidad object.
     *
     * @param                  Comunidad $v
     * @return Empresa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComunidad(Comunidad $v = null)
    {
        if ($v === null) {
            $this->setComunidadId(NULL);
        } else {
            $this->setComunidadId($v->getId());
        }

        $this->aComunidad = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Comunidad object, it will not be re-added.
        if ($v !== null) {
            $v->addEmpresa($this);
        }


        return $this;
    }


    /**
     * Get the associated Comunidad object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Comunidad The associated Comunidad object.
     * @throws PropelException
     */
    public function getComunidad(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aComunidad === null && ($this->comunidad_id !== null) && $doQuery) {
            $this->aComunidad = ComunidadQuery::create()->findPk($this->comunidad_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComunidad->addEmpresas($this);
             */
        }

        return $this->aComunidad;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('TelefonoEmpresa' == $relationName) {
            $this->initTelefonoEmpresas();
        }
    }

    /**
     * Clears out the collTelefonoEmpresas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Empresa The current object (for fluent API support)
     * @see        addTelefonoEmpresas()
     */
    public function clearTelefonoEmpresas()
    {
        $this->collTelefonoEmpresas = null; // important to set this to null since that means it is uninitialized
        $this->collTelefonoEmpresasPartial = null;

        return $this;
    }

    /**
     * reset is the collTelefonoEmpresas collection loaded partially
     *
     * @return void
     */
    public function resetPartialTelefonoEmpresas($v = true)
    {
        $this->collTelefonoEmpresasPartial = $v;
    }

    /**
     * Initializes the collTelefonoEmpresas collection.
     *
     * By default this just sets the collTelefonoEmpresas collection to an empty array (like clearcollTelefonoEmpresas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTelefonoEmpresas($overrideExisting = true)
    {
        if (null !== $this->collTelefonoEmpresas && !$overrideExisting) {
            return;
        }
        $this->collTelefonoEmpresas = new PropelObjectCollection();
        $this->collTelefonoEmpresas->setModel('Telefono');
    }

    /**
     * Gets an array of Telefono objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Empresa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     * @throws PropelException
     */
    public function getTelefonoEmpresas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoEmpresasPartial && !$this->isNew();
        if (null === $this->collTelefonoEmpresas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTelefonoEmpresas) {
                // return empty collection
                $this->initTelefonoEmpresas();
            } else {
                $collTelefonoEmpresas = TelefonoQuery::create(null, $criteria)
                    ->filterByEmpresa($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collTelefonoEmpresasPartial && count($collTelefonoEmpresas)) {
                      $this->initTelefonoEmpresas(false);

                      foreach ($collTelefonoEmpresas as $obj) {
                        if (false == $this->collTelefonoEmpresas->contains($obj)) {
                          $this->collTelefonoEmpresas->append($obj);
                        }
                      }

                      $this->collTelefonoEmpresasPartial = true;
                    }

                    $collTelefonoEmpresas->getInternalIterator()->rewind();

                    return $collTelefonoEmpresas;
                }

                if ($partial && $this->collTelefonoEmpresas) {
                    foreach ($this->collTelefonoEmpresas as $obj) {
                        if ($obj->isNew()) {
                            $collTelefonoEmpresas[] = $obj;
                        }
                    }
                }

                $this->collTelefonoEmpresas = $collTelefonoEmpresas;
                $this->collTelefonoEmpresasPartial = false;
            }
        }

        return $this->collTelefonoEmpresas;
    }

    /**
     * Sets a collection of TelefonoEmpresa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $telefonoEmpresas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Empresa The current object (for fluent API support)
     */
    public function setTelefonoEmpresas(PropelCollection $telefonoEmpresas, PropelPDO $con = null)
    {
        $telefonoEmpresasToDelete = $this->getTelefonoEmpresas(new Criteria(), $con)->diff($telefonoEmpresas);


        $this->telefonoEmpresasScheduledForDeletion = $telefonoEmpresasToDelete;

        foreach ($telefonoEmpresasToDelete as $telefonoEmpresaRemoved) {
            $telefonoEmpresaRemoved->setEmpresa(null);
        }

        $this->collTelefonoEmpresas = null;
        foreach ($telefonoEmpresas as $telefonoEmpresa) {
            $this->addTelefonoEmpresa($telefonoEmpresa);
        }

        $this->collTelefonoEmpresas = $telefonoEmpresas;
        $this->collTelefonoEmpresasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Telefono objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Telefono objects.
     * @throws PropelException
     */
    public function countTelefonoEmpresas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collTelefonoEmpresasPartial && !$this->isNew();
        if (null === $this->collTelefonoEmpresas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTelefonoEmpresas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTelefonoEmpresas());
            }
            $query = TelefonoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEmpresa($this)
                ->count($con);
        }

        return count($this->collTelefonoEmpresas);
    }

    /**
     * Method called to associate a Telefono object to this object
     * through the Telefono foreign key attribute.
     *
     * @param    Telefono $l Telefono
     * @return Empresa The current object (for fluent API support)
     */
    public function addTelefonoEmpresa(Telefono $l)
    {
        if ($this->collTelefonoEmpresas === null) {
            $this->initTelefonoEmpresas();
            $this->collTelefonoEmpresasPartial = true;
        }

        if (!in_array($l, $this->collTelefonoEmpresas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddTelefonoEmpresa($l);

            if ($this->telefonoEmpresasScheduledForDeletion and $this->telefonoEmpresasScheduledForDeletion->contains($l)) {
                $this->telefonoEmpresasScheduledForDeletion->remove($this->telefonoEmpresasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	TelefonoEmpresa $telefonoEmpresa The telefonoEmpresa object to add.
     */
    protected function doAddTelefonoEmpresa($telefonoEmpresa)
    {
        $this->collTelefonoEmpresas[]= $telefonoEmpresa;
        $telefonoEmpresa->setEmpresa($this);
    }

    /**
     * @param	TelefonoEmpresa $telefonoEmpresa The telefonoEmpresa object to remove.
     * @return Empresa The current object (for fluent API support)
     */
    public function removeTelefonoEmpresa($telefonoEmpresa)
    {
        if ($this->getTelefonoEmpresas()->contains($telefonoEmpresa)) {
            $this->collTelefonoEmpresas->remove($this->collTelefonoEmpresas->search($telefonoEmpresa));
            if (null === $this->telefonoEmpresasScheduledForDeletion) {
                $this->telefonoEmpresasScheduledForDeletion = clone $this->collTelefonoEmpresas;
                $this->telefonoEmpresasScheduledForDeletion->clear();
            }
            $this->telefonoEmpresasScheduledForDeletion[]= $telefonoEmpresa;
            $telefonoEmpresa->setEmpresa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Empresa is new, it will return
     * an empty collection; or if this Empresa has previously
     * been saved, it will retrieve related TelefonoEmpresas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Empresa.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoEmpresasJoinPersona($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Persona', $join_behavior);

        return $this->getTelefonoEmpresas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Empresa is new, it will return
     * an empty collection; or if this Empresa has previously
     * been saved, it will retrieve related TelefonoEmpresas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Empresa.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Telefono[] List of Telefono objects
     */
    public function getTelefonoEmpresasJoinIncidencia($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = TelefonoQuery::create(null, $criteria);
        $query->joinWith('Incidencia', $join_behavior);

        return $this->getTelefonoEmpresas($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->direccion = null;
        $this->cif = null;
        $this->contacto = null;
        $this->observaciones = null;
        $this->marcada = null;
        $this->comunidad_id = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collTelefonoEmpresas) {
                foreach ($this->collTelefonoEmpresas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aComunidad instanceof Persistent) {
              $this->aComunidad->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collTelefonoEmpresas instanceof PropelCollection) {
            $this->collTelefonoEmpresas->clearIterator();
        }
        $this->collTelefonoEmpresas = null;
        $this->aComunidad = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(EmpresaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
