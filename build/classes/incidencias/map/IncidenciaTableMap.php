<?php



/**
 * This class defines the structure of the 'incidencia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class IncidenciaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.IncidenciaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('incidencia');
        $this->setPhpName('Incidencia');
        $this->setClassname('Incidencia');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('origen', 'Origen', 'VARCHAR', true, 1024, null);
        $this->addColumn('nombre_persona', 'NombrePersona', 'VARCHAR', true, 1024, null);
        $this->addColumn('breve', 'Breve', 'VARCHAR', false, 1024, null);
        $this->addColumn('expediente', 'Expediente', 'VARCHAR', false, 256, null);
        $this->addColumn('descripcion', 'Descripcion', 'LONGVARCHAR', false, null, null);
        $this->addColumn('atencion', 'Atencion', 'VARCHAR', false, 256, null);
        $this->addColumn('prioridad', 'Prioridad', 'INTEGER', false, null, null);
        $this->addColumn('resuelta', 'Resuelta', 'BOOLEAN', false, 1, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addColumn('eliminado', 'Eliminado', 'BOOLEAN', false, 1, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        $this->addColumn('fecha_incidencia', 'FechaIncidencia', 'DATE', false, null, null);
        $this->addColumn('fecha_resolucion', 'FechaResolucion', 'DATE', false, null, null);
        $this->addForeignKey('comunidad_id', 'ComunidadId', 'INTEGER', 'comunidad', 'id', false, null, null);
        $this->addForeignKey('persona_id', 'PersonaId', 'INTEGER', 'persona', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Comunidad', 'Comunidad', RelationMap::MANY_TO_ONE, array('comunidad_id' => 'id', ), null, null);
        $this->addRelation('Persona', 'Persona', RelationMap::MANY_TO_ONE, array('persona_id' => 'id', ), null, null);
        $this->addRelation('AvisoInc', 'Aviso', RelationMap::ONE_TO_MANY, array('id' => 'incidencia_id', ), null, null, 'AvisoIncs');
        $this->addRelation('Tarea', 'Tarea', RelationMap::ONE_TO_MANY, array('id' => 'incidencia_id', ), null, null, 'Tareas');
        $this->addRelation('Seguimiento', 'Seguimiento', RelationMap::ONE_TO_MANY, array('id' => 'incidencia_id', ), null, null, 'Seguimientos');
        $this->addRelation('TelefonoIncidencia', 'Telefono', RelationMap::ONE_TO_MANY, array('id' => 'incidencia_id', ), null, null, 'TelefonoIncidencias');
    } // buildRelations()

} // IncidenciaTableMap
