<?php



/**
 * This class defines the structure of the 'urgencia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class UrgenciaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.UrgenciaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('urgencia');
        $this->setPhpName('Urgencia');
        $this->setClassname('Urgencia');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1024, null);
        $this->addColumn('descripcion', 'Descripcion', 'LONGVARCHAR', false, null, null);
        $this->addColumn('prioridad', 'Prioridad', 'INTEGER', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('IncidenciaUrg', 'Incidencia', RelationMap::ONE_TO_MANY, array('id' => 'urgencia_id', ), null, null, 'IncidenciaUrgs');
    } // buildRelations()

} // UrgenciaTableMap
