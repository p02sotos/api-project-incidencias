<?php



/**
 * This class defines the structure of the 'tarea' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class TareaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.TareaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tarea');
        $this->setPhpName('Tarea');
        $this->setClassname('Tarea');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1024, null);
        $this->addColumn('descripcion', 'Descripcion', 'LONGVARCHAR', false, null, null);
        $this->addColumn('prioridad', 'Prioridad', 'INTEGER', true, null, null);
        $this->addColumn('orden', 'Orden', 'INTEGER', false, null, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addColumn('eliminado', 'Eliminado', 'BOOLEAN', false, 1, null);
        $this->addColumn('realizada', 'Realizada', 'BOOLEAN', false, 1, null);
        $this->addColumn('fecha_tarea', 'FechaTarea', 'DATE', false, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        $this->addForeignKey('incidencia_id', 'IncidenciaId', 'INTEGER', 'incidencia', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Incidencia', 'Incidencia', RelationMap::MANY_TO_ONE, array('incidencia_id' => 'id', ), null, null);
        $this->addRelation('AvisoTar', 'Aviso', RelationMap::ONE_TO_MANY, array('id' => 'tarea_id', ), null, null, 'AvisoTars');
    } // buildRelations()

} // TareaTableMap
