<?php



/**
 * This class defines the structure of the 'comunidad' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class ComunidadTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.ComunidadTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('comunidad');
        $this->setPhpName('Comunidad');
        $this->setClassname('Comunidad');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 128, null);
        $this->addColumn('direccion', 'Direccion', 'VARCHAR', false, 1028, null);
        $this->addColumn('presidente', 'Presidente', 'VARCHAR', false, 128, null);
        $this->addColumn('cif', 'Cif', 'VARCHAR', false, 128, null);
        $this->addColumn('cuenta', 'Cuenta', 'VARCHAR', false, 512, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('IncidenciaCom', 'Incidencia', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'IncidenciaComs');
        $this->addRelation('AvisoCom', 'Aviso', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'AvisoComs');
        $this->addRelation('EntregaC', 'Entrega', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'EntregaCs');
        $this->addRelation('Contabilidad', 'Contabilidad', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'Contabilidads');
        $this->addRelation('Persona', 'Persona', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'Personas');
        $this->addRelation('Empresa', 'Empresa', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'Empresas');
        $this->addRelation('Caja', 'Caja', RelationMap::ONE_TO_MANY, array('id' => 'comunidad_id', ), null, null, 'Cajas');
    } // buildRelations()

} // ComunidadTableMap
