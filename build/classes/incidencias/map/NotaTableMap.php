<?php



/**
 * This class defines the structure of the 'nota' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class NotaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.NotaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('nota');
        $this->setPhpName('Nota');
        $this->setClassname('Nota');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('texto', 'Texto', 'VARCHAR', true, 128, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('comunidad_id', 'ComunidadId', 'INTEGER', 'comunidad', 'id', false, null, null);
        $this->addForeignKey('persona_id', 'PersonaId', 'INTEGER', 'persona', 'id', false, null, null);
        $this->addForeignKey('incidencia_id', 'IncidenciaId', 'INTEGER', 'incidencia', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Comunidad', 'Comunidad', RelationMap::MANY_TO_ONE, array('comunidad_id' => 'id', ), null, null);
        $this->addRelation('Incidencia', 'Incidencia', RelationMap::MANY_TO_ONE, array('incidencia_id' => 'id', ), null, null);
        $this->addRelation('Persona', 'Persona', RelationMap::MANY_TO_ONE, array('persona_id' => 'id', ), null, null);
    } // buildRelations()

} // NotaTableMap
