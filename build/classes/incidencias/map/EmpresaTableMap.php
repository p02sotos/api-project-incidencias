<?php



/**
 * This class defines the structure of the 'empresa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class EmpresaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.EmpresaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('empresa');
        $this->setPhpName('Empresa');
        $this->setClassname('Empresa');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 128, null);
        $this->addColumn('direccion', 'Direccion', 'VARCHAR', false, 1024, null);
        $this->addColumn('cif', 'Cif', 'VARCHAR', false, 128, null);
        $this->addColumn('contacto', 'Contacto', 'VARCHAR', false, 128, null);
        $this->addColumn('observaciones', 'Observaciones', 'LONGVARCHAR', false, null, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('comunidad_id', 'ComunidadId', 'INTEGER', 'comunidad', 'id', false, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Comunidad', 'Comunidad', RelationMap::MANY_TO_ONE, array('comunidad_id' => 'id', ), null, null);
        $this->addRelation('TelefonoEmpresa', 'Telefono', RelationMap::ONE_TO_MANY, array('id' => 'empresa_id', ), null, null, 'TelefonoEmpresas');
    } // buildRelations()

} // EmpresaTableMap
