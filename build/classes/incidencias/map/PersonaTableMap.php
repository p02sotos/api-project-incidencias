<?php



/**
 * This class defines the structure of the 'persona' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.incidencias.map
 */
class PersonaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'incidencias.map.PersonaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('persona');
        $this->setPhpName('Persona');
        $this->setClassname('Persona');
        $this->setPackage('incidencias');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 128, null);
        $this->addColumn('direccion', 'Direccion', 'VARCHAR', false, 128, null);
        $this->addColumn('marcada', 'Marcada', 'BOOLEAN', false, 1, null);
        $this->addColumn('nota', 'Nota', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('comunidad_id', 'ComunidadId', 'INTEGER', 'comunidad', 'id', false, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'DATE', false, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Comunidad', 'Comunidad', RelationMap::MANY_TO_ONE, array('comunidad_id' => 'id', ), null, null);
        $this->addRelation('IncidenciaPer', 'Incidencia', RelationMap::ONE_TO_MANY, array('id' => 'persona_id', ), null, null, 'IncidenciaPers');
        $this->addRelation('AvisoPer', 'Aviso', RelationMap::ONE_TO_MANY, array('id' => 'persona_id', ), null, null, 'AvisoPers');
        $this->addRelation('TelefonoPersona', 'Telefono', RelationMap::ONE_TO_MANY, array('id' => 'persona_id', ), null, null, 'TelefonoPersonas');
    } // buildRelations()

} // PersonaTableMap
