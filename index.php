<?php
/**
 * Created by PhpStorm.
 * User: Ser
 * Date: 9/06/14
 * Time: 22:01
 */
require 'vendor/autoload.php';
require_once 'vendor/propel/propel1/runtime/lib/Propel.php';
Propel::init("build/conf/incidencias-conf.php");
set_include_path("build/classes" . PATH_SEPARATOR . get_include_path());

$app = new \Slim\Slim();
$app->config(array(
    'debug' => true
));


require_once("build/api/entregas.php");
require_once("build/api/incidencias.php");
require_once("build/api/cajas.php");
require_once("build/api/movimientosCaja.php");
require_once("build/api/persona.php");
require_once("build/api/telefonos.php");
require_once("build/api/notas.php");
require_once("build/api/seguimientos.php");
require_once("build/api/empresas.php");
require_once("build/api/avisos.php");
require_once("build/api/tarea.php");
require_once("build/api/contabilidades.php");
require_once("build/api/tipoEntrega.php");
require_once("build/api/comunidades.php");
require_once("build/api/urgencia.php");



$app->run();





















/*
//$incidencia = new Incidencia();
$incidencia->crearIncidencia("FATIMA22","VECINO CUARTO B","Un vecino capullo que va y que viene","VECINO CAPULLO",date("Y/m/d"),date("H:i"));
//$incidencia->setComunidad("fatima55");
//$incidencia->setOrigen("VECINO");
$incidencia->save();

//IncidenciaQuery::create()->find()->delete();*/




